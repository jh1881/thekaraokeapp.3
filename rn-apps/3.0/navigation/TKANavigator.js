import {Platform, Settings} from 'react-native';
import {createAppContainer } from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createMaterialBottomTabNavigator} from 'react-navigation-material-bottom-tabs';
import {createDrawerNavigator} from 'react-navigation-drawer';

import {HostScreen} from '../screens/host/Host';
import {HostLoginScreen} from '../screens/host/HostLogin';
import {HostDetailScreen} from '../screens/host/HostDetail';

import {AllSongsScreen} from '../screens/songs/AllSongs';
import {NewSongsScreen} from '../screens/songs/NewSongs';

import {RequestSongScreen} from '../screens/requests/RequestSong';
import {PreviousRequestsScreen} from '../screens/requests/PreviousRequests';
import Colors from '../constants/colors';

//import SettingsScreen from '../screens/Settings';

const defaultStackNavOptions = {
    defaultNavigationOptions: {
        headerStyle: {
            backgroundColor: Platform.OS === 'android' ? Colors.primaryColor : ''
        },
        headerTintColor: Platform.OS === 'android' ? 'white' : Colors.primaryColor,
        // headerTitleStyle: {
        //     fontFamily: 'open-sans-bold'
        // },
        // headerBackTitleStyle: {
        //     fontFamily: 'open-sans-bold'
        // }
    }
};

const songsTabsConfig = {
    AllSongs: {
        screen: AllSongsScreen,
    },
    NewSongs: {
        screen: NewSongsScreen
    }
};

const requestsTabsConfig = {
    RequestSong: {
        screen: RequestSongScreen,
    },
    PreviousRequests: {
        screen: PreviousRequestsScreen
    }
};

const SongsNavigator = Platform.OS === 'android' ?
    createMaterialBottomTabNavigator(songsTabsConfig,
        {
            activeTintColor: 'white',
            shifting: true,
            barStyle: {
                backgroundColor: Colors.primaryColor
            },
        })
    : createBottomTabNavigator(songsTabsConfig,
        {
            tabBarOptions: {
                // labelStyle: {
                //     fontFamily: 'open-sans-bold'
                // },
                activeTintColor: Colors.accentColor,
            }
        });

const RequestsNavigator = Platform.OS === 'android' ?
    createMaterialBottomTabNavigator(requestsTabsConfig,
        {
            activeTintColor: 'white',
            shifting: true,
            barStyle: {
                backgroundColor: Colors.primaryColor
            },
        })
    : createBottomTabNavigator(requestsTabsConfig,
        {
            tabBarOptions: {
                // labelStyle: {
                //     fontFamily: 'open-sans-bold'
                // },
                activeTintColor: Colors.accentColor,
            }
        });

const HostNavigator = createStackNavigator({
    Host: HostScreen,
    HostLogin: HostLoginScreen,
    HostDetail: HostDetailScreen
}, defaultStackNavOptions);

const SettingsNavigator = createStackNavigator({
    Settings: Settings
}, defaultStackNavOptions);

const TKADrawerNavigator = createDrawerNavigator({
    Host: {
        screen: HostNavigator,
        navigationOptions: {
            drawerLabel: 'Host'
        }
    },
    Songs: {
        screen: SongsNavigator,
        navigationOptions : {
            drawerLabel: 'Songs'
        }
    },
    Requests: {
        screen: RequestsNavigator,
        navigationOptions: {
            drawerLabel: 'Requests'
        }
    },
    Settings: {
        screen: SettingsNavigator,
        navigationOptions: {
            drawerLabel: 'Settings'
        }
    }
}, {
    contentOptions: {
        activeTintColor: Colors.accentColor,
        // labelStyle: {
        //     fontFamily: 'open-sans-bold'
        // }
    }
});

export default createAppContainer(TKADrawerNavigator);
