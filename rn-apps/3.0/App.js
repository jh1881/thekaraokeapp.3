import React from 'react';
import { enableScreens } from 'react-native-screens';
//import { Provider } from 'react-redux';
import TKADrawerNavigator from './navigation/TKANavigator';

enableScreens();

export default function App() {
  return (
    // <Provider>
      <TKADrawerNavigator />
    // </Provider>
      );
}
