﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TKABackend.Api.DataModel;
using TKABackend.Api.Responses;
using TKABackend.Extensions;
using TKABackend.NotificationHubs;
using TKABackend.Services;

namespace TKABackend.Api
{
    [Authorize]
    [ApiController]
    [Route("api/h")]
    public class HostProcessController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly TkaDbContext _dbContext;
        private readonly NotificationHubProxyCollection _hubProxyCollection;
        private readonly ILogger _logger;
        private readonly AppSettings _appSettings;

        public HostProcessController(IUserService userService, TkaDbContext dbContext, IOptions<AppSettings> appSettings, NotificationHubProxyCollection notificationHubProxyCollection,
            ILogger logger)
        {
            _userService = userService;
            _dbContext = dbContext;
            _hubProxyCollection = notificationHubProxyCollection;
            _logger = logger;
            _appSettings = appSettings.Value;
        }

        [HttpPost]
        public async Task<IActionResult> Process([FromBody] ProcessRequest request)
        {
            // Validate the host!
            var hostId = User.Identity.Name.ToSafeInt();
            if (hostId.HasValue == false) return Unauthorized();
            var host = _userService.GetHost(hostId.Value);

            if (request == null) return BadRequest();
            switch (request.Type)
            {
                case ProcessRequestType.AmIAHost: return AmIAHost(host);
                // TODO: maybe move this to ProcessController?
                case ProcessRequestType.RequestPush: return await RequestPush(host, request);
                case ProcessRequestType.StartShow: return await StartShow(host, request);
                case ProcessRequestType.StopShow: return await StopShow(host);
                case ProcessRequestType.GetSongRequestNotifications: return await GetSongRequestNotifications(host);
                case ProcessRequestType.CheckShowStatus: return CheckShowStatus(host);
                default: return BadRequest();
            }
        }

        // TODO: maybe move this to ProcessController?
        private async Task<IActionResult> RequestPush(Host host, ProcessRequest request)
        {
            if (host == null) return Unauthorized();
            if (request.PU?.DeviceId == null || request.PU.DeviceType == null) return BadRequest();
            var deviceType = request.PU.DeviceType.ToSafeInt();
            if (deviceType == null) return BadRequest();
            var deviceId = request.PU.DeviceId.Replace(" ", "");
            var deviceRegistration = new DeviceRegistration
            {
                Handle = deviceId,
                Platform = deviceType == 1 ? MobilePlatform.apns : MobilePlatform.gcm
            };
            var hubProxy = _hubProxyCollection.GetProxy(host);
            if (hubProxy == null)
            {
                // Eep!
                return StatusCode(500);
            }

            var result = await hubProxy.RegisterForPushNotifications(host.Id.ToString(), deviceRegistration);
            if (!result.CompletedWithSuccess)
            {
                _logger.Log(LogLevel.Error, $"Failed to register {host.Id} with {deviceRegistration.Platform}: \n{string.Join("\t\n", "result.ErrorMessages")}");
                return StatusCode(502);
            }
            host.MobileDeviceId = deviceId;
            host.MobileDeviceType = deviceType.Value;

            await _dbContext.SaveChangesAsync();
            return Ok();
        }

        private IActionResult AmIAHost(Host host)
        {
            if (host == null) return NotFound();
            return Ok(new AmIAHostResponse { id = host.Id, notify = host.ReceiveMobileNotifications });
        }

        private IActionResult CheckShowStatus(Host host)
        {
            if (host == null) return Unauthorized();
            if (host.ShowInProgress) return Ok("1");
            return Ok("0");
        }

        private async Task<IActionResult> GetSongRequestNotifications(Host host)
        {
            if (host == null) return Unauthorized();
            var requests = _dbContext.SongRequests.Where(s => s.HostId == host.Id && s.Notified == false);
            foreach (var s in requests)
            {
                s.Notified = true;
            }

            await _dbContext.SaveChangesAsync();
            return Ok(requests);
        }

        private async Task<IActionResult> StopShow(Host host)
        {
            if (host == null) return Unauthorized();
            var shows = _dbContext.Shows.Where(h => h.HostId == host.Id && h.StopTime == null);
            if (shows.Any())
            {
                foreach (var s in shows)
                    s.StopTime = DateTime.Now;
            }

            host.ShowInProgress = false;

            await _dbContext.SaveChangesAsync();
            return Ok();
        }

        private async Task<IActionResult> StartShow(Host host, ProcessRequest request)
        {
            if (host == null) return Unauthorized();
            if (host.ShowInProgress) return Conflict();

            // Copy SongRequests into ShowSongRequests
            // Get current show.
            var currentShow = Show.GetCurrentShow(_dbContext, host.Id);
            if (currentShow != null)
            {
                var requests = _dbContext.SongRequests.Where(s => s.ShowId == currentShow.ShowId).ToList();
                foreach (var ssr in requests)
                {
                    _dbContext.ShowSongRequests.Add(ssr.Clone());
                }
                _dbContext.RemoveRange(requests);
            }

            // See if we have a lat and lon.
            var newShow = new Show
            {
                HostId = host.Id,
                StartTime = DateTime.Now
            };
            if (request.ST?.Lat != null && request.ST?.Lon != null)
            {
                var lat = request.ST.Lat.ToSafeDouble();
                var lon = request.ST.Lon.ToSafeDouble();
                if (lat.HasValue && lon.HasValue)
                {
                    newShow.Latitude = lat.Value;
                    newShow.Longitude = lat.Value;
                }
            }

            _dbContext.Shows.Add(newShow);

            host.ShowInProgress = true;

            await _dbContext.SaveChangesAsync();
            return Ok();
        }
    }
}
