﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TKABackend.Api.DataModel;
using TKABackend.Api.Responses;
using TKABackend.Extensions;
using TKABackend.NotificationHubs;
using TKABackend.Services;

namespace TKABackend.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProcessController : ControllerBase
    {
        private readonly TkaDbContext _dbContext;
        private readonly IUserService _userService;
        private readonly ILogger _logger;
        private readonly Mailer _mailer;
        private readonly NotificationHubProxy _proxy;
        private readonly AppSettings _appSettings;

        public ProcessController(TkaDbContext dbContext, IOptions<AppSettings> appSettings, IUserService userService, ILogger logger, Mailer mailer, NotificationHubProxy proxy)
        {
            _dbContext = dbContext;
            _userService = userService;
            _logger = logger;
            _mailer = mailer;
            _proxy = proxy;
            _appSettings = appSettings.Value;
        }

        [HttpPost]
        public async Task<IActionResult> Process([FromBody] ProcessRequest request)
        {
            using (_logger.BeginScope(new Dictionary<string, object> { { "Request", request } }))
            {
                if (request == null)
                {
                    _logger.LogTrace("Bad request (Process.1)");
                    return BadRequest();
                }
                switch (request.Type)
                {
                    case ProcessRequestType.HostLogin: return Login(request.LO);
                    case ProcessRequestType.SelectHost: return SelectHost(request);
                    case ProcessRequestType.QuerySongs: return QuerySongs(request);
                    case ProcessRequestType.RequestSong: return await RequestSong(request);
                    default:
                        {
                            _logger.LogTrace("Bad request (Process.2)");
                            return BadRequest();
                        }
                }
            }
        }


        private async Task<IActionResult> RequestSong(ProcessRequest request)
        {
            Response.Headers.Add("Access-Control-Allow-Origin", "*");

            if (string.IsNullOrEmpty(request.RQ?.Title) || string.IsNullOrEmpty(request.RQ?.Artist) ||
                string.IsNullOrEmpty(request.RQ?.Requestor) || string.IsNullOrEmpty(request.RQ?.HostId))
            {
                return BadRequest();
            }

            if (!string.IsNullOrEmpty(request.RQ.Lat))
            {
                if (string.IsNullOrEmpty(request.RQ.Lon))
                {
                    return BadRequest();
                }
            }
            else
            {
                if (string.IsNullOrEmpty(request.RQ.Lon))
                {
                    return BadRequest();
                }
            }

            var hostIdQ = request.RQ.HostId.ToSafeInt();
            if (!hostIdQ.HasValue)
            {
                return BadRequest();
            }
            var host = Host.FindById(_dbContext, hostIdQ.Value);
            if (host == null)
            {
                return NotFound();
            }

            if (!host.ShowInProgress)
            {
                return Forbid();
            }

            var maxRequestsPerHour = host.MaxSongRequestsPerHour ?? 4;

            var show = Show.GetCurrentShow(_dbContext, host.Id);
            if (show == null)
            {
                return Forbid();
            }


            //if (host.ReceiveMobileNotifications)
            //{
            //    var notification = new Notification();
            //    switch (host.MobileDeviceType)
            //    {
            //        case 0:
            //            notification.Platform = MobilePlatform.apns;
            //            break;
            //        case 1:
            //            notification.Platform = MobilePlatform.gcm; break;
            //        default:
            //            notification.Platform = MobilePlatform.wns; break;
            //    }

            //    notification.Handle = host.MobileDeviceId;
            //    notification.Content = "";
            //    var response = await _proxy.SendNotification(notification);
            //}

            if (host.ReceiveEmailNotifications && !string.IsNullOrEmpty(host.EmailAddress))
            {
                var response = await _mailer.SendTo(new MailAddress(host.EmailAddress), "New song request", GetTextEmail(request, host), GetHtmlEmail(request, host));
            }

            return Ok();
        }

        private static void AppendTableLine(StringBuilder sb, string header, string value)
        {
            sb.AppendLine("<tr><td>{header}</td><td>{value}</td></tr>");
        }

        private string GetHtmlEmail(ProcessRequest request, Host host)
        {
            var msg = new StringBuilder("<html><body>");
            msg.AppendLine($"<h1>{host.Name}, you have received a song request!</h1>");
            msg.AppendLine("<table cellpadding=\"10\">");
            AppendTableLine(msg, "<strong>Name</strong>", request.RQ.Requestor);
            AppendTableLine(msg, "<strong>Artist</strong>", request.RQ.Title);
            AppendTableLine(msg, "<strong>Title</strong>", request.RQ.Title);


            if (!string.IsNullOrWhiteSpace(request.RQ.GroupName))
                AppendTableLine(msg, "<strong>Group Names</strong>", request.RQ.GroupName);
            if (!string.IsNullOrWhiteSpace(request.RQ.DuetSecond))
                AppendTableLine(msg, "<strong>Duet Second Name</strong>", request.RQ.DuetSecond);
            if (!string.IsNullOrWhiteSpace(request.RQ.KeyChange))
                AppendTableLine(msg, "<strong>Key Change</strong>", request.RQ.KeyChange);
            if (!string.IsNullOrWhiteSpace(request.RQ.TempoChange))
                AppendTableLine(msg, "<strong>Tempo Change</strong>", request.RQ.TempoChange);

            msg.AppendLine("</table>");
            msg.AppendLine("</body></html>");
            return msg.ToString();
        }

        private string GetTextEmail(ProcessRequest request, Host host)
        {
            var msg = new StringBuilder();
            msg.AppendLine($"{host.Name}, a new song request has been received!");
            msg.AppendFormat($"{"Name",-27} = {request.RQ.Requestor}");
            msg.AppendFormat($"{"Artist",-27} = {request.RQ.Artist}");
            msg.AppendFormat($"{"Title",-27} = {request.RQ.Title}");

            if (!string.IsNullOrWhiteSpace(request.RQ.GroupName))
                msg.AppendFormat($"{"",-27} = {request.RQ.GroupName}");
            if (!string.IsNullOrWhiteSpace(request.RQ.DuetSecond))
                msg.AppendFormat($"{"Duet Second Name",-27} = {request.RQ.DuetSecond}");
            if (!string.IsNullOrWhiteSpace(request.RQ.KeyChange))
                msg.AppendFormat($"{"",-27} = {request.RQ.KeyChange}");
            if (!string.IsNullOrWhiteSpace(request.RQ.TempoChange))
                msg.AppendFormat($"{"",-27} = {request.RQ.TempoChange}");

            return msg.ToString();
        }

        private IActionResult QuerySongs(ProcessRequest request)
        {
            var seanIsACunt = true;
            var hostIdQ = request.SN.HostId.ToSafeInt();
            if (!hostIdQ.HasValue)
            {
                _logger.LogTrace("Bad Request (QuerySongs.1)");
                return BadRequest();
            }
            var hostId = hostIdQ.Value;

            var host = Host.FindById(_dbContext, hostId);
            if (host == null)
            {
                _logger.LogTrace("Not found (QuerySongs.2)");
                return NotFound();
            }
            // Don't do subscription check for a get songs?
            if (host.GetCurrentSubscription(_dbContext) == null)
            {
                _logger.LogTrace("Not found (QuerySongs.3)");
                return NotFound();
            }
            if (seanIsACunt)
                Response.Headers.Add("Access-Control-Allow-Origin", "*");
            var maxSongs = host.SongsPerPage > 0 ? host.SongsPerPage : 50;

            var songs = from song in _dbContext.Songs
                        join songInstance in _dbContext.SongInstances on song.Id equals songInstance.SongId
                        where songInstance.HostId == hostId
                        select song;
            object response;
            if (request.SN.Count)
            {
                response = $"{songs.Count()} {maxSongs}";
            }
            else if (request.SN.Cunt)
            {
                response = new SongCountResponse { Page = maxSongs, Total = songs.Count() };
            }
            else
            {
                if (!string.IsNullOrEmpty(request.SN.Search))
                {
                    if (request.SN.ExactMatch)
                    {
                        songs = songs.Where(song => song.Title.Contains(request.SN.Search) || song.Artist.Contains(request.SN.Search));
                    }
                    else
                    {
                        foreach (var str in request.SN.Search.Split(' ', StringSplitOptions.RemoveEmptyEntries))
                        {
                            songs = songs.Where(song => song.Title.Contains(str) || song.Artist.Contains(str));
                        }
                    }
                }

                songs = request.SN.OrderByTitle ?
                    songs.OrderBy(s => s.Title).ThenBy(s => s.Artist) :
                    songs.OrderBy(s => s.Artist).ThenBy(s => s.Title);

                songs = songs.Take(maxSongs);
                response = songs;
            }

            return Ok(response);
        }

        private IActionResult SelectHost(ProcessRequest request)
        {
            if (request.HO?.Number == null) return BadRequest();
            var hostId = request.HO.Number.ToSafeInt();
            if (hostId == null) return BadRequest();
            var host = Host.FindById(_dbContext, hostId.Value);
            if (host == null) return NotFound();
            if (host.GetCurrentSubscription(_dbContext) == null)
                return StatusCode(410);

            return Ok(new ChooseHostResponse
            {
                Id = host.Id,
                MenuDisplayName = host.MenuDisplayName ?? host.Name,
                Name = host.Name,
                BannerImage = host.BannerImage
            });
        }

        private IActionResult Login(HostLogin hostLoginRequest)
        {
            if (hostLoginRequest?.U == null || hostLoginRequest.P == null)
                return BadRequest();
            // Find a host that matches
            var token = _userService.Authenticate(hostLoginRequest.U, hostLoginRequest.P, out var host);
            if (host == null || token == null) return Unauthorized();

            return Ok(new LoginResponse { jwt = token });
        }
    }
}
