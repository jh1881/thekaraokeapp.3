﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CsvHelper;
using CsvHelper.Configuration.Attributes;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TKABackend.Api.DataModel;

namespace TKABackend.Api
{
    [Route("api/upload")]
    [ApiController]
    public class UploadController : ControllerBase
    {
        private readonly TkaDbContext _dbContext;

        public UploadController(TkaDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpPost]
        public async Task<IActionResult> UploadAndProcessFile(IFormFile file, bool? isNewSongs, int? hostId)
        {
            if (file == null || isNewSongs == null || hostId == null)
                return BadRequest();
            IEnumerable<CsvSongInput> records;
            using (var reader = new StreamReader(file.OpenReadStream()))
            {
                using (var csv = new CsvReader(reader))
                {
                    csv.Configuration.HasHeaderRecord = false;
                    records = csv.GetRecords<CsvSongInput>();
                }
            }

            var cmd = new SqlCommand("ProcessSongList") { CommandType = CommandType.StoredProcedure };

            cmd.Parameters.AddWithValue("@HostId", hostId);
            cmd.Parameters.AddWithValue("@IsNewSongs", isNewSongs);
            var param = cmd.Parameters.AddWithValue("@Songs", records);
            param.SqlDbType = SqlDbType.Structured;
            param.TypeName = "CsvSongs";

            await _dbContext.Database.ExecuteSqlCommandAsync("ProcessSongList", cmd.Parameters);

            return Ok();
        }
    }
}