﻿using CsvHelper.Configuration.Attributes;

namespace TKABackend.Api.DataModel
{

    public class CsvSongInput
    {
        [Index(0)]
        public string Artist { get; set; }
        [Index(1)]
        public string Title { get; set; }
    }
}
