﻿namespace TKABackend.Api.DataModel
{
    public class RequestSong : HostIdRequest
    {
        public string Title { get; set; }
        public string Artist { get; set; }
        public string Requestor { get; set; }
        public string Lat { get; set; }
        public string Lon { get; set; }
        public string Uid { get; set; }
        public string KeyChange { get; set; }
        public string TempoChange { get; set; }
        public string DuetSecond { get; set; }
        public string GroupName { get; set; }
    }
}