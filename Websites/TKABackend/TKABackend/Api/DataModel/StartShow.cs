﻿namespace TKABackend.Api.DataModel
{
    public class StartShow
    {
        public string Lat { get; set; }
        public string Lon { get; set; }
    }
}