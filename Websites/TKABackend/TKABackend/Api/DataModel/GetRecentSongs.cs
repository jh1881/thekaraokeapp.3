﻿namespace TKABackend.Api.DataModel
{
    public class GetRecentSongs : HostIdRequest
    {
        public bool OrderByTitle { get; set; }
    }
}