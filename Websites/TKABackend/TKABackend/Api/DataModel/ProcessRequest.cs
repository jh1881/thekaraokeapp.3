﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Localization.Internal;

namespace TKABackend.Api.DataModel
{
    public class ProcessRequest
    {
        public ProcessRequestType Type { get; set; }
        public HostLogin LO { get; set; }
        public ChooseHost HO { get; set; }
        public GetSongs SN { get; set; }
        public GetRecentSongs RS { get; set; }
        public RequestSong RQ { get; set; }
        public RequestPush PU { get; set; }
        public StartShow ST { get; set; }
    }
}
