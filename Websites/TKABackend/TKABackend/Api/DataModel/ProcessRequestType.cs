﻿namespace TKABackend.Api.DataModel
{
    public enum ProcessRequestType
    {
        HostLogin,
        AmIAHost,
        SelectHost,
        QuerySongs,
        RequestSong,
        RequestPush,
        StartShow,
        StopShow,
        GetSongRequestNotifications,
        CheckShowStatus,
        ValidateHost
    }
}