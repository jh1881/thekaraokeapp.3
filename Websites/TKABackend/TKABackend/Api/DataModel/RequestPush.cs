﻿namespace TKABackend.Api.DataModel
{
    public class RequestPush
    {
        public string DeviceId { get; set; }
        public string DeviceType { get; set; }
    }
}