﻿namespace TKABackend.Api.DataModel
{
    public abstract class HostIdRequest
    {
        public string HostId { get; set; }
    }
}