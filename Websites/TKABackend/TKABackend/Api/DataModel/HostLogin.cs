﻿namespace TKABackend.Api.DataModel
{
    public class HostLogin
    {
        public string U { get; set; }
        public string P { get; set; }
    }
}