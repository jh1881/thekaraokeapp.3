﻿namespace TKABackend.Api.Responses
{
    public class AmIAHostResponse
    {
        public int id { get; set; }
        public bool notify { get; set; }
    }
}
