﻿namespace TKABackend.Api.Responses
{
    public class SongCountResponse
    {
        public int Total { get; set; }
        public int Page { get; set; }
    }
}
