﻿namespace TKABackend.Api.Responses
{
    public class ChooseHostResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string MenuDisplayName { get; set; }
        public byte[] BannerImage { get; set; }
    }
}
