﻿namespace TKABackend.Api.Responses
{
    public class LoginResponse
    {
        public string jwt { get; set; }
    }
}
