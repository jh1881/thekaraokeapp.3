﻿namespace TKABackend.NotificationHubs
{
    public class Notification : DeviceRegistration
    {
        public string Content { get; set; }
    }
}
