﻿namespace TKABackend.NotificationHubs
{
    public class NotificationHubConfiguration
    {
        public string ConnectionString { get; set; }
        public string HubName { get; set; }
    }

    public class NotificationHubConfigurationArrayEntry
    {
        public string Host { get; set; }
        public NotificationHubConfiguration Settings { get; set; }
    }
}
