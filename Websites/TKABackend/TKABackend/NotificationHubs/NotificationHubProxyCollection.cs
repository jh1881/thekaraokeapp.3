﻿using System.Collections.Generic;
using Microsoft.Extensions.Configuration;

namespace TKABackend.NotificationHubs
{
    public class NotificationHubProxyCollection
    {
        private readonly Dictionary<string, NotificationHubConfiguration> _hubs = new Dictionary<string, NotificationHubConfiguration>();
        private readonly Dictionary<string, NotificationHubProxy> _proxies = new Dictionary<string, NotificationHubProxy>();

        public NotificationHubProxyCollection(IConfiguration configurationSection)
        {
            foreach (var section in configurationSection.GetChildren())
            {
                var entry = section.Get<NotificationHubConfigurationArrayEntry>();
                _hubs.Add(entry.Host, entry.Settings);
            }
        }


        public NotificationHubProxy GetProxy(Host host)
        {
            var hostId = host.Id.ToString();
            if (TryGetProxy(hostId, out var proxy)) return proxy;
            // try 0
            return TryGetProxy("0", out proxy) ? proxy : null;
        }

        private bool TryGetProxy(string hostId, out NotificationHubProxy proxy)
        {
            if (!_proxies.TryGetValue(hostId, out proxy))
            {
                // Otherwise, make one.
                if (!_hubs.TryGetValue(hostId, out var hub)) return false;

                proxy = new NotificationHubProxy(hub);
                _proxies.Add(hostId, proxy);
            }
            return true;
        }
    }
}