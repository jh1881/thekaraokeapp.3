﻿namespace TKABackend.Extensions
{
    public static class StringExtensions
    {
        public static int? ToSafeInt(this string t)
        {
            if (t == null || !int.TryParse(t, out var i)) return null;
            return i;
        }

        public static double? ToSafeDouble(this string t)
        {
            if (t == null || !double.TryParse(t, out var i)) return null;
            return i;
        }
    }
}
