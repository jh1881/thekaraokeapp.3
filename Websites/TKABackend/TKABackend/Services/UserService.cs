﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace TKABackend.Services
{
    public interface IUserService
    {
        string Authenticate(string username, string password, out Host host);
        Host GetHost(int hostId);
    }

    public class UserService : IUserService
    {
        private readonly TkaDbContext _dbContext;
        private readonly AppSettings _appSettings;

        public UserService(TkaDbContext dbContext, IOptions<AppSettings> appSettings)
        {
            _dbContext = dbContext;
            _appSettings = appSettings.Value;
        }

        public string Authenticate(string username, string password, out Host host)
        {
            username = username.ToLower();
            host = _dbContext.Hosts.FirstOrDefault(h => h.pwd == password && h.login_name == username);
            if (host == null)
                return null;

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new []
                {
                    new Claim(ClaimTypes.Name, host.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return token.ToString();
        }

        public Host GetHost(int hostId) => Host.FindById(_dbContext, hostId);
    }
}
