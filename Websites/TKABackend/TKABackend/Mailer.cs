﻿#define USE_SENDGRID
using System;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using DnsClient;
using DnsClient.Protocol;
using MimeKit;
using SendGrid;
using SendGrid.Helpers.Mail;
using SmtpClient = MailKit.Net.Smtp.SmtpClient;

namespace TKABackend
{
    public class Mailer
    {
        private async Task<bool> SendBySendGrid(MailAddress mailAddress, string title, string textBody, string htmlBody)
        {
            const string apiKey = "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";
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress("thekaraokeapp@thekaraokeapp.co.uk", "The Karaoke App");
            var to = new EmailAddress(mailAddress.Address, mailAddress.DisplayName);
            var msg = MailHelper.CreateSingleEmail(from, to, title, textBody, htmlBody);
            var response = await client.SendEmailAsync(msg);
            return response.StatusCode == HttpStatusCode.Accepted;
        }

        public async Task<bool> SendTo(MailAddress mailAddress, string title, string textBody, string htmlBody)
        {
#if USE_SENDGRID
            return await SendBySendGrid(mailAddress, title, textBody, htmlBody);
#else
            var client = new LookupClient();
            var result = client.Query(mailAddress.Host, QueryType.MX);
            if (result == null) return false;
            if (!result.Answers.Any()) return false;

            var message = new MimeMessage();
            message.From.Add(new MailboxAddress("thekaraokeapp@TheKaraokeApp.co.uk"));
            message.To.Add(new MailboxAddress(mailAddress.Address));
            message.Subject = "New song request";
            message.MessageId = $"<{Guid.NewGuid().ToString()}@thekaraokeapp.co.uk>";
            message.Date = DateTimeOffset.Now;
            message.Body = new BodyBuilder { TextBody = textBody, HtmlBody = htmlBody }.ToMessageBody();

            foreach (var record in result.Answers)
            {
                if (!(record is MxRecord mxRecord))
                    continue;
                var exchange = mxRecord.Exchange;

                try
                {
                    using (var smclient = new SmtpClient())
                    {
                        smclient.ServerCertificateValidationCallback = (sender, certificate, chain, errors) => true;
                        smclient.Connect(exchange, 25, false);
                        smclient.Send(message);
                        smclient.Disconnect(true);
                        return true;
                    }
                }
                catch { /**/ }
            }

            return false;
#endif
        }
    }
}
