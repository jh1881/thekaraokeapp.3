﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace TKABackend
{
    public class TkaDbContext : DbContext
    {
        public TkaDbContext(DbContextOptions<TkaDbContext> options) : base(options) { }
        public DbSet<Host> Hosts { get; set; }
        public DbSet<Show> Shows { get; set; }
        public DbSet<SongRequest> ShowSongRequests { get; set; }
        public DbSet<SongRequest> SongRequests { get; set; }
        public DbSet<Subscription> Subscriptions { get; set; }
        public DbSet<SongInstance> SongInstances { get; set; }
        public DbSet<Song> Songs { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Host>().ToTable("Hosts");
            modelBuilder.Entity<Show>().ToTable("Shows");
            modelBuilder.Entity<SongRequest>().ToTable("ShowSongRequests");
            modelBuilder.Entity<SongRequest>().ToTable("SongRequests");
            modelBuilder.Entity<Subscription>().ToTable("Subscriptions");
            modelBuilder.Entity<SongInstance>().ToTable("SongInstances");
            modelBuilder.Entity<Song>().ToTable("Songs");
        }
    }

    public class SongInstance
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int SongId { get; set; }
        public int HostId { get; set; }
    }

    public class Song
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Artist { get; set; }
        public DateTime DateAdded { get; set; }
    }

    public class Show
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ShowId { get; set; }
        public int HostId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? StopTime { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public DateTime? AutoCloseTime { get; set; }

        public static Show GetCurrentShow(TkaDbContext dbContext, int hostId) => dbContext.Shows.FirstOrDefault(h => h.StopTime == null && h.HostId == hostId);

        public static Show GetLastShow(TkaDbContext dbContext, int hostId)
        {
            try
            {
                var shows = dbContext.Shows.Where(h => h.HostId == hostId).ToList();
                return !shows.Any() ? null : shows.OrderBy(h => h.StartTime).Last();
            }
            catch (Exception)
            {
                return null;
            }
        }
    }

    public class Host
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public string EmailAddress { get; set; }
        public bool ShowInProgress { get; set; }
        public DateTime? CurrentSubScriptionExpiry { get; set; }
        public bool ReceiveMobileNotifications { get; set; }
        public bool ReceiveEmailNotifications { get; set; }
        public int? MaxSongRequestsPerHour { get; set; }
        public string MobileDeviceId { get; set; }
        public int MobileDeviceType { get; set; }
        public int SongsPerPage { get; set; }
        public string login_name { get; set; }
        public string pwd { get; set; }
        public int? PermittedGpsDistance { get; set; }
        public byte[] BannerImage { get; set; }
        public string MenuDisplayName { get; set; }

        public static Host FindById(TkaDbContext dbContext, int id) => dbContext.Hosts.FirstOrDefault(h => h.Id == id);

        public Subscription GetCurrentSubscription(TkaDbContext dbContext) => dbContext.Subscriptions
            .Where(h => h.HostId == Id).OrderByDescending(h => h.SubscriptionStart).ThenBy(h => h.SubscriptionEnd)
            .FirstOrDefault();
    }

    public class SongRequest
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Artist { get; set; }
        public string KeyChange { get; set; }
        public string TempoChange { get; set; }
        public string Requestor { get; set; }
        public string DuetSecond { get; set; }
        public DateTime RequestedTime { get; set; }
        public int HostId { get; set; }
        public bool Notified { get; set; }
        public string UId { get; set; }
        public string GroupNames { get; set; }
        public int ShowId { get; set; }
        public int GpsResult { get; set; }

        public SongRequest Clone()
        {
            return new SongRequest
            {
                Title = Title,
                Artist = Artist,
                KeyChange = KeyChange,
                TempoChange = TempoChange,
                Requestor = Requestor,
                DuetSecond = DuetSecond,
                RequestedTime = RequestedTime,
                HostId = HostId,
                Notified = Notified,
                UId = UId,
                GroupNames = GroupNames,
                ShowId = ShowId,
                GpsResult = GpsResult
            };
        }
    }

    public class Subscription
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int HostId { get; set; }
        public DateTime SubscriptionStart { get; set; }
        public DateTime? SubscriptionEnd { get; set; }
        public int SubscriptionType { get; set; }
        public decimal TotalCost { get; set; }
        public string Currency { get; set; }
    }
}
