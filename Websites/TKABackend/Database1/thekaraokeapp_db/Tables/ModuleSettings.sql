﻿CREATE TABLE [thekaraokeapp_db].[ModuleSettings] (
    [ModuleId]       INT            NOT NULL,
    [SettingName]    NVARCHAR (25)  NOT NULL,
    [SettingDefault] NVARCHAR (100) NOT NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'thekaraokeapp_db.ModuleSettings', @level0type = N'SCHEMA', @level0name = N'thekaraokeapp_db', @level1type = N'TABLE', @level1name = N'ModuleSettings';

