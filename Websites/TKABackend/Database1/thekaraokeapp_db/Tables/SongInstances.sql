﻿CREATE TABLE [thekaraokeapp_db].[SongInstances] (
    [SongId] INT DEFAULT (NULL) NULL,
    [HostId] INT DEFAULT (NULL) NULL
);


GO
CREATE NONCLUSTERED INDEX [HostToSong_idx]
    ON [thekaraokeapp_db].[SongInstances]([HostId] ASC);


GO
CREATE NONCLUSTERED INDEX [InstanceToSong_idx]
    ON [thekaraokeapp_db].[SongInstances]([SongId] ASC);


GO
CREATE NONCLUSTERED INDEX [SongId]
    ON [thekaraokeapp_db].[SongInstances]([SongId] ASC, [HostId] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'thekaraokeapp_db.SongInstances', @level0type = N'SCHEMA', @level0name = N'thekaraokeapp_db', @level1type = N'TABLE', @level1name = N'SongInstances';

