﻿CREATE TABLE [thekaraokeapp_db].[Subscriptions] (
    [Id]                INT           IDENTITY (4, 1) NOT NULL,
    [HostId]            INT           NOT NULL,
    [SubscriptionStart] DATETIME2 (0) NOT NULL,
    [SubscriptionEnd]   DATETIME2 (0) DEFAULT (NULL) NULL,
    [SubscriptionType]  SMALLINT      NOT NULL,
    [TotalCost]         DECIMAL (10)  NOT NULL,
    [Currency]          NVARCHAR (3)  DEFAULT (NULL) NULL,
    CONSTRAINT [PK_Subscriptions_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [HostId]
    ON [thekaraokeapp_db].[Subscriptions]([HostId] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'thekaraokeapp_db.Subscriptions', @level0type = N'SCHEMA', @level0name = N'thekaraokeapp_db', @level1type = N'TABLE', @level1name = N'Subscriptions';

