﻿CREATE TABLE [thekaraokeapp_db].[Modules] (
    [ModuleId]    INT             IDENTITY (4, 1) NOT NULL,
    [ModuleName]  NVARCHAR (100)  NOT NULL,
    [Price]       DECIMAL (10)    NOT NULL,
    [Description] NVARCHAR (2000) NOT NULL,
    CONSTRAINT [PK_Modules_ModuleId] PRIMARY KEY CLUSTERED ([ModuleId] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'thekaraokeapp_db.Modules', @level0type = N'SCHEMA', @level0name = N'thekaraokeapp_db', @level1type = N'TABLE', @level1name = N'Modules';

