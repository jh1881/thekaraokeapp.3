﻿CREATE TABLE [thekaraokeapp_db].[UploadLog] (
    [Id]            INT            IDENTITY (2455, 1) NOT NULL,
    [LogLine]       NVARCHAR (MAX) NOT NULL,
    [LogLineNumber] INT            NOT NULL,
    [QueueId]       INT            NOT NULL,
    CONSTRAINT [PK_UploadLog_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'thekaraokeapp_db.UploadLog', @level0type = N'SCHEMA', @level0name = N'thekaraokeapp_db', @level1type = N'TABLE', @level1name = N'UploadLog';

