﻿CREATE TABLE [thekaraokeapp_db].[HostModuleSettings] (
    [HostModuleId] INT            NOT NULL,
    [SettingName]  NVARCHAR (25)  NOT NULL,
    [SettingValue] NVARCHAR (100) NOT NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'thekaraokeapp_db.HostModuleSettings', @level0type = N'SCHEMA', @level0name = N'thekaraokeapp_db', @level1type = N'TABLE', @level1name = N'HostModuleSettings';

