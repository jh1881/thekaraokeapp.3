﻿CREATE TABLE [thekaraokeapp_db].[UploadQueue] (
    [Id]            INT            IDENTITY (43, 1) NOT NULL,
    [HostId]        INT            NOT NULL,
    [Filename]      NVARCHAR (MAX) NOT NULL,
    [Completed]     SMALLINT       NOT NULL,
    [InProgress]    SMALLINT       NOT NULL,
    [UploadedTime]  DATETIME2 (0)  NOT NULL,
    [CompletedTime] DATETIME2 (0)  DEFAULT (NULL) NULL,
    [isRecentSongs] SMALLINT       DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_UploadQueue_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'thekaraokeapp_db.UploadQueue', @level0type = N'SCHEMA', @level0name = N'thekaraokeapp_db', @level1type = N'TABLE', @level1name = N'UploadQueue';

