﻿CREATE TABLE [thekaraokeapp_db].[Shows] (
    [ShowId]        BIGINT        IDENTITY (634, 1) NOT NULL,
    [HostId]        INT           NOT NULL,
    [StartTime]     DATETIME2 (0) NOT NULL,
    [StopTime]      DATETIME2 (0) DEFAULT (NULL) NULL,
    [Longitude]     FLOAT (53)    DEFAULT (NULL) NULL,
    [Latitude]      FLOAT (53)    DEFAULT (NULL) NULL,
    [AutoCloseTime] DATETIME2 (0) DEFAULT (NULL) NULL,
    CONSTRAINT [PK_Shows_ShowId] PRIMARY KEY CLUSTERED ([ShowId] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'thekaraokeapp_db.Shows', @level0type = N'SCHEMA', @level0name = N'thekaraokeapp_db', @level1type = N'TABLE', @level1name = N'Shows';

