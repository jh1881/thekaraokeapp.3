﻿CREATE TABLE [thekaraokeapp_db].[Hosts] (
    [Id]                         INT            IDENTITY (4, 1) NOT NULL,
    [Name]                       NVARCHAR (45)  NOT NULL,
    [Number]                     NVARCHAR (8)   NOT NULL,
    [EmailAddress]               NVARCHAR (150) NOT NULL,
    [ShowInProgress]             SMALLINT       DEFAULT (NULL) NULL,
    [CurrentSubscriptionExpiry]  DATETIME2 (0)  DEFAULT (NULL) NULL,
    [ReceiveMobileNotifications] SMALLINT       DEFAULT ((0)) NOT NULL,
    [ReceiveEmailNotifications]  SMALLINT       DEFAULT ((0)) NOT NULL,
    [MaxSongRequestsPerHour]     SMALLINT       DEFAULT (NULL) NULL,
    [MobileDeviceId]             NVARCHAR (128) DEFAULT (NULL) NULL,
    [MobileDeviceType]           INT            DEFAULT (NULL) NULL,
    [SongsPerPage]               INT            NOT NULL,
    [login_name]                 NVARCHAR (25)  DEFAULT (NULL) NULL,
    [pwd]                        NVARCHAR (60)  DEFAULT (NULL) NULL,
    [PermittedGpsDistance]       INT            DEFAULT (NULL) NULL,
    CONSTRAINT [PK_Hosts_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [Hosts$EmailAddress_UNIQUE] UNIQUE NONCLUSTERED ([EmailAddress] ASC),
    CONSTRAINT [Hosts$Number_UNIQUE] UNIQUE NONCLUSTERED ([Number] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'thekaraokeapp_db.Hosts', @level0type = N'SCHEMA', @level0name = N'thekaraokeapp_db', @level1type = N'TABLE', @level1name = N'Hosts';

