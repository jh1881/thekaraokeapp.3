﻿CREATE TABLE [thekaraokeapp_db].[Songs] (
    [Id]        INT            IDENTITY (31621, 1) NOT NULL,
    [Title]     NVARCHAR (255) DEFAULT (NULL) NULL,
    [DateAdded] DATETIME       DEFAULT (getdate()) NOT NULL,
    [Artist]    NVARCHAR (255) DEFAULT (NULL) NULL,
    CONSTRAINT [PK_Songs_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [ArtistIdx]
    ON [thekaraokeapp_db].[Songs]([Artist] ASC);


GO
CREATE NONCLUSTERED INDEX [TitleIdx]
    ON [thekaraokeapp_db].[Songs]([Title] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'thekaraokeapp_db.Songs', @level0type = N'SCHEMA', @level0name = N'thekaraokeapp_db', @level1type = N'TABLE', @level1name = N'Songs';

