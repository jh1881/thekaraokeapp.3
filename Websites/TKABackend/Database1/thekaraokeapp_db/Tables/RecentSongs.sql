﻿CREATE TABLE [thekaraokeapp_db].[RecentSongs] (
    [SongId] INT DEFAULT (NULL) NULL,
    [HostId] INT DEFAULT (NULL) NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'thekaraokeapp_db.RecentSongs', @level0type = N'SCHEMA', @level0name = N'thekaraokeapp_db', @level1type = N'TABLE', @level1name = N'RecentSongs';

