﻿CREATE TABLE [thekaraokeapp_db].[HostModules] (
    [ModuleId]           INT            IDENTITY (1, 1) NOT NULL,
    [HostId]             INT            NOT NULL,
    [HostModuleSettings] NVARCHAR (255) NOT NULL,
    [DateAdded]          DATE           NOT NULL,
    [DateCancelled]      DATE           DEFAULT (NULL) NULL,
    CONSTRAINT [PK_HostModules_ModuleId] PRIMARY KEY CLUSTERED ([ModuleId] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'thekaraokeapp_db.HostModules', @level0type = N'SCHEMA', @level0name = N'thekaraokeapp_db', @level1type = N'TABLE', @level1name = N'HostModules';

