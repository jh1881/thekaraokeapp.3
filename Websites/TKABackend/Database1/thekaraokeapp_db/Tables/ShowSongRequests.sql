﻿CREATE TABLE [thekaraokeapp_db].[ShowSongRequests] (
    [Id]            INT            IDENTITY (10260, 1) NOT NULL,
    [Title]         NVARCHAR (MAX) NOT NULL,
    [Artist]        NVARCHAR (MAX) NOT NULL,
    [KeyChange]     NVARCHAR (25)  DEFAULT (NULL) NULL,
    [TempoChange]   NVARCHAR (25)  DEFAULT (NULL) NULL,
    [Requestor]     NVARCHAR (75)  NOT NULL,
    [DuetSecond]    NVARCHAR (75)  DEFAULT (NULL) NULL,
    [RequestedTime] DATETIME2 (0)  NOT NULL,
    [HostId]        INT            NOT NULL,
    [Notified]      SMALLINT       NOT NULL,
    [UId]           NVARCHAR (40)  NOT NULL,
    [GroupNames]    NVARCHAR (75)  NOT NULL,
    [ShowId]        INT            NOT NULL,
    [GpsResult]     INT            NOT NULL,
    CONSTRAINT [PK_ShowSongRequests_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [FacebookId]
    ON [thekaraokeapp_db].[ShowSongRequests]([UId] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'thekaraokeapp_db.ShowSongRequests', @level0type = N'SCHEMA', @level0name = N'thekaraokeapp_db', @level1type = N'TABLE', @level1name = N'ShowSongRequests';

