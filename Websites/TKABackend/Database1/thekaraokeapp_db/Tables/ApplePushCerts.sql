﻿CREATE TABLE [thekaraokeapp_db].[ApplePushCerts] (
    [Id]           INT            IDENTITY (5, 1) NOT NULL,
    [HostId]       INT            NOT NULL,
    [CertFilename] NVARCHAR (256) NOT NULL,
    [CertPassCr]   NVARCHAR (50)  NOT NULL,
    [IsProduction] SMALLINT       NOT NULL,
    [IsValid]      SMALLINT       DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_ApplePushCerts_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_SSMA_SOURCE', @value = N'thekaraokeapp_db.ApplePushCerts', @level0type = N'SCHEMA', @level0name = N'thekaraokeapp_db', @level1type = N'TABLE', @level1name = N'ApplePushCerts';

