import { LoginComponent } from './components/login/login.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { SongRequestsComponent } from './components/song-requests/song-requests.component';

const routes: Routes = [
  {path:'', pathMatch: 'full', redirectTo: "/home"},
  {path:'home', component: HomeComponent},
  {path:'songrequests', component: SongRequestsComponent},
  {path:'login', component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
 