import { TestBed } from '@angular/core/testing';

import { TkaCommsApiService } from './tka-comms-api.service';

describe('TkaCommsApiService', () => {
  let service: TkaCommsApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TkaCommsApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
