import { TestBed } from '@angular/core/testing';

import { HostUserService } from './host-user.service';

describe('HostUserService', () => {
  let service: HostUserService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HostUserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
