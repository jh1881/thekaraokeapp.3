import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient, private authService: AuthenticationService) {}

  public get(uri, params?, returnBody = true, responseType: "json" | "arraybuffer" | "blob" | "text" = "json") {
    return this.newHttpRequest('GET', uri, null, params, returnBody, responseType);
  }

  public post(uri, body?, params?) {
    return this.newHttpRequest('POST', uri, body, params);
  }

  public put(uri, body?, params?) {
    return this.newHttpRequest('PUT', uri, body, params);
  }

  public patch(uri, body?, params?) {
    return this.newHttpRequest('PATCH', uri, body, params);
  }

  public delete(uri, body?, params?) {
    return this.newHttpRequest('DELETE', uri, body, params);
  }

  private getHttpRequestHeaders() {
    return new HttpHeaders().set('Authorization', `Bearer ${this.authService.returnAuthToken()}`);
  }

  // public download(uri, method, body?, params?){
  //   this.newHttpRequest(method,uri, body, params, false, "arraybuffer").then(response => {
  //     let fileName = response.headers.get('x-filename');
  //     let contentType = response.headers.get('content-type');
  //     let data = response.body;

  //     const blob = new Blob([data], { type: contentType });

  //     saveAs(blob, fileName);
  //   })
  // }

  private newHttpRequest(type, uri, body?, params?, returnBody = true, responseType: "json" | "arraybuffer" | "blob" | "text" = "json") {
    return new Promise<any>((resolve, reject) => {
      this.http.request(type, uri, {
        observe: 'response',
        headers: this.getHttpRequestHeaders(),
        params: params,
        responseType: responseType,
        body: body
      }).subscribe(response => {

        // Request was successful
          console.log(`Calling ${uri}: Successful Status code: ${response.status}`);

          // const apiVersion = response.headers.get('SDeskVersion');

          // if (this.loadedVersion != apiVersion) {
          //   this.forceRefresh = true;
          // }

          if(returnBody){
            resolve(<any>response.body);
          } else {
            resolve(<any>response);
          }

        }, error => {
        // Request failed
          console.error(`Calling ${uri}: Failed with Error Code: ${error.status}`);
          console.error(error);

          // Genuine error message sent from SDesk
          if (error.status == 400) {
            // ToDo: We want to show 400 errors to the user, popup ?

            const errorMessage = error.body?.error;
          }

          // User is not authenticated, token expired or account has been disabled
          if (error.status == 401) {
            // Remove cached auth token
            // Redirect to login page
            this.authService.removeAuthTokenAndRedirectToLogin();
          }

          reject(<any>error.body);
        })
    });
  }
}
