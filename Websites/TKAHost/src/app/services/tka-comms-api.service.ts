import { ApiService } from './api.service';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import { promise } from 'selenium-webdriver';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class TkaCommsApiService {
  static readonly urlBase : "https://thekaraokeapp.co.uk/v3/process.php";
  private token: string;
  
  constructor(private apiService: ApiService, private authService: AuthenticationService) {
  }

  login(username: string, password: string) {
    return this.apiService.get(TkaCommsApiService.urlBase, {"op": "LO", "u": username, "p": password}, true, "json");
  }
}
