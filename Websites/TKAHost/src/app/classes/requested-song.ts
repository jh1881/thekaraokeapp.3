
export class RequestedSong {
    Id: number;
    Title: string;
    Artist: string;
    Requestor: string;
    KeyChange: string;
    TempoChange: string;
    DuetSecond: string;
    RequestedTime: string;
    GroupName: string;
    GpsResult: number;
}
