﻿using Amazon.Lambda.APIGatewayEvents;
using System.Net;
using System.Text.Json;
using TKABackend.Serverless.Model.Responses;

namespace TKABackend.Serverless.Helpers
{
    internal static class ApiHelper
    {
        public static APIGatewayProxyResponse ReturnCodeWithJsonBody(HttpStatusCode statusCode = HttpStatusCode.OK, string? json = null)
        {
            return new APIGatewayProxyResponse
            {
                StatusCode = (int)statusCode,
                Body = json,
                Headers = new Dictionary<string, string> { { "Content-Type", "application/json" } }
            };
        }

        public static APIGatewayProxyResponse ReturnCodeWithJsonBody<T>(HttpStatusCode statusCode = HttpStatusCode.OK, T? data = null) where T : class, IResponse
        {
            return ReturnCodeWithJsonBody(statusCode, JsonSerializer.Serialize(data));
        }
    }
}
