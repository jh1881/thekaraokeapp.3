﻿namespace TKABackend.Serverless.Database
{
    internal interface IRepository<T> where T : class
    {
        string KeyField { get; }
        Task AddAsync(T newItem);
        Task UpdateAsync(T newItem);
        Task DeleteAsync(T newItem);

        Task<IEnumerable<T>> GetAllAsync();
        Task<T?> GetAsync(string id);
    }
}
