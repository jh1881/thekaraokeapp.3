﻿using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;

namespace TKABackend.Serverless.Database
{
    internal abstract class BaseRepository<T> : IRepository<T> where T : class
    {
        protected DBConfig dbConfig;

        public abstract string KeyField { get; }

        protected BaseRepository()
        {
            dbConfig = new DBConfig();
        }

        public virtual Task AddAsync(T newItem)
        {
            return dbConfig.Context.SaveAsync(newItem);
        }

        public virtual Task DeleteAsync(T newItem)
        {
            return dbConfig.Context.DeleteAsync(newItem);
        }

        public virtual Task UpdateAsync(T newItem)
        {
            return dbConfig.Context.SaveAsync(newItem);
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var table = dbConfig.Context.GetTargetTable<T>();

            var results = table.Scan(new ScanOperationConfig());
            var data = await results.GetRemainingAsync();
            return dbConfig.Context.FromDocuments<T>(data);
        }

        public Task<T?> GetAsync(string id)
        {
            var conds = new []
            {
                new ScanCondition(this.KeyField, ScanOperator.Equal, id)
            };
            return dbConfig.Context.ScanAsync<T>(conds).GetRemainingAsync().ContinueWith(t => t.Result?.FirstOrDefault());
        }

        public Task<List<T>> GetByFieldAsync(string fieldName, string? text)
        {
            return dbConfig.Context.ScanAsync<T>(new[] { new ScanCondition(fieldName, ScanOperator.Equal, text) }).GetRemainingAsync();
        }

        public Task<T?> GetSingleByFieldAsync(string fieldName, string? text) => GetByFieldAsync(fieldName, text).ContinueWith(t => t.Result?.FirstOrDefault());
    }
}
