﻿using TKABackend.Serverless.Extensions;
using TKABackend.Serverless.Model;

namespace TKABackend.Serverless.Database
{
    internal class HostRepository : BaseRepository<Host>
    {
        public HostRepository() : base()
        {
        }

        public override string KeyField => nameof(Host.hostGuid);

        public Task<Host?> GetHost(string? hostGuid) => GetSingleByFieldAsync(nameof(Host.hostGuid), hostGuid);
        public Task<Host?> GetByUsername(string? username) => GetSingleByFieldAsync(nameof(Host.loginName), username);

        public async Task AddSongs(string hostGuid, List<Song> songs)
        {
            var host = await GetHost(hostGuid);
            if (host == null) return;

            host.songs = songs.Where(s => s.Title != null && s.Artist != null).ToList();
            await UpdateAsync(host);
        }

        public async Task SetRecentSongs(string hostGuid, List<Song> recentSongs)
        {
            var host = await GetHost(hostGuid);
            if (host == null) return;

            if(host.songs == null)
            {
                host.songs = recentSongs;
                host.songs.ForEach(s => s.IsRecent = true);
            }
            else
            {
                host.songs.Where(s => recentSongs.Any(r => r.Title.ToUpper() == s.Title.ToUpper() && r.Artist.ToUpper() == s.Title.ToUpper())).ForEach(rs => rs.IsRecent = true);
            }
            await UpdateAsync(host);
        }
    }
}
