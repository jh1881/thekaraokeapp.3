﻿using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TKABackend.Serverless.Database
{
    internal class DBConfig
    {
        private AmazonDynamoDBClient client;
        public AmazonDynamoDBClient Client { get => client; set => client = value; }
        public DynamoDBContext Context { get; set; }

        public DBConfig()
        {
            AmazonDynamoDBConfig clientConfig = new()
            {
                RegionEndpoint = RegionEndpoint.EUWest2
            };
            client = new AmazonDynamoDBClient(clientConfig);
            Context = new DynamoDBContext(client);
        }
    }
}
