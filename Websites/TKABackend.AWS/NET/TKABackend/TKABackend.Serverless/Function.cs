using Amazon.Lambda.Core;
using Amazon.Lambda.APIGatewayEvents;
using Microsoft.Extensions.DependencyInjection;
using TKABackend.Serverless.Processors;
using TKABackend.Serverless.Auth;
using TKABackend.Serverless.Model.Validators;
using TKABackend.Serverless.Database;
using TKABackend.Serverless.Model.Requests;
using Amazon.Lambda.S3Events;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace TKABackend.Serverless;

public class Functions
{
    private readonly ServiceProvider serviceProvider;

    public Functions()
    {
        var serviceCollection = new ServiceCollection();
        ConfigureServices(serviceCollection);
        serviceProvider = serviceCollection.BuildServiceProvider();
    }

    private static void ConfigureServices(ServiceCollection serviceCollection)
    {
        serviceCollection.AddSingleton<IAuthService>(new JWTService(JWTContainerModel.Key));
        serviceCollection.AddTransient<HostRepository>();

        serviceCollection.AddTransient<LoginProcessor>();
        serviceCollection.AddTransient<AmIAHostProcessor>();
        serviceCollection.AddTransient<ChooseHostProcessor>();
        serviceCollection.AddTransient<GetSongsProcessor>();
        serviceCollection.AddTransient<GetRecentSongsProcessor>();
        serviceCollection.AddTransient<RequestSongProcessor>();
        serviceCollection.AddTransient<RequestPushProcessor>();
        serviceCollection.AddTransient<StartShowProcessor>();
        serviceCollection.AddTransient<StopShowProcessor>();
        serviceCollection.AddTransient<GetSongRequestNotificationsProcessor>();
        serviceCollection.AddTransient<CheckShowStatusProcessor>();
        serviceCollection.AddTransient<ValidateProcessor>();
        serviceCollection.AddTransient<CsvProcessor>();

        serviceCollection.AddSingleton<IRequestValidator<ChooseHostRequest>, ChooseHostRequestValidator>();
        serviceCollection.AddSingleton<IRequestValidator<GetRecentSongsRequest>, GetRecentSongsRequestValidator>();
        serviceCollection.AddSingleton<IRequestValidator<GetSongsRequest>, GetSongsRequestValidator>();
        serviceCollection.AddSingleton<IRequestValidator<LoginRequest>, LoginRequestValidator>();
        serviceCollection.AddSingleton<IRequestValidator<RequestPushRequest>, RequestPushRequestValidator>();
        serviceCollection.AddSingleton<IRequestValidator<RequestSongRequest>, RequestSongRequestValidator>();
        serviceCollection.AddSingleton<IRequestValidator<StartShowRequest>, StartShowRequestValidator>();

        serviceCollection.AddSingleton<EmailProcessor>();
        serviceCollection.AddSingleton<PushNotificationProcessor>();
    }

    private Task InvokeEventProcessor<T>(S3Event s3event, ILambdaContext context) where T : IEventProcessor
    {
        var evt = s3event.Records?[0].S3;
        if (evt == null) return Task.CompletedTask;

        var service = serviceProvider.GetService<T>();
        if(service == null) return Task.CompletedTask;
        return service.Process(evt, context);
    }
    public Task ProcessCsv(S3Event @event, ILambdaContext context) => InvokeEventProcessor<CsvProcessor>(@event, context);


    private async Task<APIGatewayProxyResponse> InvokeProcessor<T>(APIGatewayProxyRequest request, ILambdaContext context) where T : IProcessor
    {
        var service = serviceProvider.GetService<T>();
        if (service == null) return new APIGatewayProxyResponse { StatusCode = 500 };
        return await service.Process(request, context) ?? new APIGatewayProxyResponse { StatusCode = 500 };
    }

    public Task<APIGatewayProxyResponse> Login(APIGatewayProxyRequest request, ILambdaContext context) => InvokeProcessor<LoginProcessor>(request, context);
    public Task<APIGatewayProxyResponse> AmIAHost(APIGatewayProxyRequest request, ILambdaContext context) => InvokeProcessor<AmIAHostProcessor>(request, context);
    public Task<APIGatewayProxyResponse> ChooseHost(APIGatewayProxyRequest request, ILambdaContext context) => InvokeProcessor<ChooseHostProcessor>(request, context);
    public Task<APIGatewayProxyResponse> GetSongs(APIGatewayProxyRequest request, ILambdaContext context) => InvokeProcessor<GetSongsProcessor>(request, context);
    public Task<APIGatewayProxyResponse> GetRecentSongs(APIGatewayProxyRequest request, ILambdaContext context) => InvokeProcessor<GetRecentSongsProcessor>(request, context);
    public Task<APIGatewayProxyResponse> RequestSong(APIGatewayProxyRequest request, ILambdaContext context) => InvokeProcessor<RequestSongProcessor>(request, context);
    public Task<APIGatewayProxyResponse> RequestPush(APIGatewayProxyRequest request, ILambdaContext context) => InvokeProcessor<RequestPushProcessor>(request, context);
    public Task<APIGatewayProxyResponse> StartShow(APIGatewayProxyRequest request, ILambdaContext context) => InvokeProcessor<StartShowProcessor>(request, context);
    public Task<APIGatewayProxyResponse> StopShow(APIGatewayProxyRequest request, ILambdaContext context) => InvokeProcessor<StopShowProcessor>(request, context);
    public Task<APIGatewayProxyResponse> GetSongRequestNotifications(APIGatewayProxyRequest request, ILambdaContext context) => InvokeProcessor<GetSongRequestNotificationsProcessor>(request, context);
    public Task<APIGatewayProxyResponse> CheckShowStatus(APIGatewayProxyRequest request, ILambdaContext context) => InvokeProcessor<CheckShowStatusProcessor>(request, context);
    public Task<APIGatewayProxyResponse> Validate(APIGatewayProxyRequest request, ILambdaContext context) => InvokeProcessor<ValidateProcessor>(request, context);
    public Task<APIGatewayProxyResponse> Upload(APIGatewayProxyRequest request, ILambdaContext context) => InvokeProcessor<UploadProcessor>(request, context);
}
