﻿using TKABackend.Serverless.Auth;
using TKABackend.Serverless.Database;
using TKABackend.Serverless.Extensions;
using TKABackend.Serverless.Model.Requests;
using TKABackend.Serverless.Model.Responses;
using TKABackend.Serverless.Model.Validators;

namespace TKABackend.Serverless.Processors
{
    internal class StartShowProcessor : BaseProcessor<StartShowRequest, StartShowResponse>
    {
        public StartShowProcessor(IAuthService authService, HostRepository repository , IRequestValidator<StartShowRequest> validator) : base(authService, repository, validator)
        {
        }

        protected override async Task Process()
        {
            if(AuthorisedHost == null)
            {
                StatusCode = System.Net.HttpStatusCode.BadRequest;
                return;
            }

            if (AuthorisedHost.shows == null) AuthorisedHost.shows = new List<Model.Show>();
            // If any show is open, close it.
            AuthorisedHost.shows.Where(s => s.showEnd == null).ForEach(s => s.showEnd = DateTime.Now);
            AuthorisedHost.showInProgress = true;
            AuthorisedHost.shows.Add(new Model.Show { showStart = DateTime.Now, latitude = Request?.Latitude, longitude = Request?.Longitude });
            await SaveHost();
        }
    }
}
