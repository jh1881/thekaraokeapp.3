﻿using TKABackend.Serverless.Auth;
using TKABackend.Serverless.Database;
using TKABackend.Serverless.Model.Responses;

namespace TKABackend.Serverless.Processors
{
    internal class GetSongRequestNotificationsProcessor : BaseProcessor<GetSongRequestNotificationsResponse>
    {
        public GetSongRequestNotificationsProcessor(IAuthService authService, HostRepository repository) : base(authService, repository)
        {
        }

        protected override Task Process()
        {
            if(AuthorisedHost == null)
            {
                StatusCode = System.Net.HttpStatusCode.Unauthorized;
                return Task.CompletedTask;
            }

            if (AuthorisedHost.songRequests?.Any() == true)
            {
                Response.requests = new List<Model.SongRequest>(AuthorisedHost.songRequests.Where(r => r.notified == false));
            }
            return Task.CompletedTask;
        }
    }
}
