﻿using TKABackend.Serverless.Auth;
using TKABackend.Serverless.Database;
using TKABackend.Serverless.Model.Requests;
using TKABackend.Serverless.Model.Responses;
using TKABackend.Serverless.Model.Validators;

namespace TKABackend.Serverless.Processors
{
    internal class GetSongsProcessor : BaseProcessor<GetSongsRequest, GetSongsResponse>
    {
        public GetSongsProcessor(IAuthService authService, HostRepository repository, IRequestValidator<GetSongsRequest> validator) : base(authService, repository, validator)
        {
        }

        protected override Task Process()
        {
            // Return 400 on bad input
            // Return 400 on no host???


            return Task.CompletedTask;
        }
    }
}
