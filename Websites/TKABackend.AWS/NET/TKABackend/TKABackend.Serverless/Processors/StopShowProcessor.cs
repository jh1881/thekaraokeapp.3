﻿using TKABackend.Serverless.Auth;
using TKABackend.Serverless.Database;
using TKABackend.Serverless.Model.Responses;

namespace TKABackend.Serverless.Processors
{
    internal class StopShowProcessor : BaseProcessor<StopShowResponse>
    {
        public StopShowProcessor(IAuthService authService, HostRepository repository) : base(authService, repository)
        {
        }

        protected override async Task Process()
        {
            if (!ValidateAuthority() || AuthorisedHost == null)
            {
                StatusCode = System.Net.HttpStatusCode.Unauthorized;
                return;
            }

            AuthorisedHost.showInProgress = false;
            var currentShow = AuthorisedHost.shows?.FirstOrDefault(s => s.showEnd == null);
            if(currentShow != null)
            {
                currentShow.showEnd = DateTime.Now;
            }
            await SaveHost();
        }
    }
}
