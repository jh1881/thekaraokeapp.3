﻿using TKABackend.Serverless.Auth;
using TKABackend.Serverless.Database;
using TKABackend.Serverless.Extensions;
using TKABackend.Serverless.Model.Requests;
using TKABackend.Serverless.Model.Responses;
using TKABackend.Serverless.Model.Validators;

namespace TKABackend.Serverless.Processors
{
    internal class ChooseHostProcessor : BaseProcessor<ChooseHostRequest, ChooseHostResponse>
    {
        public ChooseHostProcessor(IAuthService authService, HostRepository repository, IRequestValidator<ChooseHostRequest> validator) : base(authService, repository, validator)
        {
        }

        protected override async Task Process()
        {
            var chosenHost = await HostRepository.GetSingleByFieldAsync(nameof(AuthorisedHost.number), Request.Number);
            if(chosenHost == null)
            {
                StatusCode = System.Net.HttpStatusCode.NotFound;
                return;
            }

            if(!chosenHost.HasValidSubscription())
            {
                StatusCode = System.Net.HttpStatusCode.Gone;
                return;
            }

            Response.Id = chosenHost.hostGuid;
            Response.MenuDisplayName = chosenHost.menuDisplayName;
            Response.Name = chosenHost.name;
            Response.BannerImage = chosenHost.bannerImageb64;
        }
    }
}
