﻿using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using System.Net;
using System.Security.Claims;
using System.Text.Json;
using TKABackend.Serverless.Auth;
using TKABackend.Serverless.Database;
using TKABackend.Serverless.Helpers;
using TKABackend.Serverless.Model;
using TKABackend.Serverless.Model.Requests;
using TKABackend.Serverless.Model.Responses;
using TKABackend.Serverless.Model.Validators;

namespace TKABackend.Serverless.Processors
{
    internal abstract class BaseProcessor<TResponse> : IProcessor where TResponse : class, IResponse, new()
    {
        protected IAuthService AuthService { get; }
        protected HostRepository HostRepository { get; }
        protected ILambdaContext? Context { get; private set; }
        protected APIGatewayProxyRequest? GatewayRequest { get; private set; }
        protected bool TokenAuthorised { get; private set; }
        protected string? Token { get; private set; }
        protected string? AuthorisedId { get; private set; }
        protected TResponse Response { get; private set; } = new();
        protected HttpStatusCode StatusCode { get; set; } = HttpStatusCode.OK;
        protected Host? AuthorisedHost { get; private set; }
        protected BaseProcessor(IAuthService authService, HostRepository repository)
        {
            AuthService = authService;
            HostRepository = repository;
        }

        public async Task<APIGatewayProxyResponse> Process(APIGatewayProxyRequest request, ILambdaContext context)
        {
            GatewayRequest = request;
            Context = context;

            await ProcessHeaders();
            if(!ProcessRequest())
            {
                return ApiHelper.ReturnCodeWithJsonBody(HttpStatusCode.BadRequest, null);
            }
            await Process();

            return ApiHelper.ReturnCodeWithJsonBody(StatusCode, StatusCode == HttpStatusCode.OK ? Response : null);
        }

        private async Task ProcessHeaders()
        {
            if (GatewayRequest?.Headers?.TryGetValue("Authorization", out var bearerToken) == true)
            {
                var bits = bearerToken.Split(' ');
                if (bits[0].ToUpper() == "BEARER")
                {
                    Token = bits[1];
                    TokenAuthorised = AuthService.IsTokenValid(Token);
                    if (TokenAuthorised)
                    {
                        AuthorisedId = AuthService.GetTokenClaims(Token).FirstOrDefault(claim => claim.Type.Equals(ClaimTypes.Authentication))?.Value;
                        AuthorisedHost = await HostRepository.GetHost(AuthorisedId);
                    }
                }
            }
        }

        protected async Task SaveHost() { if (AuthorisedHost != null) await HostRepository.UpdateAsync(AuthorisedHost); }

        protected bool ValidateAuthority() => TokenAuthorised && !string.IsNullOrWhiteSpace(AuthorisedId);

        protected virtual bool ProcessRequest() { return true; }
        protected abstract Task Process();
    }

    internal abstract class BaseProcessor<TRequest, TResponse> : BaseProcessor<TResponse> where TRequest : class, IRequest, new() where TResponse : class, IResponse, new()
    {
        protected TRequest? Request { get; private set; }
        protected IRequestValidator<TRequest> Validator { get; private set; }
        protected bool? RequestIsValid { get; private set; }

        protected BaseProcessor(IAuthService authService, HostRepository repository, IRequestValidator<TRequest> validator) : base(authService, repository)
        {
            Validator = validator;
        }

        protected override bool ProcessRequest()
        {
            if(!string.IsNullOrWhiteSpace(GatewayRequest?.Body))
            {
                Request = JsonSerializer.Deserialize<TRequest>(GatewayRequest.Body);
                if(Validator != null)
                {
                    RequestIsValid = Validator.Validate(Request);
                    return RequestIsValid.Value;
                }
                return true;
            }
            return true;
        }
    }
}
