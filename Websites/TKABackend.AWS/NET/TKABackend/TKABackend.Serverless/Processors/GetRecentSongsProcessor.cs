﻿using TKABackend.Serverless.Auth;
using TKABackend.Serverless.Database;
using TKABackend.Serverless.Model.Requests;
using TKABackend.Serverless.Model.Responses;
using TKABackend.Serverless.Model.Validators;

namespace TKABackend.Serverless.Processors
{
    internal class GetRecentSongsProcessor : BaseProcessor<GetRecentSongsRequest, GetRecentSongsResponse>
    {
        public GetRecentSongsProcessor(IAuthService authService, HostRepository repository, IRequestValidator<GetRecentSongsRequest> validator) : base(authService, repository, validator)
        {
        }

        protected override async Task Process()
        {
            var host = await HostRepository.GetHost(Request.HostId);
            if(host == null)
            {
                StatusCode = System.Net.HttpStatusCode.NotFound;
                return;
            }

            var recentSongs = host.songs?.Where(s => s.IsRecent);
            if (recentSongs != null)
            {
                if (Request.OrderByTitle == true)
                {
                    recentSongs = recentSongs.OrderBy(s => s.Title).ThenBy(s => s.Artist);
                }
                else
                {
                    recentSongs = recentSongs.OrderBy(s => s.Artist).ThenBy(s => s.Title);
                }

                Response.Songs = new List<Model.Song>(recentSongs);
            }
        }
    }
}
