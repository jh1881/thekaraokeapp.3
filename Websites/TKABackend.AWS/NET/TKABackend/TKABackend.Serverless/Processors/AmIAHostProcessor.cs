﻿using TKABackend.Serverless.Auth;
using TKABackend.Serverless.Database;
using TKABackend.Serverless.Model.Responses;

namespace TKABackend.Serverless.Processors
{
    internal class AmIAHostProcessor : BaseProcessor<AmIAHostResponse>
    {
        private readonly HostRepository repository;

        public AmIAHostProcessor(IAuthService authService, HostRepository repository) : base(authService, repository)
        {
            this.repository = repository;
        }

        protected override Task Process()
        {
            if(!ValidateAuthority())
            {
                StatusCode = System.Net.HttpStatusCode.Unauthorized;
                return Task.CompletedTask;
            }

            if (AuthorisedHost == null)
            {
                StatusCode = System.Net.HttpStatusCode.NotFound;
                return Task.CompletedTask;
            }

            Response.Notif = AuthorisedHost.receiveMobileNotifications;
            Response.Id = AuthorisedHost.hostGuid;
            return Task.CompletedTask;
        }
    }
}
