﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using HttpMultipartParser;
using TKABackend.Serverless.Auth;
using TKABackend.Serverless.Database;
using TKABackend.Serverless.Extensions;
using TKABackend.Serverless.Model.Responses;

namespace TKABackend.Serverless.Processors
{
    internal class UploadProcessor : BaseProcessor<UploadResponse>
    {
        public UploadProcessor(IAuthService authService, HostRepository repository) : base(authService, repository)
        {
        }

        protected override async Task Process()
        {
            using var client = new AmazonS3Client(RegionEndpoint.EUWest2);

            /* Get csv*/
            using var stream = GatewayRequest.Body.ToStream();
            var parsed = MultipartFormDataParser.Parse(stream);
            var hostId = parsed.GetParameterValue("hostId");
            var file = parsed.Files.First();
            Stream csvContents = file.Data;

            var guid = Guid.NewGuid();

            var request = new PutObjectRequest
            {
                BucketName = "tka-csv-bucket",
                Key = $"{hostId}/{guid}",
                InputStream = csvContents,
                ContentType = "text/csv"
            };

            var awsResponse = await client.PutObjectAsync(request);
            csvContents.Dispose();
            StatusCode = awsResponse.HttpStatusCode;
        }
    }
}
