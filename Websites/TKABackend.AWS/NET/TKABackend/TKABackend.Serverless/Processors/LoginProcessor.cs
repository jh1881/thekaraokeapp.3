﻿using System.Security.Claims;
using TKABackend.Serverless.Auth;
using TKABackend.Serverless.Database;
using TKABackend.Serverless.Helpers;
using TKABackend.Serverless.Model.Requests;
using TKABackend.Serverless.Model.Responses;
using TKABackend.Serverless.Model.Validators;

namespace TKABackend.Serverless.Processors
{
    internal class LoginProcessor : BaseProcessor<LoginRequest, LoginResponse>
    {
        public LoginProcessor(IAuthService authService, IRequestValidator<LoginRequest> validator, HostRepository hostRepository) : base(authService, hostRepository, validator)
        {
        }

        protected override async Task Process()
        {
            var host = await HostRepository.GetByUsername(Request?.Username);
            if (host == null)
            {
                StatusCode = System.Net.HttpStatusCode.Unauthorized;
                return;
            }

            if (!UserHelper.ValidatePw(Request?.Username, Request?.Password, host.pwd))
            {
                StatusCode = System.Net.HttpStatusCode.Unauthorized;
                return;
            }

            var jwtModel = new JWTContainerModel
            {
                Claims = new[] { new Claim(ClaimTypes.Authentication, host.hostGuid) }
            };
            Response.jwt = AuthService.GenerateToken(jwtModel);
        }
    }
}
