﻿using TKABackend.Serverless.Auth;
using TKABackend.Serverless.Database;
using TKABackend.Serverless.Model.Requests;
using TKABackend.Serverless.Model.Responses;
using TKABackend.Serverless.Model.Validators;

namespace TKABackend.Serverless.Processors
{
    internal class RequestSongProcessor : BaseProcessor<RequestSongRequest, RequestSongResponse>
    {
        private readonly EmailProcessor emailProcessor;
        private readonly PushNotificationProcessor pushNotificationProcessor;

        public RequestSongProcessor(IAuthService authService, HostRepository repository, IRequestValidator<RequestSongRequest> validator, EmailProcessor emailProcessor, PushNotificationProcessor pushNotificationProcessor) : base(authService, repository, validator)
        {
            this.emailProcessor = emailProcessor;
            this.pushNotificationProcessor = pushNotificationProcessor;
        }

        protected override Task Process()
        {

            return Task.CompletedTask;
        }
    }
}
