﻿using TKABackend.Serverless.Auth;
using TKABackend.Serverless.Database;
using TKABackend.Serverless.Model.Responses;

namespace TKABackend.Serverless.Processors
{
    internal class ValidateProcessor : BaseProcessor<ValidateResponse>
    {
        public ValidateProcessor(IAuthService authService, HostRepository repository) : base(authService, repository)
        {
        }

        protected override Task Process()
        {

            return Task.CompletedTask;
        }
    }
}
