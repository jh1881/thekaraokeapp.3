﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Util;
using Microsoft.VisualBasic.FileIO;
using TKABackend.Serverless.Database;
using TKABackend.Serverless.Model;

namespace TKABackend.Serverless.Processors
{
    internal class CsvProcessor : IEventProcessor
    {
        private readonly HostRepository repository;

        public CsvProcessor(HostRepository repository)
        {
            this.repository = repository;
        }

        public async Task Process(S3EventNotification.S3Entity evt, Amazon.Lambda.Core.ILambdaContext context)
        {
            using var client = new AmazonS3Client(RegionEndpoint.EUWest2);
            try
            {
                // Parse out the hostId.
                var nameBits = evt.Object.Key.Split("/");
                var hostId = nameBits[0];

                // Find the host
                var host = await repository.GetHost(hostId);
                if (host == null) return;

                var result = await client.GetObjectAsync(new GetObjectRequest
                {
                    BucketName = evt.Bucket.Name,
                    Key = evt.Object.Key
                });

                List<Song> songs = new List<Song>();
                bool artistFirst = true;
                using (var tfp = new TextFieldParser(result.ResponseStream))
                {
                    while (!tfp.EndOfData)
                    {
                        tfp.TextFieldType = FieldType.Delimited;
                        var fields = tfp.ReadFields();
                        if(fields?.Length == 2)
                        {
                            if (fields[0].ToUpper() == "ARTIST")
                            {
                                // First line. skip it.
                                continue;
                            }
                            if(fields[0].ToUpper() == "TITLE")
                            {
                                // also first line, but switch order.
                                artistFirst = false;
                                continue;
                            }

                            string artist, title;
                            if (artistFirst) { artist = fields[0]; title = fields[1]; }
                            else { title = fields[0]; artist = fields[1]; }
                            songs.Add(new Song { Artist = artist, Title = title });
                        }
                    }
                }
                // Dedupe.
                var newSongs = songs.GroupBy(s => new { s.Title, s.Artist })
                    .Where(d => d.Count() == 1)
                    .Select(d => d.First())
                    .ToList();
                host.songs = newSongs;
                await repository.UpdateAsync(host);
            }
            finally {
                await client.DeleteObjectAsync(new DeleteObjectRequest {
                    BucketName = evt.Bucket.Name,
                    Key = evt.Object.Key
                });
            }
        }
    }
}
