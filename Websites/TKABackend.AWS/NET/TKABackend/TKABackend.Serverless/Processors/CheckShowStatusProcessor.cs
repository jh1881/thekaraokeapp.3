﻿using TKABackend.Serverless.Auth;
using TKABackend.Serverless.Database;
using TKABackend.Serverless.Model.Responses;

namespace TKABackend.Serverless.Processors
{
    internal class CheckShowStatusProcessor : BaseProcessor<CheckShowResponse>
    {
        public CheckShowStatusProcessor(IAuthService authService, HostRepository repository) : base(authService, repository)
        {
        }

        protected override Task Process()
        {
            if(AuthorisedHost == null)
            {
                StatusCode = System.Net.HttpStatusCode.Unauthorized;
                return Task.CompletedTask;
            }

            Response.IsShowInProgress = AuthorisedHost.showInProgress;
            return Task.CompletedTask;
        }
    }
}
