﻿using Amazon.Lambda.Core;
using Amazon.S3.Util;

namespace TKABackend.Serverless.Processors
{
    internal interface IEventProcessor
    {
        Task Process(S3EventNotification.S3Entity s3entity, ILambdaContext context);
    }
}
