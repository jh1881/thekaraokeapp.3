﻿using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using Amazon.Lambda.S3Events;

namespace TKABackend.Serverless.Processors
{
    internal interface IProcessor 
    {
        Task<APIGatewayProxyResponse> Process(APIGatewayProxyRequest request, ILambdaContext context);
    }
}
