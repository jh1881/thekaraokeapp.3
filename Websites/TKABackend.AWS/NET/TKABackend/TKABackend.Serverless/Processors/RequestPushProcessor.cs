﻿using TKABackend.Serverless.Auth;
using TKABackend.Serverless.Database;
using TKABackend.Serverless.Model.Requests;
using TKABackend.Serverless.Model.Responses;
using TKABackend.Serverless.Model.Validators;

namespace TKABackend.Serverless.Processors
{
    internal class RequestPushProcessor : BaseProcessor<RequestPushRequest, RequestPushResponse>
    {
        public RequestPushProcessor(IAuthService authService, HostRepository repository, IRequestValidator<RequestPushRequest> validator) : base(authService, repository, validator)
        {
        }

        protected override async Task Process()
        {
            if(RequestIsValid != true)
            {
                StatusCode = System.Net.HttpStatusCode.BadRequest;
                return;
            }

            if(!ValidateAuthority())
            {
                StatusCode = System.Net.HttpStatusCode.Unauthorized;
                return;
            }

            if(AuthorisedHost == null)
            {
                StatusCode = System.Net.HttpStatusCode.NotFound;
                return;
            }

            var deviceId = Request.DeviceId.Replace(" ", "");
            AuthorisedHost.mobileDeviceId = deviceId;
            AuthorisedHost.mobileDeviceType = Request.DeviceType;

            await SaveHost();
        }
    }
}
