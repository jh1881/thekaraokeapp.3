﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace TKABackend.Serverless.Auth
{
    internal class JWTContainerModel : IAuthContainerModel
    {
        internal static string Key = "Some stupidKey that We will never F%&%^£)*&ing use?";
        public string SecretKey { get; set; } = Key;
        public string SecurityAlgorithm { get; set; } = SecurityAlgorithms.HmacSha256Signature;
        public int ExpireMinutes { get; set; }
        public Claim[] Claims { get; set; }
    }
}
