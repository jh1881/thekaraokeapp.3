﻿using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace TKABackend.Serverless.Auth
{
    internal class JWTService : IAuthService
    {
        private readonly byte[] secretKeyBytes;

        public JWTService(string secretKey)
        {
            secretKeyBytes = Encoding.ASCII.GetBytes(Convert.ToBase64String(Encoding.ASCII.GetBytes(secretKey)));
        }

        private SecurityKey GetSymmetricSecurityKey() => new SymmetricSecurityKey(secretKeyBytes);

        private TokenValidationParameters GetTokenValidationParameters()
        {
            return new TokenValidationParameters
            {
                ValidateIssuer = false,
                ValidateAudience = false,
                IssuerSigningKey = GetSymmetricSecurityKey()
            };
        }

        public string GenerateToken(IAuthContainerModel model)
        {
            if (model?.Claims == null || model.Claims.Length == 0)
                throw new ArgumentException("model is not valid");

            var descriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(model.Claims),
                Expires = DateTime.UtcNow.AddDays(30),
                SigningCredentials = new SigningCredentials(GetSymmetricSecurityKey(), model.SecurityAlgorithm)
            };

            var handler = new JwtSecurityTokenHandler();
            var securityToken = handler.CreateToken(descriptor);
            var token = handler.WriteToken(securityToken);
            return token;
        }

        public IEnumerable<Claim> GetTokenClaims(string token)
        {
            if(string.IsNullOrEmpty(token)) throw new ArgumentException("token is null or empty");
            var parameters = GetTokenValidationParameters();
            var handler = new JwtSecurityTokenHandler();
            try
            {
                var tokenValid = handler.ValidateToken(token, parameters, out var validatedToken);
                return tokenValid.Claims;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool IsTokenValid(string token)
        {
            if (string.IsNullOrEmpty(token)) throw new ArgumentException("token is null or empty");
            var tokenValidationParameters = GetTokenValidationParameters();
            var handler = new JwtSecurityTokenHandler();
            try
            {
                ClaimsPrincipal tokenValid = handler.ValidateToken(token, tokenValidationParameters, out var validatedToken);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


    }

    public class JWTTester
    {
        private static JWTContainerModel GetJWTContainerModel(string id)
        {
            return new JWTContainerModel
            {
                Claims = new[]
                {
                    new Claim(ClaimTypes.Authentication, id),
                }
            };
        }

        public void Test()
        {
            var model = GetJWTContainerModel("2");
            var authService = new JWTService(model.SecretKey);

            var token = authService.GenerateToken(model);
            if (!authService.IsTokenValid(token)) throw new UnauthorizedAccessException();
            var claims = authService.GetTokenClaims(token).ToList();
        }
    }
}
