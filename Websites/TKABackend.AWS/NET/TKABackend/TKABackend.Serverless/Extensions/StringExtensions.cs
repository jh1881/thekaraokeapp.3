﻿namespace TKABackend.Serverless.Extensions
{
    internal static class StringExtensions
    {
        public static Stream ToStream(this string text)
        {
            var stream = new MemoryStream();
            using (var writer = new StreamWriter(stream, leaveOpen: true))
            {
                writer.Write(text);
                writer.Flush();
            }
            stream.Position = 0;
            return stream;
        }
    }
}
