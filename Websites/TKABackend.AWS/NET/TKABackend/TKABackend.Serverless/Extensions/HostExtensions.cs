﻿using TKABackend.Serverless.Model;

namespace TKABackend.Serverless.Extensions
{
    internal static class HostExtensions
    {
        public static bool HasValidSubscription(this Host host)
        {
            if (host?.subscriptions == null) return false;
            return host.subscriptions.Any(sub => sub.startDate <= DateTime.Now && (sub.endDate == null || sub.endDate > DateTime.Now));
        }
    }
}
