﻿namespace TKABackend.Serverless.Model
{
    public class Subscription
    {
        public string subscriptionId { get; set; }
        public DateTime startDate { get; set; }
        public DateTime? endDate { get; set; }
    }
}
