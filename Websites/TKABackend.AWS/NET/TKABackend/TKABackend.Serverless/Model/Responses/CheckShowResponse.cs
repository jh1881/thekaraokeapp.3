﻿namespace TKABackend.Serverless.Model.Responses
{
    internal class CheckShowResponse : IResponse
    {
        public bool IsShowInProgress { get; set; }
    }
}
