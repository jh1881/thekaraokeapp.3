﻿namespace TKABackend.Serverless.Model.Responses
{
    internal class LoginResponse : IResponse
    {
        public string? jwt { get; set; }
    }
}
