﻿namespace TKABackend.Serverless.Model.Responses
{
    internal class ChooseHostResponse : IResponse
    {
        public string? Name { get; set; }
        public string? MenuDisplayName { get; set; }
        public string? Id { get; set; }
//        public DateTime? LastSongsUpdate { get; set; }
        public string? BannerImage { get; set; }
    }
}
