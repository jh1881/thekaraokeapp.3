﻿namespace TKABackend.Serverless.Model.Responses
{
    internal class GetSongRequestNotificationsResponse : IResponse
    {
        public List<SongRequest>? requests { get; set; }
    }
}
