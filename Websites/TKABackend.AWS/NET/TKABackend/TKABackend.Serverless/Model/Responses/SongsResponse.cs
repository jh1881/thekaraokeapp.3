﻿namespace TKABackend.Serverless.Model.Responses
{
    internal abstract class SongsResponse : IResponse
    {
        public List<Song>? Songs { get; set; }
    }
}
