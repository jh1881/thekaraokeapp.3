﻿namespace TKABackend.Serverless.Model.Responses
{
    internal class AmIAHostResponse : IResponse
    {
        public string? Id { get; set; }
        public bool? Notif { get; set; }
    }
}
