﻿using Amazon.DynamoDBv2.DataModel;

namespace TKABackend.Serverless.Model
{
    public class Song
    {
        public string? Title { get; set; }
        public string? Artist { get; set; }
        public bool IsRecent { get; set; }
    }
}
