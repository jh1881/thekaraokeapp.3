﻿namespace TKABackend.Serverless.Model
{
    public class SongRequest
    {
        public string? title { get; set; }
        public string? artist { get; set; }
        public string? keyChange { get; set; }
        public string? tempoChange { get; set; }
        public string? requestor { get; set; }
        public string? duetSecond { get; set; }
        public string? groupNames { get; set; }
        public string? uid { get; set; }
        public int? gpsResult { get; set; }
        public bool? notified { get; set; }
        public DateTime? requestedTime { get; set; }
    }
}
