﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TKABackend.Serverless.Model.Requests
{
    internal class StartShowRequest : IRequest
    {
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
    }
}
