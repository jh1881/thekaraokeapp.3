﻿namespace TKABackend.Serverless.Model.Requests
{
    internal class GetSongsRequest : IRequest
    {
        public string? HostId { get; set; }
        public string? Search { get; set; }
        public bool? ExactMatch { get; set; }
        public int? Limit { get; set; }
        public bool? OrderByTitle { get; set; }
        public bool? CountOnly { get; set; }
        public bool? CuntOnly { get; set; }
    }
}
