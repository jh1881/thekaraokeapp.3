﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TKABackend.Serverless.Model.Requests
{
    internal class RequestSongRequest : IRequest
    {
        public string? Title { get; set; }
        public string? Artist { get; set; }
        public string? Requestor { get; set; }
        public string? HostId { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string? Uid { get; set; }
        public string? KeyChange { get; set; }
        public string? TempoChange { get; set; }
        public string? DuetSecond { get; set; }
        public string? GroupName { get; set; }
    }
}
