﻿namespace TKABackend.Serverless.Model.Requests
{
    internal class LoginRequest : IRequest
    {
        public string? Username { get; set; }
        public string? Password { get; set; } 
    }
}

