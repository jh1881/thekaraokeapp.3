﻿namespace TKABackend.Serverless.Model.Requests
{
    internal class GetRecentSongsRequest : IRequest
    {
        public string? HostId { get; set; }
        public bool? OrderByTitle { get; set; }
    }
}
