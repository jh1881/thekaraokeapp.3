﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TKABackend.Serverless.Model.Requests
{
    internal class RequestPushRequest : IRequest
    {
        public string? DeviceId { get; set; }
        public int DeviceType { get; set; }
    }
}
