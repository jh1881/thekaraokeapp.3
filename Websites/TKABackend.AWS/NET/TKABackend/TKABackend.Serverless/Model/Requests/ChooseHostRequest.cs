﻿namespace TKABackend.Serverless.Model.Requests
{
    internal class ChooseHostRequest : IRequest
    {
        public string? Number { get; set; }
    }
}
