﻿using TKABackend.Serverless.Model.Requests;

namespace TKABackend.Serverless.Model.Validators
{
    internal class StartShowRequestValidator : IRequestValidator<StartShowRequest>
    {
        public bool Validate(StartShowRequest? request)
        {
            if (request == null) return false;
            // They can be null.
            //if ((request.Latitude != null && request.Longitude == null)
            //    || (request.Latitude == null && request.Longitude != null))
            //    return false;

            return true;
        }
    }
}
