﻿using TKABackend.Serverless.Model.Requests;

namespace TKABackend.Serverless.Model.Validators
{
    internal class RequestPushRequestValidator : IRequestValidator<RequestPushRequest>
    {
        public bool Validate(RequestPushRequest? request)
        {
            if (request == null) return false;
            if (string.IsNullOrEmpty(request.DeviceId)) return false;
            if (request.DeviceType < 0 || request.DeviceType > 3) return false;
            return true;
        }
    }
}
