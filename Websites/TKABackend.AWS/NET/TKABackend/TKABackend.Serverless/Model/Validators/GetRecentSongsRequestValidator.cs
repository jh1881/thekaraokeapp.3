﻿using TKABackend.Serverless.Model.Requests;

namespace TKABackend.Serverless.Model.Validators
{
    internal class GetRecentSongsRequestValidator : IRequestValidator<GetRecentSongsRequest>
    {
        public bool Validate(GetRecentSongsRequest? request)
        {
            return (request?.HostId) != null;
        }
    }
}
