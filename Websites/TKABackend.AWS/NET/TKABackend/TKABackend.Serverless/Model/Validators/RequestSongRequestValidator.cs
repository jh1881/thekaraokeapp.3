﻿using TKABackend.Serverless.Model.Requests;

namespace TKABackend.Serverless.Model.Validators
{
    internal class RequestSongRequestValidator : IRequestValidator<RequestSongRequest>
    {
        public bool Validate(RequestSongRequest? request)
        {
            if (request == null) return false;

            // Need Uid, HostId, Title, Artist and Requestor at minimum
            if (request.Uid == null || request.HostId == null || request.Title == null || request.Artist == null || request.Requestor == null) return false;
            return true;
        }
    }
}
