﻿using TKABackend.Serverless.Model.Requests;

namespace TKABackend.Serverless.Model.Validators
{
    internal interface IRequestValidator<TRequest> where TRequest : class, IRequest
    {
        bool Validate(TRequest? request);
    }
}
