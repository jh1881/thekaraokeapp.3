﻿using TKABackend.Serverless.Model.Requests;

namespace TKABackend.Serverless.Model.Validators
{
    internal class ChooseHostRequestValidator : IRequestValidator<ChooseHostRequest>
    {
        public bool Validate(ChooseHostRequest? request)
        {
            return (request?.Number) != null;
        }
    }
}
