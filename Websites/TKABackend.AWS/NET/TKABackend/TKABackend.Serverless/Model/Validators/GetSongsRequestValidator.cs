﻿using TKABackend.Serverless.Model.Requests;

namespace TKABackend.Serverless.Model.Validators
{
    internal class GetSongsRequestValidator : IRequestValidator<GetSongsRequest>
    {
        public bool Validate(GetSongsRequest? request)
        {
            if (request == null) return false;

            // Minimum needed is HostId
            if (request.HostId == null) return false;

            return true;
        }
    }
}
