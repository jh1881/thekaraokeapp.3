﻿using TKABackend.Serverless.Model.Requests;

namespace TKABackend.Serverless.Model.Validators
{
    internal class LoginRequestValidator : IRequestValidator<LoginRequest>
    {
        public bool Validate(LoginRequest? request)
        {
            if (request == null) return false;
            if (string.IsNullOrWhiteSpace(request.Username)) return false;
            if (string.IsNullOrWhiteSpace(request.Password)) return false;
            return true;
        }
    }
}
