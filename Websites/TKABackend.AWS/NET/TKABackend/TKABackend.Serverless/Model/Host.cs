﻿using Amazon.DynamoDBv2.DataModel;

namespace TKABackend.Serverless.Model
{
    [DynamoDBTable("hosts")]
    public class Host
    {
        [DynamoDBHashKey]
        public string? hostGuid { get; set; }
        public string? number { get; set; }
        public int maxSongRequestsPerHour { get; set; }
        public bool showInProgress { get; set; }
        public int mobileDeviceType { get; set; }
        public bool receiveEmailNotifications { get; set; }
        public string? name { get; set; }
        public bool receiveMobileNotifications { get; set; }
        public DateTime subscriptionExpiry { get; set; }
        public string? mobileDeviceId { get; set; }
        public int permittedGpsDistance { get; set; }
        public string? emailAddress { get; set; }
        public string? menuDisplayName { get; set; }
        public string? pwd { get; set; }
        public string? loginName { get; set; }
        public int songsPerPage { get; set; }
        public string? bannerImageb64 { get; set; }

        public List<Song>? songs { get; set; }
        public List<SongRequest>? songRequests { get; set; }
        public List<Show>? shows { get; set; }
        public List<Subscription>? subscriptions { get; set; }
    }
}
