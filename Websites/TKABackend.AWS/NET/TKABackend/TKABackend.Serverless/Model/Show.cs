﻿namespace TKABackend.Serverless.Model
{
    public class Show
    {
        public DateTime? showStart { get; set; }
        public DateTime? showEnd { get; set; }
        public double? latitude { get; set; }
        public double? longitude { get; set; }
    }
}
