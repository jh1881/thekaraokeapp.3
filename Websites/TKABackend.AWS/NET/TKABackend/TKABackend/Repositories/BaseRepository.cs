﻿using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using TKABackend.Model;

namespace TKABackend.Repositories
{
    internal abstract class BaseRepository<T> : IRepository<T> where T : class
    {
        protected DBConfig dbConfig;

        protected BaseRepository()
        {
            dbConfig = new DBConfig();
        }

        public virtual Task AddAsync(T newItem)
        {
            return dbConfig.Context.SaveAsync(newItem);
        }

        public virtual Task DeleteAsync(T newItem)
        {
            return dbConfig.Context.DeleteAsync(newItem);
        }

        public virtual Task UpdateAsync(T newItem)
        {
            return dbConfig.Context.SaveAsync(newItem);
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var table = dbConfig.Context.GetTargetTable<T>();

            var results = table.Scan(new ScanOperationConfig());
            var data = await results.GetRemainingAsync();
            return dbConfig.Context.FromDocuments<T>(data);
        }

        public async Task<T?> GetAsync(string id)
        {
            var conds = new List<ScanCondition>
            {
                new ScanCondition("hostGuid", ScanOperator.Equal, id)
            };
            List<T>? results = await dbConfig.Context.ScanAsync<T>(conds).GetRemainingAsync();
            return results?.FirstOrDefault();
        }
    }
}
