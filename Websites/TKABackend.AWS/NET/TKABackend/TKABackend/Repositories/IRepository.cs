﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TKABackend.Repositories
{
    internal interface IRepository<T> where T: class
    {
        Task AddAsync(T newItem);
        Task UpdateAsync(T newItem);
        Task DeleteAsync(T newItem);

        Task<IEnumerable<T>> GetAllAsync();
        Task<T> GetAsync(string id);
    }
}
