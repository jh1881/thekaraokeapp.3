﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TKABackend.Model;

namespace TKABackend.Repositories
{
    internal class HostRepository : BaseRepository<Host>
    {
        public HostRepository() : base()
        {
        }
    }
}
