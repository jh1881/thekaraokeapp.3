﻿using Amazon.DynamoDBv2.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TKABackend.Model
{
    [DynamoDBTable("hosts")]
    public class Host
    {
        [DynamoDBHashKey]
        public string hostGuid { get; set; }
        public int maxSongRequestsPerHour { get; set; }
        public bool showInProgress { get; set; }
        public int mobileDeviceType { get; set; }
        public bool receiveEmailNotifications { get; set; }
        public string? name { get; set; }
        public bool receiveMobileNotifications { get; set; }
        public DateTime subscriptionExpiry { get; set; }
        public string? mobileDeviceId { get; set; }
        public int permittedGpsDistance { get; set; }
        public string? emailAddress { get; set; }
        public string? menuDisplayName { get; set; }
        public string? pwd { get; set; }
        public string? loginName { get; set; }
        public int songsPerPage { get; set; }
    }
}
