using Amazon.Lambda.Core;
using TKABackend.Model;
using TKABackend.Repositories;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace TKABackend;

public class Function
{
    private HostRepository hostRepository;

    public Function()
    {
        hostRepository = new HostRepository();
    }

    public async Task<Host> FunctionHandler(ILambdaContext context)
    {
        var testHost = new Host {
            hostGuid = Guid.NewGuid().ToString(),
        };
        await hostRepository.AddAsync(testHost);

        return testHost;
    }
}
