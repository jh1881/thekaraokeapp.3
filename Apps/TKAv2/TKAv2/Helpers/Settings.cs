﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Newtonsoft.Json;
using TKAv2.Models;
using Xamarin.Essentials;

namespace TKAv2.Helpers
{
    public static class Settings
    {
        public static Guid Id
        {
            get
            {
                var id = GetAndLog("GuidId", default(Guid));
                if (id == default(Guid))
                {
                    SetAndLog("GuidId", Guid.NewGuid());
                    id = GetAndLog("GuidId", default(Guid));
                }
                return id;
            }
        }

        public static bool NonPersonalisedAds
        {
            get => GetAndLog("NotPersonalisedAds", false);
            set => SetAndLog("NotPersonalisedAds", value);
        }

        public static bool HasSeenAdsMessage
        {
            get => GetAndLog("HasSeenAdsMessage", false);
            set => SetAndLog("HasSeenAdsMessage", value);
        }

        public static bool PreserveSingerName
        {
            get => GetAndLog("PreserveSingerName", true);
            set => SetAndLog("PreserveSingerName", value);
        }

        public static Dictionary<string, DateTime> HostSongUpdates
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<Dictionary<string, DateTime>>(GetAndLog("HostSongUpdates", ""));
                }
                catch
                {
                    return new Dictionary<string, DateTime>();
                }
            }
            set => SetAndLog("HostSongUpdates", JsonConvert.SerializeObject(value));
        }

        public static bool ExactMatch
        {
            get => GetAndLog("ExactMatch", false);
            set => SetAndLog("ExactMatch", value);
        }

        public static bool TitleFirst
        {
            get => GetAndLog("TitleFirst", false);
            set => SetAndLog("TitleFirst", value);
        }

        public static string JWT
        {
            get => GetAndLog("JWT", "");
            set => SetAndLog("JWT", value);
        }

        public static List<SongRequest> CurrentSongRequests
        {
            get => JsonConvert.DeserializeObject<List<SongRequest>>(GetAndLog("SongRequests", "")) ?? new List<SongRequest>();
            set => SetAndLog("SongRequests", JsonConvert.SerializeObject(value));
        }

        public static List<RequestedSong> LastSongsRequested
        {
            get => JsonConvert.DeserializeObject<List<RequestedSong>>(GetAndLog("LastSongsRequested", "")) ?? new List<RequestedSong>();
            set => SetAndLog("LastSongsRequested", JsonConvert.SerializeObject(value));
        }

        public static string LastRequestName
        {
            get => GetAndLog("LastRequestName", "");
            set => SetAndLog("LastRequestName", value);
        }

        private static void SetAndLog<T>(string name, T val)
        {
            Debug.WriteLine($"Settings.SetAndLog name={name}, val={val}");
            Preferences.Set(name, JsonConvert.SerializeObject(val));
        }

        private static T GetAndLog<T>(string name, T defaultValue = default(T))
        {
            Debug.WriteLine($"Settings.GetAndLog name={name}");
            var val = Preferences.Get(name, null);
            Debug.WriteLine($"Settings.GetAndLog name={name}, val={val}");
            if (val == null) return defaultValue;
            return JsonConvert.DeserializeObject<T>(val);
        }
    }
}
