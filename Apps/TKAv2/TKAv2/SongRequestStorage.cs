﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Acr.UserDialogs;
using TKAv2.Interfaces;
using TKAv2.Models;
using TKAv2.Services;
using Xamarin.Forms;

namespace TKAv2
{
    public static class SongRequestStorage
    {
        public class SongRequestsReceivedArgs : EventArgs
        {
            public int NewSongRequestsReceived { get; }

            public SongRequestsReceivedArgs(int newSongRequestsReceived)
            {
                NewSongRequestsReceived = newSongRequestsReceived;
            }
        }

        private static readonly List<SongRequest> _songRequests = new List<SongRequest>();
        public static IEnumerable<SongRequest> SongRequests => _songRequests;

        public static event EventHandler<SongRequestsReceivedArgs> OnNewSongRequestsReceived;

        // On Init, we get the current song requests.
        // We then allow other tasks (such as the background task) to add new songs or update
        //   existing songs.
        public static async Task Init()
        {
            Debug.WriteLine("SongRequestStorage.Init started");
            try
            {
                // Load the current set from file.
                var requests = (Helpers.Settings.CurrentSongRequests ?? new List<SongRequest>())
                    .Where(s => s.RequestedTime.Date < DateTime.Today);
                _songRequests.AddRange(requests);
                SaveChanges();
                Debug.WriteLine($"SongRequestStorage.Init has {_songRequests.Count} current song requests");

                // Now update the requests.
                await Update();
            }
            catch (Exception e)
            {
                Debug.WriteLine($"SongRequestStorage.Init got exception: {e}");
                MessagingCenter.Send(new ErrorMessage { ErrorText = $"Exception: {e.Message}" }, "");
            }
            Debug.WriteLine("SongRequestStorage.Init ended");
        }

        public static void Clear()
        {
            _songRequests.Clear();
            Helpers.Settings.CurrentSongRequests = new List<SongRequest>();
        }

        private static bool _isUpdating;
        private static IComms _comms;
        private static IComms Comms => _comms ?? (_comms = DependencyService.Get<IComms>());

        public static void SaveChanges()
        {
            // Should this write to a "proper" database? If the list of large, it would probably be help, but
            // it's unlikely to get that large really :|
            Helpers.Settings.CurrentSongRequests = _songRequests;
        }

        // Returns the number of new songs.
        public static async Task<int> Update()
        {
            Debug.WriteLine("SongRequestStorage.Update started");
            if (_isUpdating) return 0;
            var newSongCount = 0;
            _isUpdating = true;
            try
            {
                var result = await Comms.UnreadSongRequests();
                Debug.WriteLine($"SongRequestStorage.Update UnreadNotifications got {result}");
                if (result.IsSuccess)
                {
                    List<SongRequest> requests;
                    try
                    {
                        requests = result.Deserialise<List<SongRequest>>();
                    }
                    catch (Exception)
                    {
                        App.Current.Error("Failed to contact host server. Please check your internet connection and try again later.");
                        newSongCount = -1;
                        return newSongCount;
                    }

                    DependencyService.Get<IPushNotifications>()?.ClearBadge();

                    foreach (var existingSong in requests)
                    {
                        var song = _songRequests.FirstOrDefault(s => s.Id == existingSong.Id);
                        if (song != null)
                        {
                            song.Cancelled = existingSong.Cancelled;
                        }
                        else
                        {
                            _songRequests.Add(existingSong);
                            newSongCount++;
                        }
                    }
                    if (newSongCount > 0)
                    {
                        var toast = new ToastConfig("New song requests received!") { BackgroundColor = Color.Pink, MessageTextColor = Color.Black };
                        UserDialogs.Instance.Toast(toast);
                        //                        DependencyService.Get<IToast>()?.ShowToast("New song requests received", 250);
                        OnNewSongRequestsReceived?.Invoke(null, new SongRequestsReceivedArgs(newSongCount));
                    }

                    SaveChanges();
                }
                else
                {
                    await HandleNonSuccess(result);
                    newSongCount = -1;
                }
            }
            finally
            {
                _isUpdating = false;
            }
            Debug.WriteLine($"SongRequestStorage.Update ended with newSongCount = {newSongCount}");
            return newSongCount;
        }

        private static async Task HandleNonSuccess(CommsResult result)
        {
            string errorText;
            if (result.NoConnection)
                errorText = "No connection available.";
            else if (result.Exception != null)
                errorText = $"Exception: {result.Exception.Message}";
            else
                errorText = $"Error: {result.StatusCode} {result.RawResponse}";
            await UserDialogs.Instance.AlertAsync(errorText, "The Karaoke App");
        }
    }

    public class ErrorMessage
    {
        public string ErrorText { get; set; }
    }
}
