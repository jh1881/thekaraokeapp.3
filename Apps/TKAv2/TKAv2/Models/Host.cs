﻿using System;

namespace TKAv2.Models
{
    public class Host
    {
        public string Name { get; set; }
        public string MenuDisplayName { get; set; }
        public string Id { get; set; }
        public string Number { get; set; }
        public DateTime? LastSongsUpdate { get; set; }
        public string BannerImage { get; set; }
    }
}
