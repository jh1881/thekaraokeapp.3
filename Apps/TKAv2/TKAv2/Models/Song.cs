﻿namespace TKAv2.Models
{
    public class Song
    {
        public string Artist { get; set; }
        public string Title { get; set; }
        public int Id { get; set; }
    }
}
