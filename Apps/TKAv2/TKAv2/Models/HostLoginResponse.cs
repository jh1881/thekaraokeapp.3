﻿namespace TKAv2.Models
{
    public class HostLoginResponse
    {
        public string jwt { get; set; }
    }
}
