﻿//using Newtonsoft.Json;

using System;
using System.IO;
using System.Linq.Expressions;
using System.Text;
using Xamarin.Forms;
using TKAv2.ViewModels;
using System.Text.Json;
using TKAv2.Extensions;
using Xamarin.Forms.Svg;

namespace TKAv2.Models
{
    public class RuntimeData : BaseViewModel
    {
        public static RuntimeData TheRuntimeData { get; } = new RuntimeData();
        public class ImageCredit
        {
            public string MadeByName { get; set; }
            public string MadeByUri { get; set; }
            public string FromName { get; set; }
            public string FromUri { get; set; }
            public string LicenseName { get; set; }
            public string LicenseUri { get; set; }
        }

        public class RuntimeTheme
        {
            public ImageSource MicrophoneWhite { get; set; }
            public ImageSource MicrophoneBlack { get; set; }
            public ImageSource AvatarWhite { get; set; }
            public ImageSource AvatarBlack { get; set; }
            public ImageSource RepeatWhite { get; set; }
            public ImageSource RepeatBlack { get; set; }
            public ImageSource SettingsWhite { get; set; }
            public ImageSource SettingsBlack { get; set; }
            public ImageSource MusicWhite { get; set; }
            public ImageSource MusicBlack { get; set; }
            public ImageSource NewWhite { get; set; }
            public ImageSource NewBlack { get; set; }
            public ImageSource EnterWhite { get; set; }
            public ImageSource EnterBlack { get; set; }
            public ImageSource PlaceholderWhite { get; set; }
            public ImageSource PlaceholderBlack { get; set; }
            public ImageSource InfoWhite { get; set; }
            public ImageSource InfoBlack { get; set; }

            public ImageCredit MicrophoneCredit { get; set; }
            public ImageCredit AvatarCredit { get; set; }
            public ImageCredit RepeatCredit { get; set; }
            public ImageCredit SettingsCredit { get; set; }
            public ImageCredit MusicCredit { get; set; }
            public ImageCredit NewCredit { get; set; }
            public ImageCredit EnterCredit { get; set; }
            public ImageCredit PlaceholderCredit { get; set; }
            public ImageCredit InfoCredit { get; set; }
        }

        private class RuntimeThemeData : BaseViewModel
        {
            public ImageCredit MicrophoneCredit { get; set; }
            public ImageCredit AvatarCredit { get; set; }
            public ImageCredit RepeatCredit { get; set; }
            public ImageCredit SettingsCredit { get; set; }
            public ImageCredit MusicCredit { get; set; }
            public ImageCredit NewCredit { get; set; }
            public ImageCredit EnterCredit { get; set; }
            public ImageCredit PlaceholderCredit { get; set; }
            public ImageCredit InfoCredit { get; set; }

            public string MicrophoneSvgB64 { get; set; }
            public string AvatarSvgB64 { get; set; }
            public string RepeatSvgB64 { get; set; }
            public string SettingsSvgB64 { get; set; }
            public string MusicSvgB64 { get; set; }
            public string NewSvgB64 { get; set; }
            public string EnterSvgB64 { get; set; }
            public string PlaceholderSvgB64 { get; set; }
            public string InfoSvgB64 { get; set; }
        }

        private ImageSource _hostImage;
        private string _hostName;
        public int? FixedHost { get; set; }
        public int? HostNum { get; set; }
        public string ChosenHostId { get; set; }
        public RuntimeTheme Theme { get; set; } = new RuntimeTheme();

        public string HostName
        {
            get => _hostName;
            set => SetProperty(ref _hostName, value);
        }

        public ImageSource HostImage
        {
            get => _hostImage;
            set => SetProperty(ref _hostImage, value);
        }

        public bool ShowAds { get; set; } = true;
        public string MainAboutText { get; set; }

        public void LoadImages(string jsonData)
        {
            var themeData = JsonSerializer.Deserialize<RuntimeThemeData>(jsonData);
            Theme.MicrophoneCredit = themeData.MicrophoneCredit;
            Theme.AvatarCredit = themeData.AvatarCredit;
            Theme.RepeatCredit = themeData.RepeatCredit;
            Theme.SettingsCredit = themeData.SettingsCredit;
            Theme.MusicCredit = themeData.MusicCredit;
            Theme.NewCredit = themeData.NewCredit;
            Theme.EnterCredit = themeData.EnterCredit;
            Theme.PlaceholderCredit = themeData.PlaceholderCredit;
            Theme.InfoCredit = themeData.InfoCredit;

            LoadImage(theme => theme.MicrophoneBlack, themeData.MicrophoneSvgB64, Color.Black);
            LoadImage(theme => theme.MicrophoneWhite, themeData.MicrophoneSvgB64, Color.White);
            LoadImage(theme => theme.AvatarBlack, themeData.AvatarSvgB64, Color.Black);
            LoadImage(theme => theme.AvatarWhite, themeData.AvatarSvgB64, Color.White);
            LoadImage(theme => theme.RepeatBlack, themeData.RepeatSvgB64, Color.Black);
            LoadImage(theme => theme.RepeatWhite, themeData.RepeatSvgB64, Color.White);
            LoadImage(theme => theme.SettingsBlack, themeData.SettingsSvgB64, Color.Black);
            LoadImage(theme => theme.SettingsWhite, themeData.SettingsSvgB64, Color.White);
            LoadImage(theme => theme.MusicBlack, themeData.MusicSvgB64, Color.Black);
            LoadImage(theme => theme.MusicWhite, themeData.MusicSvgB64, Color.White);
            LoadImage(theme => theme.NewBlack, themeData.NewSvgB64, Color.Black);
            LoadImage(theme => theme.NewWhite, themeData.NewSvgB64, Color.White);
            LoadImage(theme => theme.EnterBlack, themeData.EnterSvgB64, Color.Black);
            LoadImage(theme => theme.EnterWhite, themeData.EnterSvgB64, Color.White);
            LoadImage(theme => theme.PlaceholderBlack, themeData.PlaceholderSvgB64, Color.Black);
            LoadImage(theme => theme.PlaceholderWhite, themeData.PlaceholderSvgB64, Color.White);
            LoadImage(theme => theme.InfoBlack, themeData.InfoSvgB64, Color.Black);
            LoadImage(theme => theme.InfoWhite, themeData.InfoSvgB64, Color.White);
        }

        private void LoadImage(Expression<Func<RuntimeTheme, ImageSource>> expression, string base64Data, Color color, int width = 32, int height = 32)
        {
            if (string.IsNullOrWhiteSpace(base64Data)) return;
            var decoded = Convert.FromBase64String(base64Data);
            // Load this into an SvgImageSource
            var src = SvgImageSource.FromSvgStream(() => new MemoryStream(decoded), width, height, color);
            Theme.SetPropertyValue(expression, src);
        }
    }
}
