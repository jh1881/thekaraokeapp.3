﻿using Xamarin.Forms;

namespace TKAv2.Controls
{
    class AdvancedFlyoutItem : FlyoutItem
    {
        public static readonly BindableProperty SelectedFlyoutIconProperty = BindableProperty.Create(nameof(SelectedFlyoutIcon), typeof(ImageSource), typeof(AdvancedFlyoutItem));

        public ImageSource SelectedFlyoutIcon
        {
            get => (ImageSource)GetValue(SelectedFlyoutIconProperty);
            set => SetValue(SelectedFlyoutIconProperty, value);
        }
    }
}
