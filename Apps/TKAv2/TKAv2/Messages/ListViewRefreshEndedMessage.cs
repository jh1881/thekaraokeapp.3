﻿namespace TKAv2.Messages
{
    internal class ListViewRefreshEndedMessage
    {
        internal const string Songs = "Songs";
        internal const string SongRequests = "SongRequests";
    }
}
