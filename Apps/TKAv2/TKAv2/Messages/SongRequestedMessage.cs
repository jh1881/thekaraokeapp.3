﻿using TKAv2.Models;

namespace TKAv2.Messages
{
    class SongRequestedMessage
    {
        public SongRequest Request { get; }
        public SongRequestedMessage(SongRequest request)
        {
            Request = request;
        }
    }
}
