﻿using TKAv2.Models;

namespace TKAv2.Messages
{
    class SongTappedMessage
    {
        public Song Song { get; }

        public SongTappedMessage(Song song)
        {
            Song = song;
        }
    }
}
