﻿using TKAv2.Models;

namespace TKAv2.Messages
{
    internal class HostChosenMessage
    {
        public Host Host { get; }
        public HostChosenMessage(Host host) => Host = host;
    }
}
