﻿using System.Windows.Input;
using Acr.UserDialogs;
using Xamarin.Forms;

namespace TKAv2.ViewModels
{
    public class FixedHostPageViewModel : BaseHostViewModel
    {
        public HostPageViewModel HostPageViewModel { get; }
        private ICommand _loginCommand;

        public string Username { get; set; }
        public string Password { get; set; }

        public ICommand LoginCommand => _loginCommand ?? (_loginCommand = new Command(DoLogin));

        public FixedHostPageViewModel()
        {
            HostPageViewModel = new HostPageViewModel();
        }

        private async void DoLogin()
        {
            if (string.IsNullOrEmpty(Username) || string.IsNullOrEmpty(Password))
            {
                await UserDialogs.Instance.AlertAsync("Please fill in both User Name and Password.", "The Karaoke App");
                return;
            }

            await DoLogin(Username, Password);
        }
    }
}
