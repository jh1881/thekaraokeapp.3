﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using TKAv2.Helpers;
using TKAv2.Messages;
using TKAv2.Models;
using Xamarin.Forms;

namespace TKAv2.ViewModels
{
    public class PreviousSongRequestsPageViewModel : BaseViewModel
    {
        //private ICommand _songRequestTapped;
        private ICommand _songTappedCommand;
        private ObservableCollection<RequestedSong> _previousRequests = new ObservableCollection<RequestedSong>(Settings.LastSongsRequested);

        public bool HasSongs
        {
            get
            {
                var b = PreviousRequests.Any();
                return b;
            }
        }

        public bool ShowAds => RuntimeData.TheRuntimeData.ShowAds;

        public ObservableCollection<RequestedSong> PreviousRequests
        {
            get => _previousRequests;
            set
            {
                SetProperty(ref _previousRequests, value);
                OnPropertyChanged(nameof(HasSongs));
            }
        }

        public string AdUnitId
        {
            get
            {
                switch (Device.RuntimePlatform)
                {
                    case Device.iOS: return "ca-app-pub-8064175537251762/7121828858";
                    case Device.Android: return "ca-app-pub-8064175537251762/5376594503";
                    default: return "";
                }
            }
        }

        public PreviousSongRequestsPageViewModel()
        {
            PreviousRequests = new ObservableCollection<RequestedSong>(Settings.LastSongsRequested);
            MessagingCenter.Subscribe<SongRequestedMessage>(this, "", msg =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    PreviousRequests = new ObservableCollection<RequestedSong>(Settings.LastSongsRequested);
                });
            });
        }

        public ICommand SongTappedCommand => _songTappedCommand ?? (_songTappedCommand = new Command(async o =>
        {
            if (o is RequestedSong s)
            {
                await Shell.Current.GoToAsync($"{Routes.RequestRoute}?title={s.Song.Title}&artist={s.Song.Artist}" +
                                              $"&tempochange={s.TempoChange}&keychange={s.KeyChange}");
            }
        }));
    }
}
