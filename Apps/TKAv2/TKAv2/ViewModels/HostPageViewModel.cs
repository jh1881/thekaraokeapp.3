﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Acr.UserDialogs;
using TKAv2.Helpers;
using TKAv2.Messages;
using Xamarin.Forms;
using TKAv2.Models;
//using ZXing;

namespace TKAv2.ViewModels
{
    public class HostPageViewModel : BaseHostViewModel, IDisposable
    {
        private ICommand _loginCommand;
        private ICommand _chooseHostCommand;
        private ICommand _scanResultCommand;
        private ICommand _purgeMarkedCommand;

        public ICommand LoginCommand => _loginCommand ?? (_loginCommand = new Command(async () =>
        {
            var config = new LoginConfig { Message  = "Please login", Title = "The Karaoke App"};

            var result = await UserDialogs.Instance.LoginAsync(config);
            if (!result.Ok) 
                return;
            if (string.IsNullOrEmpty(result.LoginText) || string.IsNullOrEmpty(result.Password))
            {
                await UserDialogs.Instance.AlertAsync("Please fill in both User Name and Password.", "The Karaoke App");
                return;
            }
            await DoLogin(result.LoginText, result.Password);
        }));

        public ICommand ChooseHostCommand => _chooseHostCommand ?? (_chooseHostCommand = new Command(async () =>
        {
            if(string.IsNullOrEmpty(HostId1))
            {
                await UserDialogs.Instance.AlertAsync("Enter a Host Id, or Scan a Host Code", "The Karaoke App");
                return;
            }

            if (!int.TryParse(HostId1, out var hostId))
            {
                await UserDialogs.Instance.AlertAsync("Invalid host id. Please scan a host code", "The Karaoke App");
                return;
            }

            using (var ldg = UserDialogs.Instance.Loading())
            {
                var result = await Comms.ChooseHost(HostId);
                if (!result.IsSuccess)
                {
                    ldg.Dispose();
                    await UserDialogs.Instance.AlertAsync("Could not choose that host.", "The Karaoke App");
                    return;
                }

                var host = result.Deserialise<Host>();
                Data.HostName = host.Name;
                Data.ChosenHostId = host.Id;

                MessagingCenter.Send(new HostChosenMessage(host), "");
                // Switch to Songs view.
                await Shell.Current.GoToAsync(Routes.AllSongsRoute);
            }
        }));

        //public ICommand ScanResultCommand => _scanResultCommand ?? (_scanResultCommand = new Command(args =>
        //{
        //    if (args is Result r)
        //    {
        //        HostId = GetNumbers(r.Text.ToLower());
        //    }
        //}));

        #region HostId numbers
        private string _hostId1;
        private string _hostId2;
        private string _hostId3;
        private string _hostId4;
        private string _hostId5;
        private string _hostId6;
        public string HostId
        {
            get => $"{_hostId1}{_hostId2}{_hostId3}{_hostId4}{_hostId5}{_hostId6}";
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _hostId6 = _hostId5 = _hostId4 = _hostId3 = _hostId2 = _hostId1 = null;
                    return;
                }

                for (int i = 0; i < value.Length; i++)
                {
                    var s = value.Substring(i, 1);
                    switch (i)
                    {
                        case 0: HostId1 = s; break;
                        case 1: HostId2 = s; break;
                        case 2: HostId3 = s; break;
                        case 3: HostId4 = s; break;
                        case 4: HostId5 = s; break;
                        case 5: HostId6 = s; break;
                    }
                }
            }
        }
        public string HostId1
        {
            get => _hostId1;
            set => SetProperty(ref _hostId1, value);
        }

        public string HostId2
        {
            get => _hostId2;
            set => SetProperty(ref _hostId2, value);
        }

        public string HostId3
        {
            get => _hostId3;
            set => SetProperty(ref _hostId3, value);
        }

        public string HostId4
        {
            get => _hostId4;
            set => SetProperty(ref _hostId4, value);
        }

        public string HostId5
        {
            get => _hostId5;
            set => SetProperty(ref _hostId5, value);
        }

        public string HostId6
        {
            get => _hostId6;
            set => SetProperty(ref _hostId6, value);
        }
        #endregion

        public static string GetNumbers(string input)
        {
            return input == null ? input : new string(input.Where(char.IsDigit).ToArray());
        }

        public ICommand PurgeMarkedCommand => _purgeMarkedCommand ?? (_purgeMarkedCommand = new Command(PurgeMarked));

        public HostPageViewModel()
        {
            MessagingCenter.Subscribe<LoggedInMessage>(this, "", async msg =>
            {
                if (!string.IsNullOrEmpty(Comms.LoginToken))
                {
                    await SongRequestStorage.Init();
                    GatherRequests();
                }
            });

            SongRequestStorage.OnNewSongRequestsReceived += (sender, args) => GatherRequests();
        }

        ~HostPageViewModel()
        {
            ReleaseUnmanagedResources();
        }

        public void PurgeMarked()
        {
            // Purge everything marked, then save it, then refresh the list.
            foreach (var s in SongRequests)
            {
                if (s.Marked && !s.Purged) s.Purged = true;
            }
            SongRequestStorage.SaveChanges();
            GatherRequests();
        }

        private void GatherRequests()
        {
            SongRequests = new ObservableCollection<SongRequest>(SongRequestStorage.SongRequests.Where(p => !p.Purged).ToList());
        }

        private void ReleaseUnmanagedResources()
        {
            MessagingCenter.Unsubscribe<LoggedInMessage>(this, "");
        }

        public void Dispose()
        {
            ReleaseUnmanagedResources();
            GC.SuppressFinalize(this);
        }
    }
}
