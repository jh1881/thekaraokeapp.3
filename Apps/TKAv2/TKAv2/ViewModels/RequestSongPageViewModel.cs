﻿using System.Threading.Tasks;
using System.Windows.Input;
using Acr.UserDialogs;
using TKAv2.Helpers;
using TKAv2.Messages;
using TKAv2.Services;
using Xamarin.Forms;

namespace TKAv2.ViewModels
{
    [QueryProperty("Title", "title")]
    [QueryProperty("Artist", "artist")]
    [QueryProperty("TempoChange", "tempochange")]
    [QueryProperty("KeyChange", "keychange")]
    public class RequestSongPageViewModel : BaseViewModel
    {
        private ICommand _requestSongCommand;
        private readonly TheKaraokeAppServerComms _comms;
        private string _selectedRequestType = "Solo";

        public string Title { get; set; }
        public string Artist { get; set; }
        public string Singer { get; set; } = Helpers.Settings.LastRequestName;
        public string DuetName { get; set; }
        public string GroupName { get; set; }
        public string TempoChange { get; set; }
        public string KeyChange { get; set; }

        public string SelectedRequestType
        {
            get => _selectedRequestType;
            set
            {
                if (SetProperty(ref _selectedRequestType, value))
                {
                    ShowDuet = ShowGroup = false;
                    switch (value)
                    {
                        case "Solo": break;
                        case "Duet": ShowDuet = true; break;
                        case "Group": ShowGroup = true; break;
                    }
                }
            }
        }

        public bool ShowDuet { get; set; }
        public bool ShowGroup { get; set; }

        public ICommand RequestSongCommand => _requestSongCommand ?? (_requestSongCommand = new Command(async () => await RequestSong()));

        public RequestSongPageViewModel()
        {
            _comms = DependencyService.Resolve<TheKaraokeAppServerComms>();

            MessagingCenter.Subscribe<SongTappedMessage>(this, "", msg =>
            {
                Title = msg.Song.Title;
                Artist = msg.Song.Artist;
                SelectedRequestType = "Solo";
                GroupName = "";
                DuetName = "";
                TempoChange = "";
                KeyChange = "";
            });
        }

        private async Task RequestSong()
        {
            if (string.IsNullOrEmpty(Title) || string.IsNullOrEmpty(Artist) || string.IsNullOrEmpty(Singer))
            {
                await UserDialogs.Instance.AlertAsync("Please enter Title, Artist and Singer name", "The Karaoke App");
                return;
            }

            if (ShowDuet && string.IsNullOrEmpty(DuetName))
            {
                await UserDialogs.Instance.AlertAsync("Please enter Duet Name", "The Karaoke App");
                return;
            }

            if (ShowGroup && string.IsNullOrEmpty(GroupName))
            {
                await UserDialogs.Instance.AlertAsync("Please enter Group Name", "The Karaoke App");
                return;
            }

            var result = await _comms.RequestSong(Title, Artist, Singer, DuetName, GroupName, TempoChange, KeyChange);
            if (result == RequestSongResult.Success)
            {
                Helpers.Settings.LastRequestName = Singer;
                Title = "";
                Artist = "";
                TempoChange = "";
                KeyChange = "";

                await Shell.Current.GoToAsync(Routes.AllSongsRoute);
            }
        }
    }
}
