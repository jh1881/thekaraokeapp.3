﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using TKAv2.Helpers;
using TKAv2.Models;
using Xamarin.Forms;

namespace TKAv2.ViewModels
{
    public class SettingsPageViewModel : BaseViewModel
    {
        private bool _exactSearchMatch;
        private bool _orderByTitleFirst;

        public bool ExactSearchMatch
        {
            get => _exactSearchMatch;
            set
            {
                _exactSearchMatch = value;
                OnPropertyChanged();
                Settings.ExactMatch = value;
            }
        }

        public bool OrderByTitleFirst
        {
            get => _orderByTitleFirst;
            set
            {
                _orderByTitleFirst = value;
                OnPropertyChanged();
                Settings.TitleFirst = true;
            }
        }

        public class ImageInfo
        {
            public ImageSource ImageSource { get; set; }
            public RuntimeData.ImageCredit ImageCredit { get; set; }
        }

        public ObservableCollection<ImageInfo> ImageCredits { get; set; } = new ObservableCollection<ImageInfo>();

        public SettingsPageViewModel()
        {
            _exactSearchMatch = Settings.ExactMatch;
            _orderByTitleFirst = Settings.TitleFirst;
        }

        public string MainAboutText => RuntimeData.TheRuntimeData.MainAboutText;

        public void OnAppearing()
        {
            var sw = Stopwatch.StartNew();
            try { 
                ImageCredits.Clear();

                ImageCredits.Add(new ImageInfo { ImageCredit = RuntimeData.TheRuntimeData.Theme.MicrophoneCredit, ImageSource = RuntimeData.TheRuntimeData.Theme.MicrophoneWhite });
                ImageCredits.Add(new ImageInfo { ImageCredit = RuntimeData.TheRuntimeData.Theme.AvatarCredit, ImageSource = RuntimeData.TheRuntimeData.Theme.AvatarWhite });
                ImageCredits.Add(new ImageInfo { ImageCredit = RuntimeData.TheRuntimeData.Theme.RepeatCredit, ImageSource = RuntimeData.TheRuntimeData.Theme.RepeatWhite });
                ImageCredits.Add(new ImageInfo { ImageCredit = RuntimeData.TheRuntimeData.Theme.SettingsCredit, ImageSource = RuntimeData.TheRuntimeData.Theme.SettingsWhite });
                ImageCredits.Add(new ImageInfo { ImageCredit = RuntimeData.TheRuntimeData.Theme.MusicCredit, ImageSource = RuntimeData.TheRuntimeData.Theme.MusicWhite });
                ImageCredits.Add(new ImageInfo { ImageCredit = RuntimeData.TheRuntimeData.Theme.NewCredit, ImageSource = RuntimeData.TheRuntimeData.Theme.NewWhite });
                ImageCredits.Add(new ImageInfo { ImageCredit = RuntimeData.TheRuntimeData.Theme.EnterCredit, ImageSource = RuntimeData.TheRuntimeData.Theme.EnterWhite });
                ImageCredits.Add(new ImageInfo { ImageCredit = RuntimeData.TheRuntimeData.Theme.PlaceholderCredit, ImageSource = RuntimeData.TheRuntimeData.Theme.PlaceholderWhite });
                ImageCredits.Add(new ImageInfo { ImageCredit = RuntimeData.TheRuntimeData.Theme.InfoCredit, ImageSource = RuntimeData.TheRuntimeData.Theme.InfoWhite });
            }
            finally { 
                sw.Stop();
                Console.WriteLine($"Loading image credits took {sw.Elapsed}");
            }
        }
    }
}
