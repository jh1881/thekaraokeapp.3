﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using TKAv2.Helpers;
using TKAv2.Interfaces;
using TKAv2.Messages;
using TKAv2.Models;
using Xamarin.Forms;
using TKAv2.Services;
using Xamarin.Forms.Svg;

namespace TKAv2
{
    public partial class App : Application
    {
        public event EventHandler OnStartBackgrounding;
        public event EventHandler OnStopBackgrounding;

        private static ResourceDictionary _preTheme;
        private bool _backgrounding;
        private readonly IAnalytics _analytics;
        public new static App Current => Application.Current as App;
        public static IAnalytics Analytics => Current?._analytics;
        public string CurrentHostName { get; set; }
        public string CurrentHostId { get; set; }
        //        public bool CurrentHostHasUpdates { get; set; }
        public static bool ShowAds { get; set; }

        public IComms Comms => DependencyService.Get<IComms>();
//        private const string InsightsAnalyticsAppId = "a6c3ca0009988e894d736e82ccd0d5701af6c8ef";

        public App()
        {
            // Get the default images json and load the images into RuntimeData
            var sw = Stopwatch.StartNew();
            try
            {
                using (var strm = Assembly.GetExecutingAssembly().GetManifestResourceStream("TKAv2.defaultimages.json"))
                {
                    using (var rdr = new StreamReader(strm))
                    {
                        var imageJson = rdr.ReadToEnd();
                        RuntimeData.TheRuntimeData.LoadImages(imageJson);
                    }
                }
            }
            finally
            {
                sw.Stop();
                Console.WriteLine($"Loading images took {sw.Elapsed}");
            }

            DependencyService.Register<IComms, TheKaraokeAppServerComms>();
            DependencyService.Register<IAnalytics, AppCenterAnalytics>();
            AppCenter.Start("ios=ace5c169-0cdd-48cd-9eac-107f77264af8;android=ff28dbc9-c70f-49bb-aada-c3dfca8e5229", typeof(Analytics), typeof(Crashes));
            //            _analytics = DependencyService.Get<IAnalytics>();
            //            _analytics?.Init(InsightsAnalyticsAppId);
            //            _analytics?.Track("App.Constructor");

            SvgImageSource.RegisterAssembly();
            InitializeComponent();

            UpdateImageResources();

            if (_preTheme != null) SetTheme(_preTheme);

            MessagingCenter.Subscribe<HostChosenMessage>(this, "", msg =>
            {
                RuntimeData.TheRuntimeData.ChosenHostId = msg.Host.Id;
                RuntimeData.TheRuntimeData.HostName = msg.Host.Name;
//                RuntimeData.TheRuntimeData.HostNum = RuntimeData.TheRuntimeData.FixedHost;

                CurrentHostId = msg.Host.Id;
                CurrentHostName = msg.Host.Name;
            });
            MessagingCenter.Subscribe<LoggedInMessage>(this, "", msg =>
            {
                var comms = DependencyService.Get<IComms>();
                if (string.IsNullOrEmpty(comms.LoginToken))
                {
                    StopBackgrounding();
                }
                else
                {
                    StartBackgrounding();
                }
            });
            MessagingCenter.Subscribe<ImagesUpdatedMessage>(this, "", msg => UpdateImageResources());

            MainPage = new AppShell();
        }

        private void UpdateImageResources()
        {
            Resources["MicrophoneBlack"] = RuntimeData.TheRuntimeData.Theme.MicrophoneBlack;
            Resources["MicrophoneWhite"] = RuntimeData.TheRuntimeData.Theme.MicrophoneWhite;
            Resources["AvatarBlack"] = RuntimeData.TheRuntimeData.Theme.AvatarBlack;
            Resources["AvatarWhite"] = RuntimeData.TheRuntimeData.Theme.AvatarWhite;
            Resources["RepeatBlack"] = RuntimeData.TheRuntimeData.Theme.RepeatBlack;
            Resources["RepeatWhite"] = RuntimeData.TheRuntimeData.Theme.RepeatWhite;
            Resources["SettingsBlack"] = RuntimeData.TheRuntimeData.Theme.SettingsBlack;
            Resources["SettingsWhite"] = RuntimeData.TheRuntimeData.Theme.SettingsWhite;
            Resources["MusicBlack"] = RuntimeData.TheRuntimeData.Theme.MusicBlack;
            Resources["MusicWhite"] = RuntimeData.TheRuntimeData.Theme.MusicWhite;
            Resources["NewBlack"] = RuntimeData.TheRuntimeData.Theme.NewBlack;
            Resources["NewWhite"] = RuntimeData.TheRuntimeData.Theme.NewWhite;
            Resources["EnterBlack"] = RuntimeData.TheRuntimeData.Theme.EnterBlack;
            Resources["EnterWhite"] = RuntimeData.TheRuntimeData.Theme.EnterWhite;
            Resources["PlaceholderBlack"] = RuntimeData.TheRuntimeData.Theme.PlaceholderBlack;
            Resources["PlaceholderWhite"] = RuntimeData.TheRuntimeData.Theme.PlaceholderWhite;
            Resources["InfoBlack"] = RuntimeData.TheRuntimeData.Theme.InfoBlack;
            Resources["InfoWhite"] = RuntimeData.TheRuntimeData.Theme.InfoWhite;
        }

        protected override async void OnStart()
        {
            base.OnStart();
            if (!string.IsNullOrEmpty(Settings.JWT))
            {
                Comms.LoginToken = Settings.JWT;
                using (UserDialogs.Instance.Loading("Logging in..."))
                {
//                    await Task.Delay(5000); // TODO: Test only!
                    var result = await Comms.VerifyHost();
                    if (!result.IsSuccess)
                    {
                        Comms.LoginToken = null;
                        await UserDialogs.Instance.AlertAsync("Login token has expired. Please login again.", "The Karaoke App");
                    }
                    else
                    {
                        MessagingCenter.Send(new LoggedInMessage(), "");
                    }
                }
            }

            if (!RuntimeData.TheRuntimeData.FixedHost.HasValue)
            {
                var status = await CrossPermissions.Current.CheckPermissionStatusAsync<CameraPermission>();
                if (status == PermissionStatus.Unknown)
                {
                    var result = await CrossPermissions.Current.RequestPermissionAsync<CameraPermission>();
                    if (result == PermissionStatus.Granted)
                    {
                        MessagingCenter.Send(new CameraPermissionReceivedMessage(), "");
                    }
                }
            }
        }

        public static Task BeginInvokeOnMainThreadAsync(Action action)
        {
            Debug.WriteLine($"App.BeginInvokeOnMainThreadAsync started");
            var tcs = new TaskCompletionSource<object>();
            Xamarin.Forms.Device.BeginInvokeOnMainThread(() =>
            {
                try
                {
                    action();
                    Debug.WriteLine($"App.BeginInvokeOnMainThreadAsync action completed without error");
                    tcs.SetResult(null);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine($"App.BeginInvokeOnMainThreadAsync action completed with exception {ex}");
                    tcs.SetException(ex);
                }
            });
            return tcs.Task;
        }

        public void Error(string text, Exception exception = null)
        {
            if (exception != null)
                Crashes.TrackError(exception);
            Debug.WriteLine($"App.Error {text}");
            MessagingCenter.Send(new ErrorMessage { ErrorText = text }, "");
        }

        public static void PresetTheme(ResourceDictionary dict)
        {
            _preTheme = dict;
        }

        public void SetTheme(ResourceDictionary dict)
        {
            var mergedDictionaries = Resources.MergedDictionaries;
            mergedDictionaries.Clear();
            mergedDictionaries.Add(dict);
        }

        public void StartBackgrounding()
        {
            Analytics?.Track("StartBackgrounding", "_backgrounding", $"{_backgrounding}");
            if (_backgrounding)
                return;
            _backgrounding = true;
            OnStartBackgrounding?.Invoke(this, new EventArgs());
        }

        public void StopBackgrounding()
        {
            Analytics?.Track("StopBackgrounding", "_backgrounding", $"{_backgrounding}");
            if (!_backgrounding)
                return;
            _backgrounding = false;
            OnStopBackgrounding?.Invoke(this, new EventArgs());
        }

        public Task<int> ProcessBackgrounding()
        {
            Analytics?.Track("ProcessBackgrounding");
            return SongRequestStorage.Update();
        }
    }
}
