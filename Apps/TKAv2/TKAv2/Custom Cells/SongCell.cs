﻿using Xamarin.Forms;

namespace TKAv2.Custom_Cells
{
    public class SongCell : ViewCell
    {
        public SongCell()
        {
            var overallGrid = new Grid
            {
                Padding = new Thickness(5, 5, 20, 5),
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition { Width = GridLength.Auto }
                },
                RowDefinitions =
                {
                    new RowDefinition { Height = GridLength.Auto },
                    new RowDefinition { Height = GridLength.Auto }
                }
            };
            var titleView = new Label { FontSize = 18 };
            titleView.SetBinding(Label.TextProperty, "Title");

            var artistView = new Label { FontSize = 14 };
            artistView.SetBinding(Label.TextProperty, "Artist");

            var tapImage = new Image
            {
                HorizontalOptions = LayoutOptions.End,
                VerticalOptions = LayoutOptions.Center,
                Source = "tap.png",
                HeightRequest = 18
            };

            overallGrid.Children.Add(titleView, 0, 0);
            overallGrid.Children.Add(artistView, 0, 1);
            overallGrid.Children.Add(tapImage, 1, 0);
            Grid.SetRowSpan(tapImage, 2);

            View = overallGrid;
            Height = View.Height;
        }
    }
}
