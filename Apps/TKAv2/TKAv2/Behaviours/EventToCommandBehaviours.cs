﻿using Xamarin.Forms;

namespace TKAv2.Behaviours
{
    public class EventToCommandBehaviour : EventToCommandBehaviour<View> { }

    public class ShellEventToCommandBehaviour : EventToCommandBehaviour<Shell> { }
}
