﻿using System;
using System.Net;
using Newtonsoft.Json;

namespace TKAv2.Services
{
    public class CommsResult
    {
        public HttpStatusCode StatusCode { get; set; } = HttpStatusCode.OK;
        public string RawResponse { get; set; }
        public bool IsSuccess { get; set; }
        public bool NoConnection { get; set; }
        public Exception Exception { get; set; }

        public T Deserialise<T>()
        {
            return JsonConvert.DeserializeObject<T>(RawResponse);
        }

        public object Deserialise(Type targetType)
        {
            return JsonConvert.DeserializeObject(RawResponse, targetType);
        }

        public override string ToString()
        {
            return (Exception == null ? $"{StatusCode} {RawResponse}" : $"Exception: {Exception}");
        }
    }

    public enum RequestSongResult
    {
        Success,
        ServerError,
        ShowNotInProgress,
        HostNotFound,
        BadRequest,
        NoConnection,
        TooManyRequests,
        UnknownError
    }
}
