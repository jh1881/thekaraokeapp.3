﻿using System;
using System.Threading.Tasks;

namespace TKAv2.Services
{
#if USE_AZURE
    public class AzureComms : BaseComms
    {
        public override Task<CommsResult> ChooseHost(string number)
        {
            throw new NotImplementedException();
        }

        public override Task<CommsResult> AmIaHost(string fbid = null)
        {
            throw new NotImplementedException();
        }

        public override Task<CommsResult> SongListCount(string hostId = null)
        {
            throw new NotImplementedException();
        }

        public override Task<CommsResult> SongList(string hostId = null, bool exactMatch = false, string search = null, int firstRow = 0, bool orderByTitle = false, bool getCount = false)
        {
            throw new NotImplementedException();
        }

        public override Task<CommsResult> RecentSongList(string hostId = null, bool orderByTitle = false)
        {
            throw new NotImplementedException();
        }

        public override Task<RequestSongResult> RequestSong(string title, string artist, string hostId, string requestor, string keyChange = null, string tempoChange = null, string duetSecond = null, string groupNames = null)
        {
            throw new NotImplementedException();
        }

        public override Task<bool> SendPushNotificationToken(string deviceId)
        {
            throw new NotImplementedException();
        }

        public override Task<CommsResult> StartShow()
        {
            throw new NotImplementedException();
        }

        public override Task<CommsResult> StopShow()
        {
            throw new NotImplementedException();
        }

        public override Task<CommsResult> UnreadSongRequests(string hostId = null)
        {
            throw new NotImplementedException();
        }

        public override Task<CommsResult> GetShowStatus()
        {
            throw new NotImplementedException();
        }

        public override Task<CommsResult> LoginHost(string username, string password)
        {
            throw new NotImplementedException();
        }

        public override Task<CommsResult> VerifyHost()
        {
            throw new NotImplementedException();
        }
    }
#endif
}
