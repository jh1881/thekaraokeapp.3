﻿//#define OUTPUT_RAW_COMMS_RECEIPTS

using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TKAv2.Helpers;
using TKAv2.Models;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace TKAv2.Services
{
    public class TheKaraokeAppServerComms : BaseComms
    {
        private const string ProcessAmiahost = "AM";
        private const string ProcessChooseHost = "HO";
        private const string ProcessSongList = "SN";
        private const string ProcessRecentSongList = "RS";
        private const string ProcessRequestSong = "RQ";
        private const string ProcessRequestPush = "PU";
        private const string ProcessStartShow = "ST";
        private const string ProcessStopShow = "SP";
        private const string ProcessSongRequests = "UR";
        private const string ProcessCheckShowStatus = "CS";
        private const string ProcessHostLogin = "LO";
        private const string ProcessVerifyLogin = "VE";

        private const string CommsVersion = "3";
        private const string BaseUrl = "https://thekaraokeapp.co.uk/v" + CommsVersion;

        private async Task<CommsResult> CallApi(string url)
        {
            Debug.WriteLine($"TheKaraokeAppServerComms.CallApi asked {url}");
            //            App.Analytics?.Track("Comms.CallApi", "url", url);
            if (!IsConnected())
            {
                Debug.WriteLine($"TheKaraokeAppServerComms.CallApi({url}) - no connection found");
                return new CommsResult { IsSuccess = false, NoConnection = true };
            }

            using (var client = new HttpClient())
            {
                var result = new CommsResult();
                HttpResponseMessage response = null;
                try
                {
                    var msg = new HttpRequestMessage(HttpMethod.Get, url);
                    if (!string.IsNullOrEmpty(LoginToken))
                        msg.Headers.Add("Authorization", $"Bearer {LoginToken}");
                    //                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", LoginToken);

                    //                    response = await client.GetAsync(url);
                    response = await client.SendAsync(msg);
                    result.RawResponse = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode) result.IsSuccess = true;
                    else result.StatusCode = response.StatusCode;
                }
                catch (Exception e)
                {
                    //                    App.Analytics?.ReportException(e);
                    result.IsSuccess = false;
                    result.StatusCode = response?.StatusCode ?? HttpStatusCode.Unused;
                    result.RawResponse = $"Exception: {e.Message}";
                    result.Exception = e;
                }
#if OUTPUT_RAW_COMMS_RECEIPTS
                Debug.WriteLine($"Comms.CallApi({url}) got {result}");
#endif
                return result;
            }
        }

        private static string GetProcessUrl(string operation, string parameters = null)
        {
            var url = $"{BaseUrl}/process.php?op={operation}&uid={Helpers.Settings.Id}";
            if (!string.IsNullOrEmpty(parameters)) url += $"&{parameters}";
            return url;
        }

        public override async Task<CommsResult> ChooseHost(string number)
        {
            Debug.WriteLine("TheKaraokeAppServerComms.ChooseHost started");
            var url = GetProcessUrl(ProcessChooseHost, $"number={number}");
            try
            {
                var result = await CallApi(url);
                Debug.WriteLine($"TheKaraokeAppServerComms.ChooseHost ended with result {result}");
                return result;
            }
            catch (Exception e)
            {
                Debug.WriteLine($"TheKaraokeAppServerComms.ChooseHost exception {e}");
                //                App.Analytics?.ReportException(e);
                return new CommsResult { IsSuccess = false, Exception = e };
            }
        }

        public override async Task<CommsResult> AmIaHost(string fbid = null)
        {
            var url = fbid == null ? GetProcessUrl(ProcessAmiahost) : GetProcessUrl(ProcessAmiahost, $"facebookid={fbid}");
            var value = await CallApi(url);
            return value;
        }

        public override async Task<CommsResult> SongListCount(string hostId = null)
        {
            hostId = hostId ?? RuntimeData.TheRuntimeData.ChosenHostId ?? App.Current.CurrentHostId;
            var url = GetProcessUrl(ProcessSongList, $"hostid={hostId}&count=");
            var returnValue = await CallApi(url);
            Debug.WriteLine($"TheKaraokeAppServerComms.SongListCount got {returnValue}");
            return returnValue;
        }

        public override async Task<CommsResult> SongList(string hostId = null, bool exactMatch = false, string search = null, int firstRow = 0, bool orderByTitle = false, bool getCount = false)
        {
            try
            {
                hostId = hostId ?? RuntimeData.TheRuntimeData.ChosenHostId ?? App.Current.CurrentHostId;
                var url = GetProcessUrl(ProcessSongList, $"hostid={hostId}&limit={firstRow}");
                if (string.IsNullOrEmpty(search) == false)
                {
                    url += $"&search={WebUtility.UrlEncode(search)}";
                }

                if (orderByTitle) url += "&orderbytitle=";
                if (exactMatch) url += "&exactmatch=";
                if (getCount) url += "&count=";
                var returnValue = await CallApi(url);
#if OUTPUT_RAW_COMMS_RECEIPTS
            Debug.WriteLine($"Comms.SongList asked {url} and got {returnValue}");
#endif
                return returnValue;
            }
            catch(Exception ee)
            {
                throw;
            }
        }

        public override async Task<CommsResult> RecentSongList(string hostId = null, bool orderByTitle = false)
        {
            hostId = hostId ?? RuntimeData.TheRuntimeData.ChosenHostId ?? App.Current.CurrentHostId;
            var url = GetProcessUrl(ProcessRecentSongList, $"hostid={hostId}");
            if (orderByTitle) url += "&orderbytitle=";
            var returnValue = await CallApi(url);
#if OUTPUT_RAW_COMMS_RECEIPTS
            Debug.WriteLine($"TheKaraokeAppServerComms.RecentSongList asked {url} and got {returnValue}");
#endif
            return returnValue;
        }

        private static async Task AddGeoData(StringBuilder sb, bool includeInitialAmpersand = true)
        {
            Location position = null;
            try
            {
                position = await Geolocation.GetLastKnownLocationAsync();
            }
            catch { /**/ }
            if (position != null)
            {
                sb.AppendFormat("{2}lat={0}&lon={1}", position.Latitude, position.Longitude, includeInitialAmpersand ? "&" : "");
            }
        }

        public override async Task<RequestSongResult> RequestSong(string title, string artist, string hostId, string requestor, string keyChange = null, string tempoChange = null, string duetSecond = null, string groupNames = null)
        {
            Debug.WriteLine($"TheKaraokeAppServerComms.RequestSong({title}, {artist}, {hostId}, {requestor}, {keyChange ?? "null"}, {tempoChange ?? "null"}, {duetSecond ?? "null"}, {groupNames ?? "null"})");
            var sb = new StringBuilder($"title={WebUtility.UrlEncode(title)}&artist={WebUtility.UrlEncode(artist)}&requestor={WebUtility.UrlEncode(requestor)}&hostid={hostId}");
            if (string.IsNullOrEmpty(keyChange) == false) sb.AppendFormat("&keychange={0}", WebUtility.UrlEncode(keyChange));
            if (string.IsNullOrEmpty(tempoChange) == false) sb.AppendFormat("&tempochange={0}", WebUtility.UrlEncode(tempoChange));
            if (string.IsNullOrEmpty(duetSecond) == false) sb.AppendFormat("&duetsecond={0}", WebUtility.UrlEncode(duetSecond));
            if (string.IsNullOrEmpty(groupNames) == false) sb.AppendFormat("&groupnames={0}", WebUtility.UrlEncode(groupNames));
            switch (Device.RuntimePlatform)
            {
                case Device.iOS: sb.AppendFormat("&deviceType=1"); break;
                case Device.Android: sb.AppendFormat("&deviceType=2"); break;
            }

            // Get a location
            await AddGeoData(sb);

            var url = GetProcessUrl(ProcessRequestSong, sb.ToString());
            Debug.WriteLine($"TheKaraokeAppServerComms.RequestSong url = {url}");

            var result = await CallApi(url);
            var songResult = RequestSongResult.Success;
            if (result.IsSuccess == false)
            {
                if (result.NoConnection) return RequestSongResult.NoConnection;
                if ((int)result.StatusCode == 429)
                {
                    songResult = RequestSongResult.TooManyRequests;
                }
                else
                {
                    switch (result.StatusCode)
                    {
                        case HttpStatusCode.InternalServerError:
                            songResult = RequestSongResult.ServerError;
                            break;
                        case HttpStatusCode.BadRequest:
                            songResult = RequestSongResult.BadRequest;
                            break;
                        case HttpStatusCode.NotFound:
                            songResult = RequestSongResult.HostNotFound;
                            break;
                        case HttpStatusCode.Forbidden:
                            songResult = RequestSongResult.ShowNotInProgress;
                            break;
                        default:
                            songResult = RequestSongResult.UnknownError;
                            break;
                    }
                }
            }
            Debug.WriteLine($"TheKaraokeAppServerComms.RequestSong ended with {songResult}");
            return songResult;
        }

        public override async Task<bool> SendPushNotificationToken(string deviceId)
        {
            var parameters = $"deviceid={deviceId}&devicetype=";
            switch (Device.RuntimePlatform)
            {
                case Device.iOS: parameters += "1"; break;
                case Device.Android: parameters += "2"; break;
                case Device.UWP: parameters += "3";
                    break;
            }
            var url = GetProcessUrl(ProcessRequestPush, parameters);
            var result = await CallApi(url);
            return result.IsSuccess;
        }

        public override async Task<CommsResult> StartShow()
        {
            var sb = new StringBuilder("");
            await AddGeoData(sb, false);

            var url = GetProcessUrl(ProcessStartShow, sb.ToString());

            return await CallApi(url);
        }

        public override async Task<CommsResult> StopShow()
        {
            var url = GetProcessUrl(ProcessStopShow);
            return await CallApi(url);
        }

        public override async Task<CommsResult> UnreadSongRequests(string hostId = null)
        {
            var url = GetProcessUrl(ProcessSongRequests, (hostId != null ? $"hostid={hostId}" : null));
            var response = await CallApi(url);
            return response;
        }

        public override async Task<CommsResult> GetShowStatus()
        {
            var url = GetProcessUrl(ProcessCheckShowStatus);
            var response = await CallApi(url);
            return response;
        }

        public override async Task<CommsResult> LoginHost(string username, string password)
        {
            var url = GetProcessUrl(ProcessHostLogin, $"u={username}&p={password}");
            var response = await CallApi(url);
            return response;
        }

        public override async Task<CommsResult> VerifyHost()
        {
            var url = GetProcessUrl(ProcessVerifyLogin);
            var response = await CallApi(url);
            if (response.IsSuccess == false)
            {
                Debug.WriteLine($"TheKaraokeAppServerComms.CallApi({url}) - bad result {response.StatusCode}");
                LoginToken = "";
                Settings.JWT = "";
            }
            return response;
        }
    }
}
