﻿using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.AppCenter.Analytics;
using TKAv2.Interfaces;

namespace TKAv2.Services
{
    public class AppCenterAnalytics : IAnalytics
    {
        public void Init(string appId)
        {
        }

        public void Identify(string uid, IDictionary<string, string> table = null)
        {
        }

        public void Identify(string uid, string key, string value)
        {
        }

        public void Track(string trackIdentifier, string key, string value)
        {
            Analytics.TrackEvent(trackIdentifier, new Dictionary<string, string> { { key, value } });
        }

        public void Track(string trackIdentifier, IDictionary<string, string> table = null)
        {
            Analytics.TrackEvent(trackIdentifier, table);
        }

        public void ReportException(Exception e, AnalyticsSeverity severity)
        {
        }

        public void ReportException(Exception e, IDictionary data = null)
        {
        }

        public void ReportException(Exception e, string key, string value)
        {
        }
    }
}
