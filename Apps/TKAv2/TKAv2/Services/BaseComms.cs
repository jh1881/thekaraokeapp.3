﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using TKAv2.Extensions;
using Xamarin.Essentials;

namespace TKAv2.Services
{
    public abstract class BaseComms : IComms
    {
        private string _loginToken;

        public string LoginToken
        {
            get => _loginToken;
            set { _loginToken = value; OnLoginTokenChanged?.Invoke(null); }
        }

        public string LoggedInHostId { get; set; }
        public event EventHandler OnLoginTokenChanged;

        public bool IsConnected()
        {
            var current = Connectivity.NetworkAccess;
            var isConnected = current == NetworkAccess.Internet;
            Debug.WriteLine($"Comms.IsConnected said {isConnected}");
            return isConnected;
        }

        public abstract Task<CommsResult> ChooseHost(string number);
        public abstract Task<CommsResult> AmIaHost(string fbid = null);
        public abstract Task<CommsResult> SongListCount(string hostId = null);
        public abstract Task<CommsResult> SongList(string hostId = null, bool exactMatch = false, string search = null, int firstRow = 0, bool orderByTitle = false, bool getCount = false);
        public abstract Task<CommsResult> RecentSongList(string hostId = null, bool orderByTitle = false);
        public abstract Task<RequestSongResult> RequestSong(string title, string artist, string hostId, string requestor, string keyChange = null, string tempoChange = null, string duetSecond = null, string groupNames = null);
        public abstract Task<bool> SendPushNotificationToken(string deviceId);
        public abstract Task<CommsResult> StartShow();
        public abstract Task<CommsResult> StopShow();
        public abstract Task<CommsResult> UnreadSongRequests(string hostId = null);
        public abstract Task<CommsResult> GetShowStatus();
        public abstract Task<CommsResult> LoginHost(string username, string password);
        public abstract Task<CommsResult> VerifyHost();
    }
}