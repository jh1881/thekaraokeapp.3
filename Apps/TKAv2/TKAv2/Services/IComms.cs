﻿using System;
using System.Threading.Tasks;

namespace TKAv2.Services
{
    public interface IComms
    {
        string LoginToken { get; set; }
        string LoggedInHostId { get; set; }
        event EventHandler OnLoginTokenChanged;
        bool IsConnected();
        Task<CommsResult> ChooseHost(string number);
        Task<CommsResult> AmIaHost(string fbid = null);
        Task<CommsResult> SongListCount(string hostId = null);
        Task<CommsResult> SongList(string hostId = null, bool exactMatch = false, string search = null, int firstRow = 0, bool orderByTitle = false, bool getCount = false);
        Task<CommsResult> RecentSongList(string hostId = null, bool orderByTitle = false);
        Task<RequestSongResult> RequestSong(string title, string artist, string hostId, string requestor, string keyChange = null, string tempoChange = null, string duetSecond = null, string groupNames = null);
        Task<bool> SendPushNotificationToken(string deviceId);
        Task<CommsResult> StartShow();
        Task<CommsResult> StopShow();
        Task<CommsResult> UnreadSongRequests(string hostId = null);
        Task<CommsResult> GetShowStatus();
        Task<CommsResult> LoginHost(string username, string password);
        Task<CommsResult> VerifyHost();
    }
}
