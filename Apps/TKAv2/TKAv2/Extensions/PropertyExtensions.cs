﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace TKAv2.Extensions
{
    public static class PropertyExtensions
    {
        public static void SetPropertyValue<T, TValue>(this T target, Expression<Func<T, TValue>> memberLambda, TValue value)
        {
            if (memberLambda.Body is MemberExpression memberSelectorExpression)
            {
                var property = memberSelectorExpression.Member as PropertyInfo;
                if (property != null)
                {
                    property.SetValue(target, value, null);
                }
            }
        }

        public static TValue GetPropertyValue<T, TValue>(this T target, Expression<Func<T, TValue>> memberLambda)
        {
            if (memberLambda.Body is MemberExpression memberSelectorExpression)
            {
                var property = memberSelectorExpression.Member as PropertyInfo;
                if (property != null)
                {
                    return (TValue)property.GetValue(target, null);
                }
            }

            throw new InvalidOperationException($"{memberLambda} invalid for GetPropertyValue<{typeof(T)}, {typeof(TValue)}>");
        }
    }
}
