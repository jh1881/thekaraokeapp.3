﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace TKAv2.Extensions
{
    public static class CollectionExtensions
    {
        public static void AddRange<T>(this ObservableCollection<T> collection, IEnumerable<T> items)
        {
            var list = items.ToList();
            foreach(var i in list)
                collection.Add(i);
        }

        public static async Task AddRangeSlowly<T>(this ObservableCollection<T> collection, IEnumerable<T> items,
            int batchSize = 100)
        {
            var list = items.ToList();
            var c = 0;
            while (true)
            {
                var batch = list.Skip(c).Take(batchSize).ToList();
                if (!batch.Any()) return;
                c += batch.Count;
                collection.AddRange(batch);
                await Task.Delay(10);
            }
        }
    }
}
