﻿using System;

namespace TKAv2.Extensions
{
    public class EventArgs<T> : EventArgs
    {
        public T Value { get; }
        public EventArgs(T value) { Value = value; }
    }
}
