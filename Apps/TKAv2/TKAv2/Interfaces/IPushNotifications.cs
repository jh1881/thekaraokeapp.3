﻿namespace TKAv2.Interfaces
{
    public interface IPushNotifications
    {
        void Begin();
        void End();

        void ClearBadge();
    }
}
