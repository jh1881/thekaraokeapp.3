﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TKAv2.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TKAv2.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SettingsPage : ContentPage
    {
        public SettingsPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            ((SettingsPageViewModel)BindingContext).OnAppearing();
        }
    }
}
