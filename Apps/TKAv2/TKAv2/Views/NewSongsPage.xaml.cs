﻿using TKAv2.Models;
using TKAv2.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TKAv2.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewSongsPage : ContentPage
    {
        private NewSongsPageViewModel ViewModel => BindingContext as NewSongsPageViewModel;

        public NewSongsPage()
        {
            InitializeComponent();
        }

        private void ListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item is Song song)
                ViewModel.SongTappedCommand.Execute(song);
            ((ListView)sender).SelectedItem = null;
        }
    }
}
