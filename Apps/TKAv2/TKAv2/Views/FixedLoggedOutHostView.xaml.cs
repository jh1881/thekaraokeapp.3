﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TKAv2.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FixedLoggedOutHostView : ContentView
    {
        public FixedLoggedOutHostView()
        {
            InitializeComponent();
        }
    }
}
