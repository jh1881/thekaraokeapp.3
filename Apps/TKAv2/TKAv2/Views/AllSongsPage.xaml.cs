﻿using TKAv2.Messages;
using TKAv2.Models;
using TKAv2.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TKAv2.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AllSongsPage : ContentPage
    {
        private AllSongsPageViewModel ViewModel => BindingContext as AllSongsPageViewModel;
        public AllSongsPage()
        {
            InitializeComponent();

            MessagingCenter.Subscribe<ListViewRefreshEndedMessage>(this, ListViewRefreshEndedMessage.Songs, msg => SongListView.EndRefresh());
        }

        private void ListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item is Song song)
                ViewModel.SongTappedCommand.Execute(song);
            ((ListView)sender).SelectedItem = null;
        }
    }
}
