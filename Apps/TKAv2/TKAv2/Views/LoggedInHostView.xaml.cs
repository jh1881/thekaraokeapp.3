﻿using System.Diagnostics;
using TKAv2.Messages;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TKAv2.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoggedInHostView : ContentView
    {
        public LoggedInHostView()
        {
            InitializeComponent();
            MessagingCenter.Subscribe<ListViewRefreshEndedMessage>(this, ListViewRefreshEndedMessage.SongRequests, msg => RequestsList.EndRefresh());
        }

        private void RequestsList_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e == null) return;
            ((ListView)sender).SelectedItem = null;
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();

            Debug.WriteLine($"BindingContext is now {BindingContext}");
        }
    }
}
