﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using TKAv2.Models;
using TKAv2.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TKAv2.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PreviousSongRequestsPage : ContentPage
    {
        public PreviousSongRequestsPageViewModel VM => BindingContext as PreviousSongRequestsPageViewModel;

        public PreviousSongRequestsPage()
        {
            InitializeComponent();
        }

        private void ListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item is Song song)
                VM.SongTappedCommand.Execute(song);
            ((ListView)sender).SelectedItem = null;
        }
    }
}
