﻿using System.ComponentModel;
using Google.MobileAds;
using TKAv2.iOS.Renderers;
using TKAv2.Views;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(AdMobView), typeof(AdMobViewRenderer))]
namespace TKAv2.iOS.Renderers
{
    public class AdMobViewRenderer : ViewRenderer<AdMobView, BannerView>
    {
        protected override void OnElementChanged(ElementChangedEventArgs<AdMobView> e)
        {
            base.OnElementChanged(e);
            if (Control == null && e.NewElement != null)
                SetNativeControl(CreateBannerView());
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (e.PropertyName == nameof(BannerView.AdUnitId))
                Control.AdUnitId = Element.AdUnitId;
        }

        private static UIViewController GetVisibleViewController()
        {
            var windows = UIApplication.SharedApplication.Windows;
            foreach (var w in windows)
            {
                if (w.RootViewController != null)
                    return w.RootViewController;
            }

            return null;
        }

        private BannerView CreateBannerView()
        {
            Request GetRequest()
            {
                var request = Request.GetDefaultRequest();
                MobileAds.SharedInstance.RequestConfiguration.TestDeviceIdentifiers = new[] { "f30dc62edcaf88181300aeb09ee9ec5a" };
                return request;
            }
            var bannerView = new BannerView(AdSizeCons.SmartBannerLandscape)
            {
                AdUnitId = Element.AdUnitId,
                RootViewController = GetVisibleViewController()
            };
            bannerView.LoadRequest(GetRequest());
            return bannerView;
        }
    }
}
