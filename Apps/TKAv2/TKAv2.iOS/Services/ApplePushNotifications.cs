﻿using TKAv2.Interfaces;
using TKAv2.iOS.Services;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(ApplePushNotifications))]
namespace TKAv2.iOS.Services
{
    class ApplePushNotifications : IPushNotifications
    {
        public void Begin()
        {
            Device.BeginInvokeOnMainThread(() => UIApplication.SharedApplication.RegisterForRemoteNotifications());
        }

        public void End()
        {
            Device.BeginInvokeOnMainThread(() => UIApplication.SharedApplication.UnregisterForRemoteNotifications());
        }

        public void ClearBadge()
        {
            Device.BeginInvokeOnMainThread(() => UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0);
        }
    }
}
