﻿using TKAv2.Droid.Services;
using TKAv2.Interfaces;
using Xamarin.Forms;

[assembly: Dependency(typeof(DroidPushNotifications))]
namespace TKAv2.Droid.Services
{
    class DroidPushNotifications : IPushNotifications
    {
        public void Begin()
        {
        }

        public void End()
        {
        }

        public void ClearBadge()
        {
        }
    }
}
