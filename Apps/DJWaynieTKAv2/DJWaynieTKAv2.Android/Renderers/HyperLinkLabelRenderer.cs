﻿using System.ComponentModel;
using Android.Content;
using Android.Text.Util;
using DJWaynieTKAv2.Droid.Renderers;
using Java.Util.Regex;
using TKAv2.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;


[assembly: ExportRenderer(typeof(HyperLinkLabel), typeof(HyperLinkLabelRenderer))]
namespace DJWaynieTKAv2.Droid.Renderers
{
    public class HyperLinkLabelRenderer : LabelRenderer
    {
        public HyperLinkLabelRenderer(Context context) : base(context) { }

        private class MatchFilter : Java.Lang.Object, Linkify.IMatchFilter
        {
            public bool AcceptMatch(Java.Lang.ICharSequence s, int start, int end)
            {
                return true;
            }
        }

        private class TransformFilter : Java.Lang.Object, Linkify.ITransformFilter
        {
            public string TransformUrl(Matcher match, string url)
            {
                return "";
            }
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement == null)
            {
                var element = Element as HyperLinkLabel;
                if (element == null) return;

                var nativeEditText = Control;
                if (!string.IsNullOrEmpty(element.NavigateUri))
                {
                    Linkify.AddLinks(nativeEditText, Pattern.Compile("^" + element.Text + "$"), element.NavigateUri, new MatchFilter(), new TransformFilter());
                }
                else if (element.NavigateCommand != null)
                {
                    element.GestureRecognizers.Add(new TapGestureRecognizer
                    {
                        Command = element.NavigateCommand,
                        NumberOfTapsRequired = 1
                    });
                }
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            var element = Element as HyperLinkLabel;
            if (element == null) return;
            switch (e.PropertyName)
            {
                case "Text":
                    SetGesture();
                    break;
                case "NavigateCommand":
                    SetGesture();
                    break;
            }
        }

        private void SetGesture()
        {
            var element = Element as HyperLinkLabel;
            if (element == null) return;
            var nativeEditText = Control;
            if (!string.IsNullOrEmpty(element.NavigateUri))
            {
                Linkify.AddLinks(nativeEditText, Pattern.Compile("^" + element.Text + "$"), element.NavigateUri, new MatchFilter(), new TransformFilter());
            }
            else if (element.NavigateCommand != null)
            {
                if (element.GestureRecognizers.Count > 0) return;
                element.GestureRecognizers.Add(new TapGestureRecognizer
                {
                    Command = element.NavigateCommand,
                    NumberOfTapsRequired = 1
                });
            }
        }
    }

}