﻿using DJWaynieTKAv2.iOS.Services;
using Google.MobileAds;
using TKAv2.Services;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(AppleAdInterstitial))]
namespace DJWaynieTKAv2.iOS.Services
{
    class AppleAdInterstitial : IAdInterstitial
    {
        Interstitial _interstitial;

        public AppleAdInterstitial()
        {
            LoadAd();
            _interstitial.ScreenDismissed += (s, e) => LoadAd();
        }

        void LoadAd()
        {
            // TODO: change this id to your admob id    
            _interstitial = new Interstitial(Constants.InterstitialAdUnitId);

            var request = Request.GetDefaultRequest();
//            request.TestDevices = new [] { "Your Test Device ID", "GADSimulator" };
            _interstitial.LoadRequest(request);
        }

        public void ShowAd()
        {
            if (_interstitial.IsReady)
            {
                var viewController = GetVisibleViewController();
                _interstitial.PresentFromRootViewController(viewController);
            }
        }

        private static UIViewController GetVisibleViewController()
        {
            var rootController = UIApplication.SharedApplication.KeyWindow.RootViewController;

            switch (rootController.PresentedViewController)
            {
                case null: return rootController;
                case UINavigationController controller: return controller.VisibleViewController;
                case UITabBarController barController: return barController.SelectedViewController;
                default: return rootController.PresentedViewController;
            }
        }
    }
}
