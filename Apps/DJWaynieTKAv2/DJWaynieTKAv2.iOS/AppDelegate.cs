﻿using System;
using System.Diagnostics;
using WindowsAzure.Messaging;
using Acr.UserDialogs;
using Foundation;
using TKAv2;
using TKAv2.Models;
using TKAv2.Services;
using UIKit;
using UserNotifications;
using Xamarin.Forms;

namespace DJWaynieTKAv2.iOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {

#if USE_AZURE
        private SBNotificationHub Hub { get; set; }

        public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {
            Hub = new SBNotificationHub(Constants.ListenConnectionString, Constants.NotificationHubName);

            Hub.UnregisterAllAsync(deviceToken, error => {
                if (error != null)
                {
                    System.Diagnostics.Debug.WriteLine("Error calling Unregister: {0}", error.ToString());
                    return;
                }

                NSSet tags = null; // create tags if you want
                Hub.RegisterNativeAsync(deviceToken, tags, errorCallback => {
                    if (errorCallback != null)
                        System.Diagnostics.Debug.WriteLine("RegisterNativeAsync error: " + errorCallback.ToString());
                });
            });
        }
#endif

#if USE_AZURE
        public override void ReceivedRemoteNotification(UIApplication application, NSDictionary userInfo)
        {
            ProcessNotification(userInfo, false);
        }

        void ProcessNotification(NSDictionary options, bool fromFinishedLaunching)
        {
            var nsAps = new NSString("aps");
            // Check to see if the dictionary has the aps key.  This is the notification payload you would have sent
            if (null != options && options.ContainsKey(nsAps))
            {
                var alert = string.Empty;

                //Get the aps dictionary
                if (options.ObjectForKey(nsAps) is NSDictionary aps)
                {
                    //Extract the alert text
                    // NOTE: If you're using the simple alert by just specifying
                    // "  aps:{alert:"alert msg here"}  ", this will work fine.
                    // But if you're using a complex alert with Localization keys, etc.,
                    // your "alert" object from the aps dictionary will be another NSDictionary.
                    // Basically the JSON gets dumped right into a NSDictionary,
                    // so keep that in mind.
                    var nsAlert = new NSString("alert");
                    if (aps.ContainsKey(nsAlert))
                        alert = (aps[nsAlert] as NSString)?.ToString();
                }

                //If this came from the ReceivedRemoteNotification while the app was running,
                // we of course need to manually process things like the sound, badge, and alert.
                if (!fromFinishedLaunching && !string.IsNullOrEmpty(alert))
                {
                    //Manually show an alert
                    var avAlert = new UIAlertView("Notification", alert, (IUIAlertViewDelegate)null, "OK", null);
                    avAlert.Show();
                }
            }
        }
#else
        public override void FailedToRegisterForRemoteNotifications(UIApplication application, NSError error)
        {
            // Notify the user!
            App.Analytics?.Track("FailedToRegisterForRemoteNotifications");
            Device.BeginInvokeOnMainThread(
                async () => await UserDialogs.Instance.AlertAsync("Error registering push notifications"));
        }

        public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {
            var token = deviceToken.Description;
            if (!string.IsNullOrWhiteSpace(token))
            {
                token = token.Trim('<').Trim('>').Replace(" ", "");
            }
            Debug.WriteLine($"Device Token {token}");
            App.Analytics?.Track("RegisteredForRemoteNotifications", "token", token);
            var comms = DependencyService.Get<IComms>();
            System.Threading.Tasks.Task.Factory.StartNew(async () => await comms.SendPushNotificationToken(token));
        }

        public override void DidReceiveRemoteNotification(UIApplication application, NSDictionary userInfo, Action<UIBackgroundFetchResult> completionHandler)
        {
            App.Analytics?.Track("DidReceiveRemoteNotification");

            // Push notification received.
            System.Threading.Tasks.Task.Factory.StartNew(async () => await _app.ProcessBackgrounding());
            completionHandler(UIBackgroundFetchResult.NewData);
        }
#endif

        private App _app;

        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            StartNotifications();

            RuntimeData.TheRuntimeData.FixedHost = 123456;
            RuntimeData.TheRuntimeData.MainAboutText = "DJ Waynie's Karaoke App, written by Adam Godley of LV426";
            Google.MobileAds.MobileAds.SharedInstance.Start(status => { });
            Plugin.InputKit.Platforms.iOS.Config.Init();
//            ZXing.Net.Mobile.Forms.iOS.Platform.Init();
            Forms.SetFlags("Shell_Experimental", "Visual_Experimental", "CollectionView_Experimental", "FastRenderers_Experimental");
            Forms.Init();
            _app = new App();
#if !USE_AZURE
            _app.OnStartBackgrounding += (sender, args) =>
            {
                UIApplication.SharedApplication.BeginInvokeOnMainThread(() =>
                {
                    if (UIDevice.CurrentDevice.CheckSystemVersion(10, 0))
                    {
                        // Request Permissions
                        UNUserNotificationCenter.Current.RequestAuthorization(
                            UNAuthorizationOptions.Alert | UNAuthorizationOptions.Badge | UNAuthorizationOptions.Sound,
                            (granted, error) =>
                            {
                                // Do something if needed
                            });
                    }
                    else if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
                    {
                        var notificationSettings = UIUserNotificationSettings.GetSettingsForTypes(
                            UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound, null);

                        app.RegisterUserNotificationSettings(notificationSettings);
                    }
                });
            };
#endif
            LoadApplication(_app);

            return base.FinishedLaunching(app, options);
        }

        private void StartNotifications()
        {
            if (UIDevice.CurrentDevice.CheckSystemVersion(10, 0))
            {
                UNUserNotificationCenter.Current.RequestAuthorization(
                    UNAuthorizationOptions.Alert | UNAuthorizationOptions.Sound | UNAuthorizationOptions.Badge,
                    (granted, error) =>
                    {
                        if (granted)
                            InvokeOnMainThread(UIApplication.SharedApplication.RegisterForRemoteNotifications);
                    });
            }
            else if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
            {
                var pushSettings = UIUserNotificationSettings.GetSettingsForTypes(
                    UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound,
                    new NSSet());

                UIApplication.SharedApplication.RegisterUserNotificationSettings(pushSettings);
                UIApplication.SharedApplication.RegisterForRemoteNotifications();
            }
            else
            {
                var notificationTypes = UIRemoteNotificationType.Alert | UIRemoteNotificationType.Badge |
                                        UIRemoteNotificationType.Sound;
                UIApplication.SharedApplication.RegisterForRemoteNotificationTypes(notificationTypes);
            }
        }
    }
}
