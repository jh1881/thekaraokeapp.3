﻿using System;
using System.ComponentModel;
using DJWaynieTKAv2.iOS.Renderers;
using Google.MobileAds;
using Microsoft.AppCenter.Crashes;
using TKAv2.Views;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(AdMobView), typeof(AdMobViewRenderer))]
namespace DJWaynieTKAv2.iOS.Renderers
{
    public class AdMobViewRenderer : ViewRenderer<AdMobView, BannerView>
    {
        protected override void OnElementChanged(ElementChangedEventArgs<AdMobView> e)
        {
            base.OnElementChanged(e);
            if (Control == null && e.NewElement != null)
            {
                SetNativeControl(CreateBannerView());
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (e.PropertyName == nameof(BannerView.AdUnitId))
            {
                Control.AdUnitId = Element.AdUnitId;
            }
        }

        private static UIViewController GetVisibleViewController()
        {
            var windows = UIApplication.SharedApplication.Windows;
            foreach (var w in windows)
            {
                if (w.RootViewController != null)
                    return w.RootViewController;
            }

            return null;
        }

        private BannerView CreateBannerView()
        {
            Request GetRequest()
            {
                var request = Request.GetDefaultRequest();
                request.TestDevices = new[] { "f30dc62edcaf88181300aeb09ee9ec5a", "94a7b7489fffd29e83db6da063cd9eac" };
                return request;
            }

            try
            {
                var bannerView = new BannerView(AdSizeCons.SmartBannerLandscape)
                {
                    AdUnitId = Element.AdUnitId,
                    RootViewController = GetVisibleViewController()
                };
                bannerView.LoadRequest(GetRequest());
                return bannerView;
            }
            catch (Exception ee)
            {
                Crashes.TrackError(ee);
                return null;
            }
        }
    }
}
