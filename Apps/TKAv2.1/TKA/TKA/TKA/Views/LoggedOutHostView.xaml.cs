﻿using TKA.Messages;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TKA.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoggedOutHostView
    {
        public LoggedOutHostView()
        {
            InitializeComponent();
            MessagingCenter.Subscribe<CameraPermissionReceivedMessage>(this, "", msg =>
            {
                if (Resources["ScanTemplate"] is DataTemplate template)
                {
                    var ob = template.CreateContent();
                    if (ob is Grid grid)
                    {
                        grid.BindingContext = BindingContext;
                        ScanGrid.Children.Add(grid);
                    }
                }
            });
        }

        private void Entry_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(e.NewTextValue)) return;
            if (sender == EntryHostId1)
                EntryHostId2.Focus();
            else if (sender == EntryHostId2)
                EntryHostId3.Focus();
            else if (sender == EntryHostId3)
                EntryHostId4.Focus();
            else if (sender == EntryHostId4)
                EntryHostId5.Focus();
            else if (sender == EntryHostId5)
                EntryHostId6.Focus();
        }
    }
}
