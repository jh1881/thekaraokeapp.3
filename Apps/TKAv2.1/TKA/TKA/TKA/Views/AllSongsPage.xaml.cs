﻿using TKA.Messages;
using TKA.Models;
using TKA.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TKA.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AllSongsPage : ContentPage
    {
        private AllSongsPageViewModel ViewModel => BindingContext as AllSongsPageViewModel;
        public AllSongsPage()
        {
            InitializeComponent();
            MessagingCenter.Subscribe<ListViewRefreshEndedMessage>(this, ListViewRefreshEndedMessage.Songs, msg => SongListView.EndRefresh());
        }

        private void ListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item is Song song)
                ViewModel.SongTappedCommand.Execute(song);
            ((ListView)sender).SelectedItem = null;

        }
    }
}
