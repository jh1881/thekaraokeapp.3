﻿using System.Diagnostics;
using TKA.Messages;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TKA.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoggedInHostView
    {
        public LoggedInHostView()
        {
            InitializeComponent();
            MessagingCenter.Subscribe<ListViewRefreshEndedMessage>(this, ListViewRefreshEndedMessage.SongRequests, msg => RequestsList.EndRefresh());
        }

        private void RequestsList_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e == null) return;
            ((ListView)sender).SelectedItem = null;
        }
    }
}
