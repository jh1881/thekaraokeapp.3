﻿using Xamarin.Forms.Xaml;

namespace TKA.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FixedLoggedOutHostView
    {
        public FixedLoggedOutHostView()
        {
            InitializeComponent();
        }
    }
}