﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TKA.Models;
using TKA.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TKA.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PreviousSongRequestsPage : ContentPage
    {
        public PreviousSongRequestsPageViewModel VM => BindingContext as PreviousSongRequestsPageViewModel;

        public PreviousSongRequestsPage()
        {
            InitializeComponent();
        }

        private void ListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item is Song song)
                VM.SongTappedCommand.Execute(song);
            ((ListView)sender).SelectedItem = null;
        }
    }
}