﻿namespace TKA.Interfaces
{
    public interface IPushNotifications
    {
        void Begin();
        void End();

        void ClearBadge();
    }
}
