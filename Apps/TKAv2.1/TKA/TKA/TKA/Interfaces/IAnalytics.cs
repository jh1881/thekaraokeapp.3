﻿using System;
using System.Collections;
using System.Collections.Generic;
using TKA.Models;

namespace TKA.Interfaces
{
    public interface IAnalytics
    {
        void Init(string appId);

        void Identify(string uid, IDictionary<string, string> table = null);

        void Identify(string uid, string key, string value);

        void Track(string trackIdentifier, string key, string value);

        void Track(string trackIdentifier, IDictionary<string, string> table = null);

        void ReportException(Exception e, AnalyticsSeverity severity);

        void ReportException(Exception e, IDictionary data = null);

        void ReportException(Exception e, string key, string value);
    }
}
