﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TKA.Helpers
{
    public static class Routes
    {
        public static string HostRoute = "//host";
        public static string HostHostRoute = $"{HostRoute}/host";
        public static string HostMapRoute = $"{HostRoute}/map";
        public static string HostLoginRoute = $"{HostRoute}/login";

        public static string SongsRoute = "//songs";
        public static string AllSongsRoute = $"{SongsRoute}/all";
        public static string NewSongsRoute = $"{SongsRoute}/new";

        public static string RequestRoute = "//request";
        public static string RequestSongRoute = $"{RequestRoute}/requestsong";
        public static string PreviousSongRequestsRoute = $"{RequestRoute}/previousrequests";

        public static string SettingsRoute = "//settings";
    }
}
