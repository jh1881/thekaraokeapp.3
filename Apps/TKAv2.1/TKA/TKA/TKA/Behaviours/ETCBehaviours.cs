﻿using Xamarin.Forms;

namespace TKA.Behaviours
{
    public class EventToCommandBehaviour : EventToCommandBehaviour<View> { }
    public class ShellEventToCommandBehaviour : EventToCommandBehaviour<Shell> { }
}
