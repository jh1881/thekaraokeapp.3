﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using TKA.Helpers;
using TKA.Messages;
using TKA.Models;
using Xamarin.Forms;

namespace TKA.ViewModels
{
    public class PreviousSongRequestsPageViewModel : BaseViewModel
    {
        private ObservableCollection<RequestedSong> _previousRequests = new ObservableCollection<RequestedSong>(Settings.LastSongsRequested);

        public bool HasSongs => PreviousRequests.Any();
        public bool ShowAds => RuntimeData.TheRuntimeData.ShowAds;

        public ObservableCollection<RequestedSong> PreviousRequests
        {
            get => _previousRequests;
            set => SetProperty(ref _previousRequests, value, otherPropertiesToRaise: new[] { nameof(HasSongs) });
        }

        public string AdUnitId
        {
            get
            {
                switch (Device.RuntimePlatform)
                {
                    case Device.iOS: return "ca-app-pub-8064175537251762/7121828858";
                    case Device.Android: return "ca-app-pub-8064175537251762/5376594503";
                    default: return "";
                }
            }
        }

        public PreviousSongRequestsPageViewModel()
        {
            MessagingCenter.Subscribe<SongRequestedMessage>(this, "", msg =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    PreviousRequests = new ObservableCollection<RequestedSong>(Settings.LastSongsRequested);
                });
            });
            SongTappedCommand = new Command(async o =>
            {
                if (o is RequestedSong s)
                {
                    await Shell.Current.GoToAsync($"{Routes.RequestRoute}?title={s.Song.Title}&artist={s.Song.Artist}" +
                                                  $"&tempochange={s.TempoChange}&keychange={s.KeyChange}");
                }
            });
        }

        public ICommand SongTappedCommand { get; }
    }
}
