﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using Acr.UserDialogs;
using Microsoft.AppCenter.Crashes;
using TKA.Helpers;
using TKA.Interfaces;
using TKA.Messages;
using TKA.Models;
using Xamarin.Forms;

namespace TKA.ViewModels
{
    public class AllSongsPageViewModel : BaseViewModel
    {
        private enum ChangePageDirection { Up, Down }

        private readonly IComms _comms;
        private readonly KTimer _timer;
        private TaskCompletionSource<bool> _songsTaskCompletionSource;
        private bool _gatheringSongs;
        private int _songsPerPage;
        private int _thisPage;
        private int _totalPages;
        private bool _showPages;
        private int _songListCount;
        private bool _nextPageEnabled;
        private ObservableCollection<Song> _songs;
        private string _searchText;
        private string _lastSearchText;
        private bool _previousPageEnabled;

        public ObservableCollection<Song> Songs
        {
            get => _songs;
            set => SetProperty(ref _songs, value);
        }

        public bool GatheringSongs
        {
            get => _gatheringSongs;
            set => SetProperty(ref _gatheringSongs, value);
        }

        public int ThisPage
        {
            get => _thisPage;
            set => SetProperty(ref _thisPage, value, otherPropertiesToRaise: new []{nameof(PageText)});
        }

        public int TotalPages
        {
            get => _totalPages;
            set => SetProperty(ref _totalPages, value, otherPropertiesToRaise: new[] { nameof(PageText) });
        }

        public bool ShowPages
        {
            get => _showPages;
            set => SetProperty(ref _showPages, value);
        }

        public int SongListCount
        {
            get => _songListCount;
            set => SetProperty(ref _songListCount, value);
        }

        public bool NextPageEnabled
        {
            get => _nextPageEnabled;
            set => SetProperty(ref _nextPageEnabled, value);
        }

        public bool PreviousPageEnabled
        {
            get => _previousPageEnabled;
            set => SetProperty(ref _previousPageEnabled, value);
        }

        public string AdUnitId
        {
            get
            {
                switch (Device.RuntimePlatform)
                {
                    case Device.iOS: return "ca-app-pub-8064175537251762/7121828858";
                    case Device.Android: return "ca-app-pub-8064175537251762/5376594503";
                    default: return "";
                }
            }
        }

        public string SearchText
        {
            get => _searchText;
            set => SetProperty(ref _searchText, value, onChangedAction: SearchTextChanged);
        }

        public bool ShowAds => RuntimeData.TheRuntimeData.ShowAds;
        public string PageText => $"Page {ThisPage} of {TotalPages}";

        public ICommand RefreshList { get; }
        public ICommand NextPageCommand { get; }
        public ICommand PreviousPageCommand { get; }
        public ICommand SongTappedCommand { get; }

        public AllSongsPageViewModel()
        {
            _comms = DependencyService.Get<IComms>();
            MessagingCenter.Subscribe<HostChosenMessage>(this, "", async msg => await LoadSongs(msg.Host.Id));
            if (!string.IsNullOrEmpty(RuntimeData.TheRuntimeData.ChosenHostId))
            {
                Task.Factory.StartNew(async () => await LoadSongs(RuntimeData.TheRuntimeData.ChosenHostId));
            }

            _timer = new KTimer
            {
                Delay = 1000,
                OnTimerFired = async (sender, args) =>
                {
                    if (_lastSearchText == _searchText) return;
                    await DoNewSearch();
                }
            };

            NextPageCommand = new Command(async () => await ChangePage(ChangePageDirection.Up));
            PreviousPageCommand = new Command(async () => await ChangePage(ChangePageDirection.Down));
            SongTappedCommand = new Command<Song>(song =>
            {
                MessagingCenter.Send(new SongTappedMessage(song), "");
                Shell.Current.GoToAsync(Routes.RequestSongRoute);
            });
            RefreshList = new Command(async () =>
            {
                await RepeatSearch();
                MessagingCenter.Send(new ListViewRefreshEndedMessage(), ListViewRefreshEndedMessage.Songs);
            });
        }

        private async Task RepeatSearch()
        {
            GatheringSongs = true;
            try
            {
                var firstRow = (_thisPage - 1) * _songsPerPage;
                if (firstRow < 0) firstRow = 0;
                var songsResult = await _comms.SongList(App.Current.CurrentHostId, Settings.ExactMatch, _lastSearchText, firstRow, Settings.TitleFirst);
                await ProcessSongs(songsResult);
            }
            catch (Exception ee)
            {
                Crashes.TrackError(ee);
                Debug.WriteLine($"HostViewModel.DoNewSearch had exception {ee}");
                var m = ee.Message;
                MessagingCenter.Send(new ErrorMessage { ErrorText = m }, "");
            }
            finally
            {
                GatheringSongs = false;
            }
        }

        private int ChangePageUp()
        {
            var firstRow = _thisPage * _songsPerPage;
            ThisPage++;
            PreviousPageEnabled = true;
            NextPageEnabled = _thisPage < _totalPages;
            return firstRow;
        }

        private int ChangePageDown()
        {
            // Can't remember why the -2, but... ;)
            var firstRow = (_thisPage - 2) * _songsPerPage;
            ThisPage--;
            NextPageEnabled = true;
            PreviousPageEnabled = ThisPage > 1;
            return firstRow;
        }

        private async Task ChangePage(ChangePageDirection direction)
        {
            GatheringSongs = true;
            try
            {
                var firstRow = direction == ChangePageDirection.Up ? ChangePageUp() : ChangePageDown();
                var songsResult = await _comms.SongList(App.Current.CurrentHostId, Settings.ExactMatch, _lastSearchText, firstRow, Settings.TitleFirst);
                await ProcessSongs(songsResult);
            }
            catch (Exception ee)
            {
                Crashes.TrackError(ee);
                Debug.WriteLine($"HostViewModel.DoNewSearch had exception {ee}");
                var m = ee.Message;
                MessagingCenter.Send(new ErrorMessage { ErrorText = m }, "");
            }
            finally
            {
                GatheringSongs = false;
            }
        }

        private async Task DoNewSearch()
        {
            Debug.WriteLine("HostViewModel.DoNewSearch started");
            try
            {
                GatheringSongs = true;
                _lastSearchText = _searchText;
                // This gets a count first.
                var countResult = await _comms.SongList(App.Current.CurrentHostId, Settings.ExactMatch, _searchText, 0,
                    Settings.TitleFirst, true);
                ProcessSongCountResult(countResult);
                PreviousPageEnabled = false;
                NextPageEnabled = ShowPages;
                // Then we get the first page of proper results.
                var songsResult = await _comms.SongList(App.Current.CurrentHostId, Settings.ExactMatch, _searchText, 0,
                    Settings.TitleFirst);
                await ProcessSongs(songsResult);
            }
            catch (Exception ee)
            {
                Crashes.TrackError(ee);
                Debug.WriteLine($"HostViewModel.DoNewSearch had exception {ee}");
                var m = ee.Message;
                MessagingCenter.Send(new ErrorMessage { ErrorText = m }, "");
            }
            finally
            {
                Debug.WriteLine("HostViewModel.DoNewSearch ended");
                GatheringSongs = false;
            }
        }

        private void SearchTextChanged()
        {
            if (_searchText.Length >= 3 || _searchText.Length == 0)
                _timer.ResetTimer();
        }

        private async Task LoadSongs(string hostId)
        {
            if (_songsTaskCompletionSource != null)
            {
                await _songsTaskCompletionSource.Task;
            }

            GatheringSongs = true;
            try
            {
                _songsTaskCompletionSource = new TaskCompletionSource<bool>();
                var songListCountResult = await _comms.SongListCount(hostId);
                if (songListCountResult.IsSuccess)
                {
                    ProcessSongCountResult(songListCountResult);
                    var songList = await _comms.SongList(hostId, orderByTitle: Settings.TitleFirst);
                    if (songList.IsSuccess)
                    {
                        await ProcessSongs(songList);
                        NextPageEnabled = true;
                        NextPageEnabled = ShowPages;
                    }
                    else
                    {
                        await UserDialogs.Instance.AlertAsync("Unable to get song list. Please try again later", "The Karaoke App");
                    }
                }
            }
            finally
            {
                GatheringSongs = false;
            }

            _songsTaskCompletionSource.SetResult(true);
        }

        private void ProcessSongCountResult(CommsResult count)
        {
            Debug.WriteLine("HostViewModel.ProcessSongCountResult started");
            if (count.IsSuccess == false)
            {
                GatheringSongs = false;
                throw new Exception($"Unable to get song list. Please try again later. [{count.StatusCode}]");
            }
            var bits = count.RawResponse.Split(' ');
            if (bits.Length != 2)
            {
                GatheringSongs = false;
                throw new Exception($"Failed to get song list. Please try again later: {count.RawResponse}");
            }
            SongListCount = int.Parse(bits[0]);
            _songsPerPage = int.Parse(bits[1]);
            if (SongListCount > _songsPerPage)
            {
                ShowPages = true;
                TotalPages = SongListCount / _songsPerPage;
                if (_songsPerPage * TotalPages < SongListCount)
                    TotalPages++;
                ThisPage = 1;
            }
            else
            {
                ShowPages = false;
            }
            Debug.WriteLine("HostViewModel.ProcessSongCountResult ended");
        }

        private async Task ProcessSongs(CommsResult result)
        {
            Debug.WriteLine("HostViewModel.ProcessSongs started");
            List<Song> songs;
            try
            {
                songs = result.Deserialise<List<Song>>();
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"HostViewModel.ProcessSongs failed for some reason {ex.Message}");
                return;
            }

            await App.BeginInvokeOnMainThreadAsync(() =>
            {
                Debug.WriteLine("HostViewModel.ProcessSongs thread started");
                Song currentSong = null;
                int songsProcessed = 0;
                try
                {
                    Songs = new ObservableCollection<Song>();
                    foreach (var song in songs)
                    {
                        currentSong = song;
                        Songs.Add(song);
                        songsProcessed++;
                    }
                }
                catch (Exception e)
                {
                    var exText = $"Exception with song num {songsProcessed} {currentSong?.Title} by {currentSong?.Artist}: {e.Message}";
                    Debug.WriteLine($"HostViewModel.ProcessSongs thread had exception {exText}");
                    MessagingCenter.Send(new ErrorMessage { ErrorText = exText }, "");
                }
                Debug.WriteLine("HostViewModel.ProcessSongs thread ended");
            });
            Debug.WriteLine("HostViewModel.ProcessSongs ended");
        }
    }
}
