﻿using System.Threading.Tasks;
using System.Windows.Input;
using Acr.UserDialogs;
using TKA.Helpers;
using TKA.Messages;
using TKA.Models;
using TKA.Services;
using Xamarin.Forms;

namespace TKA.ViewModels
{
    [QueryProperty("Title", "title")]
    [QueryProperty("Artist", "artist")]
    [QueryProperty("TempoChange", "tempochange")]
    [QueryProperty("KeyChange", "keychange")]
    public class RequestSongPageViewModel : BaseViewModel
    {
        private readonly TheKaraokeAppServerComms _comms;
        private string _selectedRequestType = "Solo";

        private string _title;
        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }

        private string _artist;
        public string Artist
        {
            get => _artist;
            set => SetProperty(ref _artist, value);
        }

        private string _singer = Settings.LastRequestName;
        public string Singer
        {
            get => _singer;
            set => SetProperty(ref _singer, value);
        }

        private string _duetName;
        public string DuetName
        {
            get => _duetName;
            set => SetProperty(ref _duetName, value);
        }

        private string _groupName;
        public string GroupName
        {
            get => _groupName;
            set => SetProperty(ref _groupName, value);
        }

        private string _tempoChange;
        public string TempoChange
        {
            get => _tempoChange;
            set => SetProperty(ref _tempoChange, value);
        }

        private string _keyChange;
        public string KeyChange
        {
            get => _keyChange;
            set => SetProperty(ref _keyChange, value);
        }

        public string SelectedRequestType
        {
            get => _selectedRequestType;
            set //=> SetProperty(ref _selectedRequestType, value);
            {
                if (SetProperty(ref _selectedRequestType, value))
                {
                    ShowDuet = ShowGroup = false;
                    switch (value)
                    {
                        case "Duet": ShowDuet = true; break;
                        case "Group": ShowGroup = true; break;
                    }
                }
            }
        }

        private bool _showDuet;
        public bool ShowDuet
        {
            get => _showDuet;
            set => SetProperty(ref _showDuet, value);
        }

        private bool _showGroup;
        public bool ShowGroup
        {
            get => _showGroup;
            set => SetProperty(ref _showGroup, value);
        }

        public ICommand RequestSongCommand { get; }

        public RequestSongPageViewModel()
        {
            RequestSongCommand = new Command(async () => await RequestSong());
            _comms = DependencyService.Resolve<TheKaraokeAppServerComms>();

            MessagingCenter.Subscribe<SongTappedMessage>(this, "", msg =>
            {
                Title = msg.Song.Title;
                Artist = msg.Song.Artist;
                SelectedRequestType = "Solo";
                GroupName = "";
                DuetName = "";
                TempoChange = "";
                KeyChange = "";
            });
        }

        private async Task RequestSong()
        {
            if (string.IsNullOrEmpty(Title) || string.IsNullOrEmpty(Artist) || string.IsNullOrEmpty(Singer))
            {
                await UserDialogs.Instance.AlertAsync("Please enter Title, Artist and Singer name", "The Karaoke App");
                return;
            }

            if (ShowDuet && string.IsNullOrEmpty(DuetName))
            {
                await UserDialogs.Instance.AlertAsync("Please enter Duet Name", "The Karaoke App");
                return;
            }

            if (ShowGroup && string.IsNullOrEmpty(GroupName))
            {
                await UserDialogs.Instance.AlertAsync("Please enter Group Name", "The Karaoke App");
                return;
            }

            var result = await _comms.RequestSong(Title, Artist, Singer, DuetName, GroupName, TempoChange, KeyChange);
            if (result == RequestSongResult.Success)
            {
                Helpers.Settings.LastRequestName = Singer;
                Title = "";
                Artist = "";
                TempoChange = "";
                KeyChange = "";

                await Shell.Current.GoToAsync(Routes.AllSongsRoute);
            }
        }
    }
}
