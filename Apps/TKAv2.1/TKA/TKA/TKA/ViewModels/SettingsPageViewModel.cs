﻿using TKA.Helpers;
using TKA.Models;

namespace TKA.ViewModels
{
    public class SettingsPageViewModel : BaseViewModel
    {
        private bool _exactSearchMatch;
        private bool _orderByTitleFirst;

        public bool ExactSearchMatch
        {
            get => _exactSearchMatch;
            set => SetProperty(ref _exactSearchMatch, value, onChangedAction: () => Settings.ExactMatch = value);
        }

        public bool OrderByTitleFirst
        {
            get => _orderByTitleFirst;
            set => SetProperty(ref _orderByTitleFirst, value, onChangedAction: () => Settings.TitleFirst = value);
        }

        public string MainAboutText => RuntimeData.TheRuntimeData.MainAboutText;

        public SettingsPageViewModel()
        {
            _exactSearchMatch = Settings.ExactMatch;
            _orderByTitleFirst = Settings.TitleFirst;
        }
    }
}
