﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Input;
using Acr.UserDialogs;
using Newtonsoft.Json;
using TKA.Helpers;
using TKA.Interfaces;
using TKA.Messages;
using TKA.Models;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace TKA.ViewModels
{
    public class BaseHostViewModel : BaseViewModel
    {
        protected IComms Comms { get; }
        private ICommand _logoutCommand, _startShowCommand, _stopShowCommand, _clearRequestsCommand, _refreshCommand;
        private bool _isLoggedIn, _startShowAvailable, _stopShowAvailable;
        private ObservableCollection<SongRequest> _songRequests = new ObservableCollection<SongRequest>();
        private string _pageTitle;
        private bool _gatheringSongRequests, _changingShowStatus, _checkingStatus;
        private string _changingShowStatusText;

        public ICommand LogoutCommand => _logoutCommand ?? (_logoutCommand = new Command(() =>
        {
            IsLoggedIn = false;
            Settings.JWT = null;
            Comms.LoginToken = null;
        }));

        public ICommand StartShowCommand => _startShowCommand ?? (_startShowCommand = new Command(async () => await StartShow()));
        public ICommand StopShowCommand => _stopShowCommand ?? (_stopShowCommand = new Command(async () => await StopShow()));
        public ICommand ClearRequestsCommand => _clearRequestsCommand ?? (_clearRequestsCommand = new Command(() => { }));
        public ICommand RefreshCommand => _refreshCommand ?? (_refreshCommand = new Command(async () =>
        {
            await GetSongRequests();
            MessagingCenter.Send(new ListViewRefreshEndedMessage(), ListViewRefreshEndedMessage.SongRequests);
        }));

        public ObservableCollection<SongRequest> SongRequests
        {
            get => _songRequests;
            set => SetProperty(ref _songRequests, value);
        }

        public RuntimeData Data { get; set; }

        public bool StartShowAvailable
        {
            get => _startShowAvailable;
            set => SetProperty(ref _startShowAvailable, value);
        }

        public bool StopShowAvailable
        {
            get => _stopShowAvailable;
            set => SetProperty(ref _stopShowAvailable, value);
        }

        public bool IsLoggedIn
        {
            get => _isLoggedIn;
            set => SetProperty(ref _isLoggedIn, value);
        }

        public string PageTitle
        {
            get => _pageTitle;
            set => SetProperty(ref _pageTitle, value);
        }

        public bool GatheringSongRequests
        {
            get => _gatheringSongRequests;
            set => SetProperty(ref _gatheringSongRequests, value);
        }

        public bool ChangingShowStatus
        {
            get => _changingShowStatus;
            set => SetProperty(ref _changingShowStatus, value);
        }

        public string ChangingShowStatusText
        {
            get => _changingShowStatusText;
            set => SetProperty(ref _changingShowStatusText, value);
        }

        public bool CheckingStatus
        {
            get => _checkingStatus;
            set => SetProperty(ref _checkingStatus, value);
        }


        protected BaseHostViewModel()
        {
            Data = RuntimeData.TheRuntimeData;
            Comms = DependencyService.Get<IComms>();
            IsLoggedIn = !string.IsNullOrEmpty(Comms.LoginToken);
            MessagingCenter.Subscribe<LoggedInMessage>(this, "", msg => IsLoggedIn = !string.IsNullOrEmpty(Comms.LoginToken));
        }

        private async Task<bool> CheckLocationAvailability()
        {
            bool locationNotAvailable = false;
            try
            {
                var location = await Geolocation.GetLastKnownLocationAsync();

                if (location != null)
                {
                    Console.WriteLine(
                        $"Latitude: {location.Latitude}, Longitude: {location.Longitude}, Altitude: {location.Altitude}");
                }
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                // Handle not supported on device exception
                locationNotAvailable = true;
            }
            catch (FeatureNotEnabledException fneEx)
            {
                // Handle not enabled on device exception
                locationNotAvailable = true;
            }
            catch (PermissionException pEx)
            {
                // Handle permission exception
                locationNotAvailable = true;
            }
            catch (Exception ex)
            {
                // Unable to get location
                locationNotAvailable = true;
            }

            return !locationNotAvailable;
        }

        private async Task StartShow()
        {
            var locationNotAvailable = !await CheckLocationAvailability();
            if (locationNotAvailable)
            {
                if (Shell.Current is AppShell aShell)
                {
                    var selected = await aShell.DisplayActionSheetAsync(
                        "Location is turned off. If you continue, incoming requests will not be GPS confirmed.",
                        "Cancel", "Continue", "Location Settings");
                    if (selected == "Cancel") return;

                    if (selected == "Location Settings")
                    {
                        AppInfo.ShowSettingsUI();
                        return;
                    }
                }
                else return;
            }

            ChangingShowStatusText = "Starting show...";
            ChangingShowStatus = true;
            var result = await Comms.StartShow();
            if (result.IsSuccess == false)
            {
                ChangingShowStatus = false;
                if (result.StatusCode == HttpStatusCode.Conflict)
                    MessagingCenter.Send(new ErrorMessage { ErrorText = "Show was already started." }, "");
                else
                    await HandleNonSuccess(result);
            }
            else
            {
                SongRequestStorage.Clear();
                await CheckStatus(false);
                ChangingShowStatus = false;
                var hdr = "Show started.";
                if (locationNotAvailable)
                {
                    var nl = Environment.NewLine;
                    hdr += $"{nl}However, location is not available on this device.{nl}Incoming requests will not be GPS confirmed.";
                }

                await UserDialogs.Instance.AlertAsync(hdr, "The Karaoke App");
            }
        }

        private async Task StopShow()
        {
            ChangingShowStatusText = "Stopping show...";
            ChangingShowStatus = true;
            var result = await Comms.StopShow();
            if (result.IsSuccess == false)
            {
                ChangingShowStatus = false;
                if (result.StatusCode == HttpStatusCode.Conflict)
                    await UserDialogs.Instance.AlertAsync("Show was not started.", "The Karaoke App");
                else
                    await HandleNonSuccess(result);
            }
            else
            {
                await CheckStatus(false);
                ChangingShowStatus = false;
                await UserDialogs.Instance.AlertAsync("Show stopped.", "The Karaoke App");
            }
        }

        public class SongRequestComparer : IEqualityComparer<SongRequest>
        {
            #region IEqualityComparer implementation
            public bool Equals(SongRequest x, SongRequest y) => x?.Id == y?.Id;

            public int GetHashCode(SongRequest obj) => obj.Id.GetHashCode();
            #endregion
        }

        private async Task GetSongRequests()
        {
            GatheringSongRequests = true;
            await SongRequestStorage.Update();
            GatheringSongRequests = false;
        }

        private static async Task HandleNonSuccess(CommsResult result)
        {
            string errorText;
            if (result.NoConnection)
                errorText = "No connection available.";
            else if (result.Exception != null)
                errorText = $"Exception: {result.Exception.Message}";
            else
                errorText = $"Error: {result.StatusCode} {result.RawResponse}";
            await UserDialogs.Instance.AlertAsync(errorText, "The Karaoke App");
        }

        private async Task CheckStatus(bool showCheckingStatus = true, bool handleBackgrounding = true)
        {
            if (showCheckingStatus) CheckingStatus = true;
            try
            {
                var result = await Comms.GetShowStatus();
                if (result.IsSuccess == false)
                {
                    StartShowAvailable = true;
                    StopShowAvailable = false;
                    //                    App.Current.StopBackgrounding();
                }
                else
                {
                    var s = result.RawResponse;
                    if (s == "1")
                    {
                        // Show is started.
                        StartShowAvailable = false;
                        StopShowAvailable = true;
                        //if (handleBackgrounding)
                        //    App.Current.StartBackgrounding();
                    }
                    else
                    {
                        StartShowAvailable = true;
                        StopShowAvailable = false;
                        //if (handleBackgrounding)
                        //    App.Current.StopBackgrounding();
                    }
                }
            }
            catch (Exception)
            {
                StartShowAvailable = true;
                StopShowAvailable = false;
                try { } catch { }
            }

            if (showCheckingStatus) CheckingStatus = false;
        }

        private void ClearRequests()
        {
            foreach (var request in SongRequests.Where(f => f.Marked).ToList())
            {
                SongRequests.Remove(request);
            }
        }

        protected async Task DoLogin(string username, string password)
        {
            using (UserDialogs.Instance.Loading("Logging in..."))
            {
                var loginResult = await Comms.LoginHost(username, password);
                if (loginResult.IsSuccess)
                {
                    using (UserDialogs.Instance.Loading())
                    {
                        var response = loginResult.Deserialise<HostLoginResponse>();
                        // TODO: Check the jwt is valid.
                        Comms.LoginToken = response.jwt;

                        var amiahostResult = await Comms.AmIaHost();
                        AmIAHostResult realResult;
                        try
                        {
                            realResult = amiahostResult.Deserialise<AmIAHostResult>();
                        }
                        catch (JsonReaderException)
                        {
                            if (int.TryParse(amiahostResult.RawResponse, out var x))
                            {
                                realResult = new AmIAHostResult { Id = x.ToString() };
                            }
                            else
                            {
                                // Bad things. Most probably a connection issue such as a wifi requiring registration
                                await UserDialogs.Instance.AlertAsync("Failed to contact host server. Please check your internet connection and try again later.",
                                    "The Karaoke App");
                                return;
                            }
                        }

                        Comms.LoggedInHostId = realResult?.Id;
                        IsLoggedIn = true;
                        Settings.JWT = response.jwt;

                        MessagingCenter.Send(new LoggedInMessage(), "");
                        PageTitle = "Host Admin";

                        await GetSongRequests();
                        await CheckStatus();
                    }
                }
                else
                {
                    await UserDialogs.Instance.AlertAsync("Could not login with those credentials.");
                }
            }
        }
    }
}
