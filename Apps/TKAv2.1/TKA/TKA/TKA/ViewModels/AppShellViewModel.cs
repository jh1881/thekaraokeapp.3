﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Microsoft.AppCenter.Analytics;
using TKA.Helpers;
using TKA.Messages;
using TKA.Models;
using Xamarin.Forms;

namespace TKA.ViewModels
{
    public class AppShellViewModel : BaseViewModel
    {
        public RuntimeData Data { get; set; }

        public ICommand NavigatingCommand { get; set; }

        private bool _initialised;
        private bool _isLoggedIn;
        private string _title = "Host Title";
        private ImageSource _menuHeaderImageSource;
        private string _hostNameText = "Choose a host...";
        private bool _hasHostNameText;

        public string HostNameText
        {
            get => _hostNameText;
            set => SetProperty(ref _hostNameText, value);
        }

        public ImageSource MenuHeaderImageSource
        {
            get => _menuHeaderImageSource;
            set => SetProperty(ref _menuHeaderImageSource, value);
        }

        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }

        public bool IsLoggedIn
        {
            get => _isLoggedIn;
            set => SetProperty(ref _isLoggedIn, value);
        }

        public bool HasHostNameText
        {
            get => _hasHostNameText;
            set => SetProperty(ref _hasHostNameText, value);
        }

        public AppShellViewModel()
        {
            Data = RuntimeData.TheRuntimeData;
            Data.HostNum = Data.FixedHost;
            NavigatingCommand = new Command(o =>
            {
                if (o is ShellNavigatingEventArgs ee)
                {
                    Analytics.TrackEvent("ShellNavigate", new Dictionary<string, string> { { "target", ee.Target.Location.ToString() } });
                }

                if (!_initialised)
                {
                    _initialised = true;
                    return;
                }
                return;
                if (o is ShellNavigatingEventArgs e)
                {
                    var route = e.Target.Location.ToString();
                    if (route.StartsWith(Routes.HostRoute)) return;
                    if (Data.HostNum == null)
                        e.Cancel();
                }
            });
            MessagingCenter.Subscribe<HostChosenMessage>(this, "", msg =>
            {
                HostNameText = msg.Host.MenuDisplayName;
                HasHostNameText = !string.IsNullOrEmpty(HostNameText);
                if (string.IsNullOrEmpty(msg.Host.BannerImage))
                {
                    var url = $"http://thekaraokeapp.co.uk/banners/{msg.Host.Number}";
                    MenuHeaderImageSource = ImageSource.FromUri(new Uri(url));
                }
                else
                {
                    try
                    {
                        var bytes = Convert.FromBase64String(msg.Host.BannerImage);
                        MenuHeaderImageSource = ImageSource.FromStream(() => new MemoryStream(bytes));
                    }
                    catch
                    {
                        var url = $"http://thekaraokeapp.co.uk/banners/{msg.Host.Number}";
                        MenuHeaderImageSource = ImageSource.FromUri(new Uri(url));
                    }
                }
            });

            if (Data.FixedHost.HasValue)
            {
                MessagingCenter.Send(new FixedHostMessage(), "");
            }
        }

        //private static async Task<byte[]> DownloadImageAsync(string imageUrl)
        //{
        //    using (var httpClient = new HttpClient { Timeout = TimeSpan.FromSeconds(15) })
        //    {
        //        try
        //        {
        //            using (var httpResponse = await httpClient.GetAsync(imageUrl))
        //            {
        //                if (httpResponse.StatusCode == HttpStatusCode.OK)
        //                {
        //                    return await httpResponse.Content.ReadAsByteArrayAsync();
        //                }
        //                else
        //                {
        //                    //Url is Invalid
        //                    return null;
        //                }
        //            }
        //        }
        //        catch (Exception)
        //        {
        //            //Handle Exception
        //            return null;
        //        }
        //    }
        //}
    }
}
