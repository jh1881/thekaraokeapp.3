﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using Microsoft.AppCenter.Crashes;
using TKA.Helpers;
using TKA.Interfaces;
using TKA.Messages;
using TKA.Models;
using Xamarin.Forms;

namespace TKA.ViewModels
{
    class NewSongsPageViewModel : BaseViewModel
    {
        private TaskCompletionSource<bool> _songsTaskCompletionSource;
        private readonly IComms _comms;
        private ObservableCollection<Song> _recentSongs;
        private bool _gatheringSongs;

        public bool GatheringSongs
        {
            get => _gatheringSongs;
            set => SetProperty(ref _gatheringSongs, value);
        }

        public ObservableCollection<Song> RecentSongs
        {
            get => _recentSongs;
            set => SetProperty(ref _recentSongs, value);
        }

        public string AdUnitId
        {
            get
            {
                switch (Device.RuntimePlatform)
                {
                    case Device.iOS: return "ca-app-pub-8064175537251762/7121828858";
                    case Device.Android: return "ca-app-pub-8064175537251762/5376594503";
                    default: return "";
                }
            }
        }

        public bool ShowAds => RuntimeData.TheRuntimeData.ShowAds;
        public ICommand SongTappedCommand { get; }
        public ICommand RefreshList { get; }

        public NewSongsPageViewModel()
        {
            _comms = DependencyService.Get<IComms>();

            MessagingCenter.Subscribe<HostChosenMessage>(this, "", async msg => await LoadSongs(msg.Host.Id));
            if (!string.IsNullOrEmpty(RuntimeData.TheRuntimeData.ChosenHostId))
            {
                Task.Factory.StartNew(async () => await LoadSongs(RuntimeData.TheRuntimeData.ChosenHostId));
            }

            SongTappedCommand = new Command<Song>(song =>
            {
                MessagingCenter.Send(new SongTappedMessage(song), "");
                Shell.Current.GoToAsync(Routes.RequestSongRoute);
            });

            RefreshList = new Command(async () => await LoadSongs(RuntimeData.TheRuntimeData.ChosenHostId));
        }

        private async Task LoadSongs(string hostId)
        {
            if (_songsTaskCompletionSource != null)
            {
                await _songsTaskCompletionSource.Task;
            }

            GatheringSongs = true;
            _songsTaskCompletionSource = new TaskCompletionSource<bool>();
            var recentSongsResult = await _comms.RecentSongList(hostId, Helpers.Settings.TitleFirst);
            if (recentSongsResult.IsSuccess)
                await ProcessRecentSongs(recentSongsResult);

            GatheringSongs = false;
            _songsTaskCompletionSource.SetResult(true);
        }

        private async Task ProcessRecentSongs(CommsResult result)
        {
            try
            {
                var songs = result.Deserialise<List<Song>>();
                await App.BeginInvokeOnMainThreadAsync(() =>
                {
                    RecentSongs = new ObservableCollection<Song>();
                    foreach (var song in songs)
                        RecentSongs.Add(song);
                });
            }
            catch (Exception ee)
            {
                Crashes.TrackError(ee);
                Debug.WriteLine($"NewSongsPageViewModel.ProcessRecentSongs failed: {ee}");
            }
        }
    }
}
