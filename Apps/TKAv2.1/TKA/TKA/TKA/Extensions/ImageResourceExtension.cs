﻿using System;
using System.Diagnostics;
using System.Reflection;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TKA.Extensions
{
    [ContentProperty(nameof(Source))]
    public class ImageResourceExtension : IMarkupExtension
    {
        private static readonly Assembly Assembly;

        static ImageResourceExtension()
        {
            Assembly = typeof(ImageResourceExtension).GetTypeInfo().Assembly;
        }

        public string Source { get; set; }
        public object ProvideValue(IServiceProvider serviceProvider)
        {
            if (Source == null) return null;
            Debug.WriteLine($"Loading image {Source}");
            var ob = (StreamImageSource)ImageSource.FromResource(Source, Assembly);
            return ob;
        }
    }
}
