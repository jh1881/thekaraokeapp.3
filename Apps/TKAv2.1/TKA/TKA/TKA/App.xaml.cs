﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using TKA.Interfaces;
using TKA.Messages;
using TKA.Models;
using TKA.Services;
using Xamarin.Forms;
using Device = Xamarin.Forms.Device;

namespace TKA
{
    public partial class App : Application
    {
        public event EventHandler OnStartBackgrounding;
        public event EventHandler OnStopBackgrounding;

        private static ResourceDictionary _preTheme;
        private bool _backgrounding;
        private readonly IAnalytics _analytics;
        public new static App Current => Application.Current as App;
        public static IAnalytics Analytics => Current?._analytics;
        public string CurrentHostName { get; set; }
        public string CurrentHostId { get; set; }
        //        public bool CurrentHostHasUpdates { get; set; }
        public static bool ShowAds { get; set; }
        public App()
        {
            DependencyService.Register<IComms, TheKaraokeAppServerComms>();
            DependencyService.Register<IAnalytics, AppCenterAnalytics>();
            AppCenter.Start("ios=ace5c169-0cdd-48cd-9eac-107f77264af8;android=ff28dbc9-c70f-49bb-aada-c3dfca8e5229", typeof(Analytics), typeof(Crashes));

            InitializeComponent();

            if (_preTheme != null) SetTheme(_preTheme);

            MessagingCenter.Subscribe<HostChosenMessage>(this, "", msg =>
            {
                RuntimeData.TheRuntimeData.ChosenHostId = msg.Host.Id;
                RuntimeData.TheRuntimeData.HostName = msg.Host.Name;
                //                RuntimeData.TheRuntimeData.HostNum = RuntimeData.TheRuntimeData.FixedHost;

                CurrentHostId = msg.Host.Id;
                CurrentHostName = msg.Host.Name;
            });
            MessagingCenter.Subscribe<LoggedInMessage>(this, "", msg =>
            {
                var comms = DependencyService.Get<IComms>();
                if (string.IsNullOrEmpty(comms.LoginToken))
                {
                    StopBackgrounding();
                }
                else
                {
                    StartBackgrounding();
                }
            });

            MainPage = new AppShell();
        }

        public void SetTheme(ResourceDictionary dict)
        {
            var mergedDictionaries = Resources.MergedDictionaries;
            mergedDictionaries.Clear();
            mergedDictionaries.Add(dict);
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }

        public void Error(string text, Exception exception = null)
        {
            if (exception != null)
                Crashes.TrackError(exception);
            Debug.WriteLine($"App.Error {text}");
            MessagingCenter.Send(new ErrorMessage { ErrorText = text }, "");
        }

        public static Task BeginInvokeOnMainThreadAsync(Action action)
        {
            Debug.WriteLine($"App.BeginInvokeOnMainThreadAsync started");
            var tcs = new TaskCompletionSource<object>();
            Device.BeginInvokeOnMainThread(() =>
            {
                try
                {
                    action();
                    Debug.WriteLine($"App.BeginInvokeOnMainThreadAsync action completed without error");
                    tcs.SetResult(null);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine($"App.BeginInvokeOnMainThreadAsync action completed with exception {ex}");
                    tcs.SetException(ex);
                }
            });
            return tcs.Task;
        }

        public void StartBackgrounding()
        {
            Analytics?.Track("StartBackgrounding", "_backgrounding", $"{_backgrounding}");
            if (_backgrounding)
                return;
            _backgrounding = true;
            OnStartBackgrounding?.Invoke(this, new EventArgs());
        }

        public void StopBackgrounding()
        {
            Analytics?.Track("StopBackgrounding", "_backgrounding", $"{_backgrounding}");
            if (!_backgrounding)
                return;
            _backgrounding = false;
            OnStopBackgrounding?.Invoke(this, new EventArgs());
        }
    }
}
