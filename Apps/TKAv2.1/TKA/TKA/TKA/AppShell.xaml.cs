﻿using System;
using System.Threading.Tasks;
using Microsoft.AppCenter.Crashes;
using TKA.Helpers;
using TKA.Interfaces;
using TKA.Messages;
using TKA.Models;
using Xamarin.Forms;

namespace TKA
{
    public partial class AppShell : Xamarin.Forms.Shell
    {
        public AppShell()
        {
            MessagingCenter.Subscribe<LoggedInMessage>(this, "", msg =>
            {
                var comms = DependencyService.Get<IComms>();
                if (string.IsNullOrEmpty(comms.LoginToken))
                {
                    // Logged out
                }
                else
                {
                    // Logged In
                    Device.BeginInvokeOnMainThread(() => Current.GoToAsync(Routes.HostLoginRoute));
                }
            });
            MessagingCenter.Subscribe<FixedHostMessage>(this, "", async msg =>
            {
                try
                {
                    var comms = DependencyService.Get<IComms>();
                    var result = await comms.ChooseHost(RuntimeData.TheRuntimeData.FixedHost.ToString());
                    if (result.IsSuccess)
                    {
                        var host = result.Deserialise<Host>();
                        RuntimeData.TheRuntimeData.HostNum = RuntimeData.TheRuntimeData.FixedHost;
                        MessagingCenter.Send(new HostChosenMessage(host), "");

                        HostFlyoutItem.Items.Add(FixedHostLoginTab);
                        HostFlyoutItem.Items.Remove(ChooseHostFlyoutTab);
                    }
                    else
                    {
                    }
                }
                catch (Exception ee)
                {
                    Crashes.TrackError(ee);
                }
                Device.BeginInvokeOnMainThread(() => Current.GoToAsync(Routes.AllSongsRoute));
            });

            InitializeComponent();

            HostFlyoutItem.Items.Remove(FixedHostLoginTab);

            //// TODO: Remove this when Maps are done!
            HostFlyoutItem.Items.Remove(HostMapTab);
        }

        public async Task<string> DisplayActionSheetAsync(string message, string cancel, string destruction, params string[] buttons)
        {
            return await DisplayActionSheet(message, cancel, destruction, buttons);
        }
    }
}
