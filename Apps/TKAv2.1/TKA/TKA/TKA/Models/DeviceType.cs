﻿namespace TKA.Models
{
    public enum DeviceType
    {
        Ios = 1,
        Android = 2,
        Windows = 3
    }
}