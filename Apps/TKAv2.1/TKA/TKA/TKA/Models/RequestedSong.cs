﻿using System;
using Newtonsoft.Json;

namespace TKA.Models
{
    public class RequestedSong
    {
        public Song Song { get; set; }
        public DateTime TimeRequested { get; set; }
        public string KeyChange { get; set; }
        public string TempoChange { get; set; }

        [JsonIgnore] public string Artist => Song?.Artist;
        [JsonIgnore] public string Title => Song?.Title;
    }
}
