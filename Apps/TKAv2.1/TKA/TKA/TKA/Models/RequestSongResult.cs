﻿namespace TKA.Models
{
    public enum RequestSongResult
    {
        Success,
        ServerError,
        ShowNotInProgress,
        HostNotFound,
        BadRequest,
        NoConnection,
        TooManyRequests,
        UnknownError
    }
}