﻿namespace TKA.Models
{
    public enum AnalyticsSeverity
    {
        Warning,
        Error,
        Critical
    }
}
