﻿namespace TKA.Models
{
    public class HostLoginResponse
    {
        public string jwt { get; set; }
    }
}
