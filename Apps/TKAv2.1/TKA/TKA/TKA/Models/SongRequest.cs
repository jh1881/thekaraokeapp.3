﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using Newtonsoft.Json;

namespace TKA.Models
{
    public class SongRequest : BaseNotifyPropertyChanged
    {
        private bool _marked;
        private bool _cancelled;
        private bool _purged;

        private int _id;
        public int Id
        {
            get => _id;
            set => SetProperty(ref _id, value);
        }

        private string _title;
        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }

        private string _artist;
        public string Artist
        {
            get => _artist;
            set => SetProperty(ref _artist, value);
        }

        private string _keyChange;
        public string KeyChange
        {
            get => _keyChange;
            set => SetProperty(ref _keyChange, value, null, new[] { nameof(KeyChangeVisible) });
        }

        private string _tempoChange;
        public string TempoChange
        {
            get => _tempoChange;
            set => SetProperty(ref _tempoChange, value, null, new[] { nameof(TempoChangeVisible) });
        }

        private string _requestor;
        public string Requestor
        {
            get => _requestor;
            set => SetProperty(ref _requestor, value);
        }

        private string _duetSecond;
        public string DuetSecond
        {
            get => _duetSecond;
            set => SetProperty(ref _duetSecond, value, null, new[] { nameof(DuetVisible) });
        }

        private DateTime _requestedTime;
        public DateTime RequestedTime
        {
            get => _requestedTime;
            set => SetProperty(ref _requestedTime, value, null, new[] { nameof(ShortTime) });
        }

        private bool _notified;
        [JsonConverter(typeof(JsonBoolConverter))]
        public bool Notified
        {
            get => _notified;
            set => SetProperty(ref _notified, value);
        }

        private string _groupName;
        public string GroupNames
        {
            get => _groupName;
            set => SetProperty(ref _groupName, value, null, new[] { nameof(GroupVisible) });
        }

        private int _gpsResult;
        public int GpsResult
        {
            get => _gpsResult;
            set => SetProperty(ref _gpsResult, value, null, new[] { nameof(HasGoodGps) });
        }

        public bool HasGoodGps => GpsResult == 1;

        public bool Cancelled
        {
            get => _cancelled;
            set { _cancelled = value; OnPropertyChanged(); }
        }

        public bool Marked
        {
            get => _marked;
            set
            {
                _marked = value; OnPropertyChanged();
                SongRequestStorage.SaveChanges();
            }
        }

        public bool Purged
        {
            get => _purged;
            set
            {
                _purged = value;
                OnPropertyChanged();
            }
        }

        public bool DuetVisible => !string.IsNullOrEmpty(DuetSecond);
        public bool GroupVisible => !string.IsNullOrEmpty(GroupNames);
        public bool KeyChangeVisible => !string.IsNullOrEmpty(KeyChange);
        public bool TempoChangeVisible => !string.IsNullOrEmpty(TempoChange);
        public string ShortTime => RequestedTime.ToString("HH:mm");
    }
}
