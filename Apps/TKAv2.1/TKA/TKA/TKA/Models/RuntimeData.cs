﻿using Xamarin.Forms;

namespace TKA.Models
{
    public class RuntimeData : BaseNotifyPropertyChanged
    {
        private ImageSource _hostImage;
        private string _hostName;
        public int? FixedHost { get; set; }
        public static RuntimeData TheRuntimeData { get; } = new RuntimeData();

        public int? HostNum { get; set; }
        public string ChosenHostId { get; set; }

        public string HostName
        {
            get => _hostName;
            set => SetProperty(ref _hostName, value);
        }

        public ImageSource HostImage
        {
            get => _hostImage;
            set => SetProperty(ref _hostImage, value);
        }

        public bool ShowAds { get; set; } = true;
        public string MainAboutText { get; set; }
    }
}
