﻿using TKA.Models;

namespace TKA.Messages
{
    class SongRequestedMessage
    {
        public SongRequest Request { get; }
        public SongRequestedMessage(SongRequest request)
        {
            Request = request;
        }
    }
}
