﻿using TKA.Models;

namespace TKA.Messages
{
    internal class HostChosenMessage
    {
        public Host Host { get; }
        public HostChosenMessage(Host host) => Host = host;
    }
}
