﻿using TKA.Models;

namespace TKA.Messages
{
    class SongTappedMessage
    {
        public Song Song { get; }

        public SongTappedMessage(Song song)
        {
            Song = song;
        }
    }
}
