﻿using System;
using System.Linq;
using DJWayniesApp.DataModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Diagnostics;
using Acr.UserDialogs;
using DJWayniesApp.Interfaces;
using Xamarin.Forms;
using DJWayniesApp.Messages;

namespace DJWayniesApp
{
    public static class SongRequestStorage
    {
        public class SongRequestsReceivedArgs : EventArgs
        {
            public int NewSongRequestsReceived { get; }

            public SongRequestsReceivedArgs(int newSongRequestsReceived)
            {
                NewSongRequestsReceived = newSongRequestsReceived;
            }
        }

        public static ObservableCollection<SongRequest> SongRequests { get; private set; } = new ObservableCollection<SongRequest>();

        public static event EventHandler<SongRequestsReceivedArgs> OnNewSongRequestsReceived;

        // On Init, we get the current song requests.
        // We then allow other tasks (such as the background task) to add new songs or update
        //   existing songs.
        public static async Task Init()
        {
            Debug.WriteLine("SongRequestStorage.Init started");
            try
            {
                // Load the current set from file.
                var requests = (Helpers.Settings.CurrentSongRequests ?? new List<SongRequest>())
                    .Where(s => s.RequestedTime.Date < DateTime.Today);
                SongRequests = new ObservableCollection<SongRequest>(requests);
                Helpers.Settings.CurrentSongRequests = SongRequests.ToList();
                Debug.WriteLine($"SongRequestStorage.Init has {SongRequests.Count} current song requests");

                // Now update the requests.
                await Update();
            }
            catch (Exception e)
            {
                Debug.WriteLine($"SongRequestStorage.Init got exception: {e}");
                MessagingCenter.Send(new ErrorMessage { ErrorText = $"Exception: {e.Message}" }, "");
            }
            Debug.WriteLine("SongRequestStorage.Init ended");
        }

        public static void Clear()
        {
            SongRequests.Clear();
            Helpers.Settings.CurrentSongRequests = new List<SongRequest>();
        }

        private static bool _isUpdating;

        public static void SaveChanges()
        {
            Helpers.Settings.CurrentSongRequests = SongRequests.ToList();
        }

        // Returns the number of new songs.
        public static async Task<int> Update()
        {
            Debug.WriteLine("SongRequestStorage.Update started");
            if (_isUpdating) return 0;
            var newSongCount = 0;
            _isUpdating = true;
            try
            {
                var result = await Comms.UnreadSongRequests();
                Debug.WriteLine($"SongRequestStorage.Update UnreadNotifications got {result}");
                if (result.IsSuccess)
                {
                    List<SongRequest> requests;
                    try
                    {
                        requests = result.Deserialise<List<SongRequest>>();
                    }
                    catch (Exception)
                    {
                        App.Current.Error("Failed to contact host server. Please check your internet connection and try again later.");
                        newSongCount = -1;
                        return newSongCount;
                    }

                    DependencyService.Get<IPushNotifications>()?.ClearBadge();

                    await App.BeginInvokeOnMainThreadAsync(() =>
                    {
                        foreach (var existingSong in requests)
                        {
                            var song = SongRequests.FirstOrDefault(s => s.Id == existingSong.Id);
                            if (song != null)
                            {
                                song.Cancelled = existingSong.Cancelled;
                            }
                            else
                            {
                                SongRequests.Add(existingSong);
                                newSongCount++;
                            }
                        }
                    });
                    if (newSongCount > 0)
                    {
                        UserDialogs.Instance.Toast("New song requests receiced");
//                        DependencyService.Get<IToast>()?.ShowToast("New song requests received", 250);
                        OnNewSongRequestsReceived?.Invoke(null, new SongRequestsReceivedArgs(newSongCount));
                    }
                    Helpers.Settings.CurrentSongRequests = SongRequests.ToList();
                }
                else
                {
                    HandleNonSuccess(result);
                    newSongCount = -1;
                }
            }
            finally
            {
                _isUpdating = false;
            }
            Debug.WriteLine($"SongRequestStorage.Update ended with newSongCount = {newSongCount}");
            return newSongCount;
        }

        private static void HandleNonSuccess(Comms.CommsResult result)
        {
            string errorText;
            if (result.NoConnection)
                errorText = "No connection available.";
            else if (result.Exception != null)
                errorText = $"Exception: {result.Exception.Message}";
            else
                errorText = $"Error: {result.StatusCode} {result.RawResponse}";
            MessagingCenter.Send(new ErrorMessage { ErrorText = errorText }, "");
        }
    }
}
