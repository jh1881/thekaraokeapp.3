using System;
using TKAShared;

namespace DJWayniesApp
{
    public class CommsDataProvider : ICommsDataProvider
    {
        public string HostId => App.Current.CurrentHostId;
        public Guid Uid => Helpers.Settings.Id;
    }
}