﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using Autofac;
using DJWayniesApp.Alerts;
using Xamarin.Forms;
using DJWayniesApp.DataModel;
using DJWayniesApp.Messages;
using Plugin.Geolocator;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;

namespace DJWayniesApp.ViewModels
{
    class HostControlViewModel : INotifyPropertyChanged
    {
        #region Members
        private bool _startShowAvailable;
        private bool _stopShowAvailable;
        private bool _showHours;
        private int _showHoursToRun;
        private bool _gatheringSongRequests;
        private bool _changingShowStatus;
        private string _changingShowStatusText;
        private bool _checkingStatus;
        #endregion

        #region Properties
        public ObservableCollection<SongRequest> SongRequests { get; set; }
        public ICommand RefreshCommand { get; set; }
        public ICommand StopShowCommand { get; set; }
        public ICommand StartShowCommand { get; set; }
        public ICommand GoToNormalViewCommand { get; set; }
        public ICommand LogoutCommand { get; set; }
        public ICommand ClearRequestsCommand { get; set; }

        public bool CheckingStatus
        {
            get => _checkingStatus;
            set { _checkingStatus = value; OnPropertyChanged(); }
        }
        public bool StartShowAvailable
        {
            get => _startShowAvailable;
            set { _startShowAvailable = value; OnPropertyChanged(); }
        }

        public bool StopShowAvailable
        {
            get => _stopShowAvailable;
            set { _stopShowAvailable = value; OnPropertyChanged(); }
        }

        public bool ShowHours
        {
            get => _showHours;
            set { _showHours = value; OnPropertyChanged(); }
        }

        public int ShowHoursToRun
        {
            get => _showHoursToRun;
            set { _showHoursToRun = value; OnPropertyChanged(); }
        }

        public bool GatheringSongRequests
        {
            get => _gatheringSongRequests;
            set { _gatheringSongRequests = value; OnPropertyChanged(); }
        }

        public bool ChangingShowStatus
        {
            get => _changingShowStatus;
            set { _changingShowStatus = value; OnPropertyChanged(); }
        }

        public string ChangingShowStatusText
        {
            get => _changingShowStatusText;
            set { _changingShowStatusText = value; OnPropertyChanged(); }
        }
        #endregion

        public HostControlViewModel()
        {
            StartShowCommand = new Command(async () => await StartShow());
            StopShowCommand = new Command(async () => await StopShow());
            //            SongRequests = new ObservableCollection<SongRequest>();
            SongRequests = SongRequestStorage.SongRequests;
            LogoutCommand = new Command(() => App.Current.LoggedOut());

            RefreshCommand = new Command(async () =>
                {
                    await GetSongRequests();
                    MessagingCenter.Send(new ListViewRefreshEndedMessage(), "HostControlViewModel");
                });
            GoToNormalViewCommand = new Command(() => MessagingCenter.Send(new SwitchToSingerViewMessage(), ""));
            ClearRequestsCommand = new Command(() =>
            {
                foreach (var request in SongRequests.Where(f => f.Marked).ToList())
                {
                    SongRequests.Remove(request);
                }
            });
            Task.Factory.StartNew(async () =>
                {
                    await GetSongRequests();
                    await CheckStatus();
                });
        }

        private async Task CheckStatus(bool showCheckingStatus = true, bool handleBackgrounding = true)
        {
            if (showCheckingStatus)
                CheckingStatus = true;

            try
            {
                var result = await Comms.GetShowStatus();
                if (result.IsSuccess == false)
                {
                    StartShowAvailable = true;
                    StopShowAvailable = false;
                    App.Current.StopBackgrounding();
                }
                else
                {
                    var s = result.RawResponse;
                    if (s == "1")
                    {
                        // Show is started.
                        StartShowAvailable = false;
                        StopShowAvailable = true;
                        if (handleBackgrounding)
                            App.Current.StartBackgrounding();
                    }
                    else
                    {
                        StartShowAvailable = true;
                        StopShowAvailable = false;
                        if (handleBackgrounding)
                            App.Current.StopBackgrounding();
                    }
                }
            }
            catch (Exception)
            {
                StartShowAvailable = true;
                StopShowAvailable = false;
                // ReSharper disable once EmptyGeneralCatchClause
                try { App.Current.StopBackgrounding(); } catch { }
            }

            if (showCheckingStatus) CheckingStatus = false;
        }

        public class SongRequestComparer : IEqualityComparer<SongRequest>
        {
            #region IEqualityComparer implementation
            public bool Equals(SongRequest x, SongRequest y)
            {
                return x.Id == y.Id;
            }

            public int GetHashCode(SongRequest obj)
            {
                return obj.Id.GetHashCode();
            }
            #endregion
        }

        private async Task GetSongRequests()
        {
            GatheringSongRequests = true;
            await SongRequestStorage.Update();
            GatheringSongRequests = false;
        }

        private static void HandleNonSuccess(Comms.CommsResult result)
        {
            string errorText;
            if (result.NoConnection)
                errorText = "No connection available.";
            else if (result.Exception != null)
                errorText = $"Exception: {result.Exception.Message}";
            else
                errorText = $"Error: {result.StatusCode} {result.RawResponse}";
            MessagingCenter.Send(new ErrorMessage { ErrorText = errorText }, "");
        }

        private async Task StopShow()
        {
            ChangingShowStatusText = "Stopping show...";
            ChangingShowStatus = true;
            var result = await Comms.StopShow();
            if (result.IsSuccess == false)
            {
                ChangingShowStatus = false;
                if (result.StatusCode == HttpStatusCode.Conflict)
                    MessagingCenter.Send(new ErrorMessage { ErrorText = "Show was not started." }, "");
                else
                    HandleNonSuccess(result);
            }
            else
            {
                await CheckStatus(false);
                ChangingShowStatus = false;
                MessagingCenter.Send(new DisplayAlertMessage { Header = "Show stopped." }, "");
            }
        }

        private async Task StartShow()
        {
            bool locationNotAvailable = false;
            if (CrossGeolocator.IsSupported)
            {
                var d = await CrossPermissions.Current.RequestPermissionsAsync(Permission.LocationWhenInUse);
                var pS = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.LocationWhenInUse);
                if (pS == PermissionStatus.Disabled || pS == PermissionStatus.Unknown)
                {
                    // Location not available on this device.
                    locationNotAvailable = true;
                }
                if(pS == PermissionStatus.Denied || pS == PermissionStatus.Restricted)
                {
                    using (var scope = App.Current.Container.BeginLifetimeScope())
                    {
                        var dlgProvider = scope.Resolve<IDialogProvider>();
                        var selected = await dlgProvider.DisplayActionSheet(
                            "Location is turned off. If you continue, incoming requests will not be GPS confirmed",
                            "Cancel", "Continue", "Location Settings");
                        if (selected == "Cancel")
                        {
                            return;
                        }
                        if (selected == "Location Settings")
                        {
                            CrossPermissions.Current.OpenAppSettings();
                            return;
                        }
                    }
                }
            }

            ChangingShowStatusText = "Starting show...";
            ChangingShowStatus = true;
            var result = await Comms.StartShow();
            if (result.IsSuccess == false)
            {
                ChangingShowStatus = false;
                if (result.StatusCode == HttpStatusCode.Conflict)
                    MessagingCenter.Send(new ErrorMessage { ErrorText = "Show was already started." }, "");
                else
                    HandleNonSuccess(result);
            }
            else
            {
                SongRequestStorage.Clear();
                await CheckStatus(false);
                ChangingShowStatus = false;
                var hdr = "Show started.";
                if (locationNotAvailable)
                {
                    hdr += "\r\nHowever, location is not available on this device.\r\nIncoming requests will not be GPS confirmed";
                }
                MessagingCenter.Send(new DisplayAlertMessage { Header = hdr }, "");
            }
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
