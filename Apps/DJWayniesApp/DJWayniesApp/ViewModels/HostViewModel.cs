﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using DJWayniesApp.DataModel;
using Xamarin.Forms;
using DJWayniesApp.Messages;

namespace DJWayniesApp.ViewModels
{
    [SuppressMessage("ReSharper", "ExplicitCallerInfoArgument")]
    public class HostViewModel : INotifyPropertyChanged
    {
        private enum ChangePageDirection
        {
            Up, Down
        }

        private ObservableCollection<Song> _songs = new ObservableCollection<Song>();
        private ObservableCollection<Song> _recentSongs = new ObservableCollection<Song>();

        private bool _requestingDuet;
        private string _requestName;
        private string _requestName2;
        private string _requestSongTitle;
        private string _requestSongArtist;
        private string _requestTempoChange;
        private string _requestKeyChange;

        private PageType _selectedPageIndex;
        private bool _gatheringSongs;
        private int _songListCount;
        private int _thisPage;
        private int _totalPages;
        private bool _showPages;
        private int _songsPerPage;
        private string _searchText;
        private bool _previousPageEnabled;
        private bool _nextPageEnabled;
        private readonly KTimer _timer;
        private bool _requestingSong;
        private Dictionary<int, string> _requestTypeList;
        private int _selectedIndex;
        private ObservableCollection<RequestedSong> _lastRequestedSongs;
        private bool _showLastRequestedSongs;

        private string _lastSearchText = "";
        private string _requestNameGroup;
        private bool _showAds = App.Current.ShowAds;

        private void UpdateShowLastRequestedSongs()
        {
            ShowLastRequestedSongs = !(_lastRequestedSongs?.Count <= 0);
        }

        public HostViewModel()
        {
            Debug.WriteLine("HostViewModel Constructor started");

            Debug.WriteLine("HostViewModel.DoNewSearch making _requestTypeList");
            _requestTypeList = new Dictionary<int, string> { { 0, "Solo" }, { 1, "Duet" }, { 2, "Group" } };

            Debug.WriteLine("HostViewModel.DoNewSearch making _timer");
            _timer = new KTimer
            {
                Delay = 1000,
                OnTimerFired = async (sender, args) =>
                {
                    if (_lastSearchText == _searchText) return;
                    await DoNewSearch();
                }
            };

            _requestingSong = false;
            _requestName = Helpers.Settings.LastRequestName;
            var lastSongsRequested = Helpers.Settings.LastSongsRequested ?? new List<RequestedSong>();

            LastRequestedSongs = new ObservableCollection<RequestedSong>(lastSongsRequested);
            UpdateShowLastRequestedSongs();

            Comms.OnLoginTokenChanged += CommsOnOnLoginTokenChanged;

            Debug.WriteLine("HostViewModel.DoNewSearch making Commands");
            NextPageCommand = new Command(async () => await ChangePage(ChangePageDirection.Up));
            PreviousPageCommand = new Command(async () => await ChangePage(ChangePageDirection.Down));
            SongTappedCommand = new Command<Song>(song =>
            {
                RequestSongTitle = song.Title;
                RequestSongArtist = song.Artist;
                SelectedPageIndex = PageType.RequestSong;
            });
            RefreshList = new Command(async () =>
            {
                await RepeatSearch();
                MessagingCenter.Send(new ListViewRefreshEndedMessage(), "HostViewModel");
            });
            RequestSongCommand = new Command(async () => await DoRequestSong());
            HamburgerCommand = new Command(() => SelectedPageIndex = PageType.Settings);

            Debug.WriteLine("HostViewModel.DoNewSearch making GatheringSongs");
            GatheringSongs = false;

            Debug.WriteLine("HostViewModel.DoNewSearch making ShowPages");
            ShowPages = false;
            Debug.WriteLine("HostViewModel Constructor ended");
        }

        private void CommsOnOnLoginTokenChanged(object sender, EventArgs eventArgs)
        {
            UpdateShowLastRequestedSongs();
        }

        private int ChangePageUp()
        {
            var firstRow = _thisPage * _songsPerPage;
            ThisPage++;
            PreviousPageEnabled = true;
            NextPageEnabled = (_thisPage < _totalPages);
            return firstRow;
        }

        private int ChangePageDown()
        {
            var firstRow = (_thisPage - 2) * _songsPerPage;
            ThisPage--;
            NextPageEnabled = true;
            PreviousPageEnabled = (ThisPage > 1);
            return firstRow;
        }

        private async Task ChangePage(ChangePageDirection direction)
        {
            GatheringSongs = true;
            var firstRow = (direction == ChangePageDirection.Up ? ChangePageUp() : ChangePageDown());
            var songsResult = await Comms.SongList(App.Current.CurrentHostId, Helpers.Settings.ExactMatch, _lastSearchText, firstRow, Helpers.Settings.TitleFirst);
            ProcessSongs(songsResult);
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region Properties
        public SettingsViewModel Settings { get; } = new SettingsViewModel();
        public ICommand SongTappedCommand { get; set; }
        public ICommand RefreshList { get; set; }
        public ICommand RequestSongCommand { get; set; }
        public ICommand PreviousPageCommand { get; set; }
        public ICommand NextPageCommand { get; set; }
        public ICommand HamburgerCommand { get; set; }
        public string PageText => $"Page {ThisPage} of {TotalPages}";
//        public bool HasSongUpdates => App.Current.CurrentHostHasUpdates;

        public string MainAboutText => CustomAppData.Theme.MainAboutText;

        public Dictionary<int, string> RequestTypeList
        {
            get => _requestTypeList;
            set { _requestTypeList = value; OnPropertyChanged(); }
        }

        public int SelectedIndex
        {
            get => _selectedIndex;
            set
            {
                _selectedIndex = value; OnPropertyChanged();
                OnPropertyChanged(nameof(ShowDuet)); OnPropertyChanged(nameof(ShowGroup));
                OnPropertyChanged(nameof(ShowFirstNameLabel)); OnPropertyChanged(nameof(ShowYourNameLabel));
            }
        }

        public bool ShowDuet => _selectedIndex == 1;
        public bool ShowGroup => _selectedIndex == 2;
        public bool ShowFirstNameLabel => _selectedIndex == 1;
        public bool ShowYourNameLabel => _selectedIndex != 1;

        public bool RequestingSong
        {
            get => _requestingSong;
            set { _requestingSong = value; OnPropertyChanged(); OnPropertyChanged(nameof(CanEditSearchText)); }
        }

        public ObservableCollection<Song> Songs
        {
            get => _songs;
            set { _songs = value; OnPropertyChanged(); }
        }

        public ObservableCollection<Song> RecentSongs
        {
            get => _recentSongs;
            set { _recentSongs = value; OnPropertyChanged(); }
        }

        public PageType SelectedPageIndex
        {
            get => _selectedPageIndex;
            set
            {
                // Don't check for existing setting, as user can change the page
                //   and we don't propagate that change back to here.
                _selectedPageIndex = value;
                MessagingCenter.Send(new HostChangePageMessage { Page = value }, "");
            }
        }

        public bool RequestingDuet
        {
            get => _requestingDuet;
            set
            {
                if (value == _requestingDuet) return;
                _requestingDuet = value;
                OnPropertyChanged();
            }
        }

        public string RequestName
        {
            get => _requestName;
            set
            {
                if (value == _requestName) return;
                _requestName = value;
                OnPropertyChanged();
            }
        }

        public string RequestName2
        {
            get => _requestName2;
            set
            {
                if (value == _requestName2) return;
                _requestName2 = value;
                OnPropertyChanged();
            }
        }

        public string RequestSongTitle
        {
            get => _requestSongTitle;
            set
            {
                if (value == _requestSongTitle) return;
                _requestSongTitle = value;
                OnPropertyChanged();
            }
        }

        public string RequestSongArtist
        {
            get => _requestSongArtist;
            set
            {
                if (value == _requestSongArtist) return;
                _requestSongArtist = value;
                OnPropertyChanged();
            }
        }

        public string RequestTempoChange
        {
            get => _requestTempoChange;
            set
            {
                if (value == _requestTempoChange) return;
                _requestTempoChange = value;
                OnPropertyChanged();
            }
        }

        public string RequestKeyChange
        {
            get => _requestKeyChange;
            set
            {
                if (value == _requestKeyChange) return;
                _requestKeyChange = value;
                OnPropertyChanged();
            }
        }

        public bool GatheringSongs
        {
            get => _gatheringSongs;
            set
            {
                if (_gatheringSongs == value) return;
                _gatheringSongs = value;
                OnPropertyChanged(); OnPropertyChanged(nameof(CanEditSearchText));
            }
        }

        public bool CanEditSearchText => !_gatheringSongs && !_requestingSong;

        public int SongListCount
        {
            get => _songListCount;
            set { _songListCount = value; OnPropertyChanged(); }
        }

        public int ThisPage
        {
            get => _thisPage;
            set { _thisPage = value; OnPropertyChanged(); OnPropertyChanged(nameof(PageText)); }
        }

        public int TotalPages
        {
            get => _totalPages;
            set { _totalPages = value; OnPropertyChanged(); OnPropertyChanged(nameof(PageText)); }
        }

        public bool ShowPages
        {
            get => _showPages;
            set { _showPages = value; OnPropertyChanged(); OnPropertyChanged(nameof(PageText)); }
        }

        public string SearchText
        {
            get => _searchText;
            set
            {
                if (_searchText == value) return;
                _searchText = value; OnPropertyChanged();
                SearchTextChanged();
            }
        }

        public bool PreviousPageEnabled
        {
            get => _previousPageEnabled;
            set { _previousPageEnabled = value; OnPropertyChanged(); }
        }

        public bool NextPageEnabled
        {
            get => _nextPageEnabled;
            set { _nextPageEnabled = value; OnPropertyChanged(); }
        }

        public string RequestNameGroup
        {
            get => _requestNameGroup;
            set { _requestNameGroup = value; OnPropertyChanged(); }
        }

        public bool ShowAds
        {
            get => _showAds;
            set { _showAds = value; OnPropertyChanged(); }
        }

        public bool ShowLastRequestedSongs
        {
            get => _showLastRequestedSongs;
            set
            {
                _showLastRequestedSongs = value; OnPropertyChanged();
            }
        }

        public ObservableCollection<RequestedSong> LastRequestedSongs
        {
            get => _lastRequestedSongs;
            set
            {
                Debug.WriteLine($"HostViewModel.LastRequestedSongs set to have {value?.Count} songs");
                _lastRequestedSongs = value; OnPropertyChanged();
            }
        }

        #endregion

        private void SearchTextChanged()
        {
            if (_searchText.Length >= 3 || _searchText.Length == 0)
                _timer.ResetTimer();
        }

        public void Init()
        {
            Debug.WriteLine("HostViewModel.Init started");
            GatheringSongs = true;
            try
            {
                Task.Factory.StartNew(async () =>
                {
                    Debug.WriteLine("HostViewModel.Init task started");
                    try
                    {
                        var recentSongs = await Comms.RecentSongList(orderByTitle: Helpers.Settings.TitleFirst);
                        ProcessRecentSongs(recentSongs);

                        _lastSearchText = null;
                        var count = await Comms.SongListCount();
                        ProcessSongCountResult(count);

                        var result = await Comms.SongList();
                        if (result.IsSuccess == false)
                            throw new Exception("Could not get song list. Please try again later.");

                        ProcessSongs(result);
                        NextPageEnabled = true;
                        NextPageEnabled = ShowPages;
                    }
                    catch (Exception ee)
                    {
                        Debug.WriteLine($"HostViewModel.Init task had exception {ee}");
                        var m = ee.Message;
                        MessagingCenter.Send(new ErrorMessage { ErrorText = m }, "");
                        GatheringSongs = false;
                    }
                    Debug.WriteLine("HostViewModel.Init task ended");
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine($"HostViewModel.Init had exception {e}");
                MessagingCenter.Send(new ErrorMessage { ErrorText = e.Message }, "");
                GatheringSongs = false;
            }
            Debug.WriteLine("HostViewModel.Init ended");
        }

        private async Task DoRequestSong()
        {
            Debug.WriteLine("DoRequestSong Started");
            string text = "";
            string errorText = null, errorTitle = null;
            if (!IsRequestDataValid())
            {
                text = "Please fill in at least Your Name, Song Title and Song Artist";
                MessagingCenter.Send(new ErrorMessage { ErrorText = text }, "");
                Debug.WriteLine("DoRequestSong Ended - insufficient user entered data");
                return;
            }
            RequestingSong = true;
            var result = await Comms.RequestSong(_requestSongTitle, _requestSongArtist, App.Current.CurrentHostId, _requestName,
                _requestKeyChange, _requestTempoChange, (SelectedIndex == 1 ? _requestName2 : null),
                (SelectedIndex == 2 ? _requestNameGroup : null));
            var showAd = false;
            switch (result)
            {
                case Comms.RequestSongResult.BadRequest:
                    errorText = "Something went wrong.";
                    break;
                case Comms.RequestSongResult.HostNotFound:
                    errorText = "Something went wrong.";
                    break;
                case Comms.RequestSongResult.ServerError:
                    errorText = "Something went wrong.";
                    break;
                case Comms.RequestSongResult.ShowNotInProgress:
                    errorText = $"{App.Current.CurrentHostName}'s show has not started yet! Please try again later.";
                    break;
                case Comms.RequestSongResult.UnknownError:
                    errorText = "Something went wrong.";
                    break;
                case Comms.RequestSongResult.Success:
                    text = "Song requested!";
                    Helpers.Settings.LastRequestName = _requestName;
                    AddSongToLastRequested(_requestSongTitle, _requestSongArtist, _requestKeyChange, _requestTempoChange);
                    RequestSongTitle = "";
                    RequestSongArtist = "";
                    RequestKeyChange = "";
                    RequestTempoChange = "";
                    showAd = true;
                    break;
                case Comms.RequestSongResult.NoConnection:
                    errorText = "No internet connection available. Please try later";
                    break;
                case Comms.RequestSongResult.TooManyRequests:
                    errorTitle = "Whoah there, Diva!";
                    errorText = "You're going a little fast.\r\nGive some other folk a chance & try again in an hour!";
                    break;
            }
            RequestingSong = false;
            Debug.WriteLine("DoRequestSong ended " + (string.IsNullOrEmpty(errorText) ? " with OK" : " with error " + errorText));
            if (!string.IsNullOrEmpty(errorText))
                MessagingCenter.Send(new ErrorMessage { ErrorText = errorText, ErrorTitle = errorTitle }, "");
            else
                MessagingCenter.Send(new DisplayAlertMessage { Text = "", Header = text, ShowAd = showAd }, "");
        }

        private void AddSongToLastRequested(string requestSongTitle, string requestSongArtist, string requestKeyChange, string requestTempoChange)
        {
            var currentList = Helpers.Settings.LastSongsRequested?.ToArray() ?? new RequestedSong[0];
            var requestedSong = new RequestedSong
            {
                KeyChange = requestKeyChange,
                TimeRequested = DateTime.Now,
                Song = new DataModel.Song { Artist = requestSongArtist, Title = requestSongTitle },
                TempoChange = requestTempoChange
            };
            Helpers.Settings.LastSongsRequested = currentList.TakeLast(49).Concat(new[] { requestedSong }).OrderByDescending(o => o.TimeRequested).ToList();
            LastRequestedSongs = new ObservableCollection<RequestedSong>(Helpers.Settings.LastSongsRequested);
        }

        private bool IsRequestDataValid()
        {
            if (string.IsNullOrEmpty(_requestName)) return false;
            if (string.IsNullOrEmpty(_requestSongTitle)) return false;
            if (string.IsNullOrEmpty(_requestSongArtist)) return false;
            return true;
        }

        private async Task RepeatSearch()
        {
            GatheringSongs = true;
            var firstRow = (_thisPage - 1) * _songsPerPage;
            if (firstRow < 0) firstRow = 0;
            var songsResult = await Comms.SongList(App.Current.CurrentHostId, Helpers.Settings.ExactMatch, _lastSearchText, firstRow, Helpers.Settings.TitleFirst);
            ProcessSongs(songsResult);
        }

        private async Task DoNewSearch()
        {
            Debug.WriteLine("HostViewModel.DoNewSearch started");
            try
            {
                GatheringSongs = true;
                _lastSearchText = _searchText;
                var countResult = await Comms.SongList(App.Current.CurrentHostId, Helpers.Settings.ExactMatch, _searchText, 0,
                            Helpers.Settings.TitleFirst, true);
                ProcessSongCountResult(countResult);
                PreviousPageEnabled = false;
                NextPageEnabled = ShowPages;
                var songsResult = await Comms.SongList(App.Current.CurrentHostId, Helpers.Settings.ExactMatch, _searchText, 0,
                            Helpers.Settings.TitleFirst, false);
                ProcessSongs(songsResult);
            }
            catch (Exception ee)
            {
                Debug.WriteLine($"HostViewModel.DoNewSearch had exception {ee}");
                var m = ee.Message;
                MessagingCenter.Send(new ErrorMessage { ErrorText = m }, "");
                GatheringSongs = false;
            }
            finally
            {
                Debug.WriteLine("HostViewModel.DoNewSearch ended");
            }
        }

        private void ProcessRecentSongs(Comms.CommsResult result)
        {
            List<Song> songs;
            try
            {
                songs = result.Deserialise<List<Song>>();
            }
            catch (Exception)
            {
                Debug.WriteLine("HostViewModel.ProcessSongs failed for some reason");
                GatheringSongs = false;
                return;
            }

            Device.BeginInvokeOnMainThread(() =>
            {
                RecentSongs.Clear();
                foreach (var song in songs)
                {
                    RecentSongs.Add(song);
                }
            });
        }

        private void ProcessSongs(Comms.CommsResult result)
        {
            Debug.WriteLine("HostViewModel.ProcessSongs started");
            List<Song> songs;
            try
            {
                songs = result.Deserialise<List<Song>>();
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"HostViewModel.ProcessSongs failed for some reason {ex.Message}");
                GatheringSongs = false;
                return;
            }

            Device.BeginInvokeOnMainThread(() =>
            {
                Debug.WriteLine("HostViewModel.ProcessSongs thread started");
                Song currentSong = null;
                int songsProcessed = 0;
                try
                {
                    Songs.Clear();
                    foreach (var song in songs)
                    {
                        currentSong = song;
                        Songs.Add(song);
                        songsProcessed++;
                    }
                }
                catch (Exception e)
                {
                    var exText = $"Exception with song num {songsProcessed} {currentSong?.Title} by {currentSong?.Artist}: {e.Message}";
                    Debug.WriteLine($"HostViewModel.ProcessSongs thread had exception {exText}");
                    MessagingCenter.Send(new ErrorMessage { ErrorText = exText }, "");
                }
                GatheringSongs = false;
                Debug.WriteLine("HostViewModel.ProcessSongs thread ended");
            });
            Debug.WriteLine("HostViewModel.ProcessSongs ended");
        }

        private void ProcessSongCountResult(Comms.CommsResult count)
        {
            Debug.WriteLine("HostViewModel.ProcessSongCountResult started");
            if (count.IsSuccess == false)
            {
                GatheringSongs = false;
                throw new Exception($"Unable to get song list. Please try again later. [{count.StatusCode}]");
            }
            var bits = count.RawResponse.Split(' ');
            if (bits.Length != 2)
            {
                GatheringSongs = false;
                throw new Exception($"Failed to get song list. Please try again later: {count.RawResponse}");
            }
            SongListCount = int.Parse(bits[0]);
            _songsPerPage = int.Parse(bits[1]);
            if (SongListCount > _songsPerPage)
            {
                ShowPages = true;
                TotalPages = SongListCount / _songsPerPage;
                if (_songsPerPage * TotalPages < SongListCount)
                    TotalPages++;
                ThisPage = 1;
            }
            else
            {
                ShowPages = false;
            }
            Debug.WriteLine("HostViewModel.ProcessSongCountResult ended");
        }

        public void DeInit()
        {
            _timer?.Dispose();
            Comms.OnLoginTokenChanged -= CommsOnOnLoginTokenChanged;
        }

        public void LastRequestedSongTapped(RequestedSong requestedSong)
        {
            RequestSongTitle = requestedSong.Song.Title;
            RequestSongArtist = requestedSong.Song.Artist;
            RequestKeyChange = requestedSong.KeyChange;
            RequestTempoChange = requestedSong.TempoChange;
            SelectedPageIndex = PageType.RequestSong;
        }
    }

    public enum PageType
    {
        RecentSongs,
        SongList,
        Settings,
        RequestSong,
    }

    public class Song
    {
        public string Title { get; set; }
        public string Artist { get; set; }
    }
}
