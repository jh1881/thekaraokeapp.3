﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using DJWayniesApp.DataModel;
using Xamarin.Forms;

namespace DJWayniesApp.ViewModels
{
    public class BaseSplashPageViewModel : INotifyPropertyChanged
    {
        private string _username;
        private string _password;
        private bool _isHostLoggingIn;
        private bool _isWorking;

        public bool IsWorking
        {
            get => _isWorking;
            set { _isWorking = value; OnPropertyChanged(); }
        }

        public bool IsHostLoggingIn
        {
            get => _isHostLoggingIn;
            set { _isHostLoggingIn = value; OnPropertyChanged(); }
        }

        public string Password
        {
            get => _password;
            set { _password = value; OnPropertyChanged(); }
        }

        public string Username
        {
            get => _username;
            set { _username = value; OnPropertyChanged(); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public BaseSplashPageViewModel()
        {
            CheckExistingLoginToken();
        }

        public void CheckExistingLoginToken()
        {
            if (!string.IsNullOrEmpty(Helpers.Settings.JWT))
            {
                IsWorking = true;
                Comms.LoginToken = Helpers.Settings.JWT;
                Task.Factory.StartNew(async () =>
                {
                    var response = await Comms.VerifyHost();
                    if (response.IsSuccess)
                        await App.Current.SuccessfulLoginAction(Helpers.Settings.JWT);
                    else
                    {
                        Comms.LoginToken = null;
                        Helpers.Settings.JWT = null;
                        IsWorking = false;
                    }
                });
            }
        }

        public ICommand StartCommand
        {
            get { return new Command(async () => await App.Current.SuccessfulLoginAction(null)); }
        }

        public ICommand CancelHostLoginCommand
        {
            get { return new Command(() => IsHostLoggingIn = false); }
        }

        public ICommand HostLoginCommand
        {
            get { return new Command(() => IsHostLoggingIn = true); }
        }

        public ICommand DoHostLoginCommand
        {
            get
            {
                return new Command(async () =>
                {
                  IsWorking = true;
                  try
                  {
                      var result = await Comms.LoginHost(Username, Password);
                      var jwt = result.Deserialise<HostLoginResponse>();
                      await App.Current.SuccessfulLoginAction(jwt.jwt);
                  }
                  finally
                  {
                      IsWorking = false;
                  }
                });
            }
        }
    }
}
