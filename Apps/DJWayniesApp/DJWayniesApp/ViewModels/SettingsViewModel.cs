﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace DJWayniesApp.ViewModels
{
    public class SettingsViewModel : INotifyPropertyChanged
    {
        private bool _exactSearchMatch;
        private bool _orderByTitleFirst;
        public event PropertyChangedEventHandler PropertyChanged;

        public SettingsViewModel()
        {
            _exactSearchMatch = Helpers.Settings.ExactMatch;
            _orderByTitleFirst = Helpers.Settings.TitleFirst;
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public bool ExactSearchMatch
        {
            get => _exactSearchMatch;
            set
            {
                _exactSearchMatch = value;
                OnPropertyChanged();
                Helpers.Settings.ExactMatch = value;
            }
        }

        public bool OrderByTitleFirst
        {
            get => _orderByTitleFirst;
            set
            {
                _orderByTitleFirst = value;
                OnPropertyChanged();
                Helpers.Settings.TitleFirst = value;
            }
        }
    }
}