﻿using System.Diagnostics;
using DJWayniesApp.DataModel;
using Xamarin.Forms;
using DJWayniesApp.ViewModels;
using DJWayniesApp.Messages;
using Xamarin.Forms.Xaml;
using Song = DJWayniesApp.ViewModels.Song;

namespace DJWayniesApp.Views
{
    public partial class HostPage
    {
        private HostViewModel _viewModel;
        public HostPage()
        {
            Debug.WriteLine($"HostPage Constructor started");
            InitializeComponent();
            Debug.WriteLine($"HostPage Constructor ended");
        }

        protected override void OnAppearing()
        {
            Debug.WriteLine($"HostPage.OnAppearing started");
            base.OnAppearing();
            SetValue(NavigationPage.BarBackgroundColorProperty, Color.Black);
            SetValue(NavigationPage.BarTextColorProperty, Color.White);

            var parent = this.Parent;
            if (parent is App)
            {
                TopRow.Height = 20;
                TopRow2.Height = 20;
            }
            MessagingCenter.Subscribe<HostChangePageMessage>(this, "", PageChanged);
            MessagingCenter.Subscribe<ListViewRefreshEndedMessage>(this, "HostViewModel", message => SongListView.EndRefresh());
            _viewModel = BindingContext as HostViewModel;
            _viewModel?.Init();
            Debug.WriteLine($"HostPage.OnAppearing completed");
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            _viewModel?.DeInit();

            MessagingCenter.Unsubscribe<HostChangePageMessage>(this, "HostViewModel");
            MessagingCenter.Unsubscribe<ListViewRefreshEndedMessage>(this, "");
        }

        private void PageChanged(HostChangePageMessage hostChangePageMessage)
        {
            switch (hostChangePageMessage.Page)
            {
                case PageType.RequestSong:
                    CurrentPage = RequestSongPage;
                    break;
                case PageType.SongList:
                    CurrentPage = SongListPage;
                    break;
                case PageType.Settings:
                    CurrentPage = SettingsPage;
                    break;
                case PageType.RecentSongs:
                    CurrentPage = RecentSongPage;
                    break;
            }
        }

        private void ListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item is Song song)
                _viewModel.SongTappedCommand.Execute(song);
            ((ListView) sender).SelectedItem = null;
        }

        private void LastRequestedSongsListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            // Transfer the data into the various bits
            if(e.Item is RequestedSong item)
                _viewModel?.LastRequestedSongTapped(item);
            ((ListView)sender).SelectedItem = null;
        }
    }
}
