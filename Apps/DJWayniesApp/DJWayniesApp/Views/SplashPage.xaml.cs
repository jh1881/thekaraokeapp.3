﻿using Xamarin.Forms.Xaml;

namespace DJWayniesApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SplashPage : BaseSplashPage
    {
        public SplashPage()
        {
            InitializeComponent();
        }
    }
}