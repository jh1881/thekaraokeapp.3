﻿using DJWayniesApp.Messages;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DJWayniesApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HostControlPage : ContentPage
    {
        public HostControlPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            MessagingCenter.Subscribe<SwitchToSingerViewMessage>(this, "", async msg => await Navigation.PushAsync(new HostPage()));
            MessagingCenter.Subscribe<ListViewRefreshEndedMessage>(this, "HostControlViewModel", message => RequestsList.EndRefresh());
            App.Current.ApplyControlHostId();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            MessagingCenter.Unsubscribe<SwitchToSingerViewMessage>(this, "");
            MessagingCenter.Unsubscribe<ListViewRefreshEndedMessage>(this, "HostControlViewModel");
            App.Current.RestoreOriginalHostIdIfNeeded();
        }

        private void RequestsList_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e == null) return;
            ((ListView) sender).SelectedItem = null;
        }
    }
}