﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ZXing;

namespace DJWayniesApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChooseHostPage : ContentPage
    {
        public ChooseHostPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            //if (App.Current.UseFacebookLogin && !App.Current.IsLoggedIn)
            //{
            //    //                Navigation.PushModalAsync(new LoginPage());
            //}
        }

        private async Task<string> ScanQR()
        {
            var options = new ZXing.Mobile.MobileBarcodeScanningOptions
                {
                    PossibleFormats = new List<BarcodeFormat> {BarcodeFormat.QR_CODE}
                };
            var scanner = new ZXing.Mobile.MobileBarcodeScanner();
            var result = await scanner.Scan(options);
            return result?.Text;
        }    
    }
}
