﻿using DJWayniesApp.ViewModels;
using Xamarin.Forms;

namespace DJWayniesApp.Views
{
    public class BaseSplashPage : ContentPage
    {
        protected override void OnAppearing()
        {
            base.OnAppearing();
            var vm = new BaseSplashPageViewModel();
            BindingContext = vm;
        }
    }
}
