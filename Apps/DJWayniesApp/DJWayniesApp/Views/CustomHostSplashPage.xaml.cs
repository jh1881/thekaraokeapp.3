﻿using Xamarin.Forms.Xaml;

namespace DJWayniesApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CustomHostSplashPage : BaseSplashPage
    {
        public CustomHostSplashPage()
        {
            InitializeComponent();
        }
    }
}
