﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DJWayniesApp.Extensions
{
    public class OnPlatformExtension : IMarkupExtension
    {
        public object Ios { get; set; }
        public object Android { get; set; }
        public object WinPhone { get; set; }

        public object ProvideValue(IServiceProvider serviceProvider)
        {
            switch (Device.RuntimePlatform)
            {
                case Device.Android: return Android;
                case Device.iOS: return Ios;
                case Device.UWP:
                case Device.WinPhone:
                case Device.WinRT: return WinPhone;
            }
            return null;
        }
    }
}
