﻿using System.Threading.Tasks;
using Xamarin.Forms;

namespace DJWayniesApp.Behaviours
{
    public class FadeBehaviour : BindableBehaviour<VisualElement>
    {
        public FadeBehaviour()
        {
            FadeInAnimationLength = 250;
            FadeOutAnimationLength = 350;
        }
        public static readonly BindableProperty IsSelectedProperty = BindableProperty.Create("IsSelected", typeof(bool), typeof(FadeBehaviour), false, BindingMode.Default, propertyChanged: IsSelectedChanged);

        public bool IsSelected
        {
            get => (bool)GetValue(IsSelectedProperty);
            set => SetValue(IsSelectedProperty, value);
        }

        public uint FadeInAnimationLength { get; set; }

        public uint FadeOutAnimationLength { get; set; }

        protected override void OnAttachedTo(VisualElement visualElement)
        {
            base.OnAttachedTo(visualElement);
            visualElement.Opacity = 0;
            visualElement.IsVisible = false;
        }

        private static void IsSelectedChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var behavior = bindable as FadeBehaviour;
            if (behavior?.AssociatedObject == null) return;
            behavior.Animate();
        }

        private void Animate()
        {
            if (IsSelected) AssociatedObject.IsVisible = true;
            AssociatedObject.FadeTo(
                IsSelected ? 1 : 0,
                IsSelected ? FadeInAnimationLength : FadeOutAnimationLength,
                Easing.Linear).ContinueWith(x =>
                    {
                        if (!IsSelected)
                            AssociatedObject.IsVisible = false;
                    }, TaskScheduler.FromCurrentSynchronizationContext());
        }
    }
}
