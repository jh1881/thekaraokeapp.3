﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DJWayniesApp.Alerts
{
    public class PageProxy : IPage
    {
        private readonly Func<Page> _pageResolver;

        public PageProxy(Func<Page> pageResolver)
        {
            _pageResolver = pageResolver;
        }

        public async Task DisplayAlert(string title, string message, string cancel)
        {
            var sem = new SemaphoreSlim(1, 1);
            Device.BeginInvokeOnMainThread(async () =>
            {
                await _pageResolver() .DisplayAlert(title, message, cancel);
                sem.Release();
            });
            await sem.WaitAsync();
        }

        public async Task<bool> DisplayAlert(string title, string message, string accept, string cancel)
        {
            bool result = false;
            var sem = new SemaphoreSlim(1, 1);
            Device.BeginInvokeOnMainThread(async () =>
            {
                result = await _pageResolver().DisplayAlert(title, message, accept, cancel);
                sem.Release();
            });
            await sem.WaitAsync();
            return result;
        }

        public async Task<string> DisplayActionSheet(string title, string cancel, string destruction, params string[] buttons)
        {
            string result = null;
            var sem = new SemaphoreSlim(1, 1);
            Device.BeginInvokeOnMainThread(async () =>
            {
                result = await _pageResolver().DisplayActionSheet(title, cancel, destruction, buttons);
                sem.Release();
            });
            await sem.WaitAsync();
            return result;
        }

        public INavigation Navigation => _pageResolver().Navigation;
    }
}
