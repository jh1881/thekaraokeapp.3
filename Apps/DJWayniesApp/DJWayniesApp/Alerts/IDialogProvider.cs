﻿using System.Threading.Tasks;
using Xamarin.Forms;

namespace DJWayniesApp.Alerts
{
    public interface IDialogProvider
    {
        Task DisplayAlert(string title, string message, string cancel);
        Task<bool> DisplayAlert(string title, string message, string accept, string cancel);
        Task<string> DisplayActionSheet(string title, string cancel, string destruction, params string[] buttons);
    }

    public interface IPage : IDialogProvider
    {
        INavigation Navigation { get; }
    }
}
