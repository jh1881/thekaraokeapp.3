﻿using Acr.UserDialogs;
using Autofac;
using DJWayniesApp.Alerts;
using DJWayniesApp.DataModel;
using DJWayniesApp.Interfaces;
using DJWayniesApp.Messages;
using DJWayniesApp.ViewModels;
using DJWayniesApp.Views;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;
using DJWayniesApp.Helpers;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using TheKaraokeApp.Core.Interfaces;
using Xamarin.Forms;
using Device = Xamarin.Forms.Device;

namespace DJWayniesApp
{
    public partial class App
    {
        public new static App Current => Application.Current as App;
        public static IAnalytics Analytics => Current?._analytics;

        private NavigationPage _navigationPage;
        private Task _preloadTask;
        private string _originalHostId;
        private string _controlHostId;
        public string CurrentHostName { get; set; }
        public string CurrentHostId { get; set; }
//        public bool CurrentHostHasUpdates { get; set; }
        public bool ShowAds { get; set; }
        public bool IsLoggedIn => !string.IsNullOrEmpty(Comms.LoginToken);
        public IContainer Container { get; }
        private bool _backgrounding;
        private readonly IAnalytics _analytics;

        public event EventHandler OnStartBackgrounding;
        public event EventHandler OnStopBackgrounding;

        public const string InsightsAnalyticsAppId = "a6c3ca0009988e894d736e82ccd0d5701af6c8ef";
        public const string HockeyAppAnalyticsAppId = "ace5c1690cdd48cd9eac107f77264af8";
        public const string GoogleAdMobsUnitId = "ca-app-pub-9610560063312392/2748224265";

        public App()
        {
            _analytics = DependencyService.Get<IAnalytics>();
            _analytics?.Init(InsightsAnalyticsAppId);
            _analytics?.Track("App.Constructor");

            InitializeComponent();
            ShowAds = true;
            var builder = new ContainerBuilder();

            builder.RegisterInstance<Func<Page>>(() =>
            {
                if (MainPage == _navigationPage) return _navigationPage.CurrentPage;
                if (MainPage is MasterDetailPage page) return page.Detail;
                if (MainPage is IPageContainer<Page> container) return container.CurrentPage;

                return MainPage;
            });
            CoreInit.Init(builder);
            if (_analytics != null)
                builder.RegisterInstance(_analytics);
            builder.RegisterInstance(UserDialogs.Instance);
            Container = builder.Build();

            MessagingCenter.Subscribe<ErrorMessage>(this, "", async msg => await ErrorMessageReceived(msg));
            MessagingCenter.Subscribe<DisplayAlertMessage>(this, "", async msg => await DisplayAlertReceived(msg));

            MakeMainPage();
        }

        protected override void OnStart()
        {
            base.OnStart();

            AppCenter.Start("android=ff28dbc9-c70f-49bb-aada-c3dfca8e5229;ios=ace5c169-0cdd-48cd-9eac-107f77264af8", typeof(Analytics), typeof(Crashes));
        }

        private void MakeMainPage(bool includePreloadTask = true)
        {
            try
            {
                Debug.WriteLine("App.MakeMainPage started");
                //Analytics?.Track("App.MakeMainPage", "includePreloadTask", "true");
                //Analytics?.Track("App.MakeMainPage", "HostNumber", CustomAppData.Host.HostNumber);
                if (!string.IsNullOrEmpty(CustomAppData.Host.HostNumber))
                {
                    MainPage = new CustomHostSplashPage();
                    if (includePreloadTask)
                    {
                        _preloadTask = Task.Run(async () =>
                        {
                            if (!Settings.HasSeenAdsMessage)
                            {
                                Settings.NonPersonalisedAds = !await UserDialogs.Instance.ConfirmAsync(
                                    "Can we continue to use your data to tailor ads for you?",
                                    "Privacy", "Yes, continue to see relevant ads",
                                    "No, see ads that are less relevant");
                                Settings.HasSeenAdsMessage = true;
                            }

                            Debug.WriteLine("App.MakeMainPage preloadTask started");
                            //Analytics?.Track("App.MakeMainPage.PreloadTask");
                            var result = await Comms.ChooseHost(CustomAppData.Host.HostNumber);
                            //Analytics?.Track("App.MakeMainPage.PreloadTask", "ChooseHost", $"{result.IsSuccess}");
                            if (result.IsSuccess)
                            {
                                // We need to check here that this is good. Otherwise, we can't continue.
                                try
                                {
                                    Debug.WriteLine(result.RawResponse);
                                    //Analytics?.Track("App.MakeMainPage.PreloadTask", "result", $"{result.RawResponse}");
                                    var host = result.Deserialise<Host>();
                                    CurrentHostId = host.Id;
                                    CurrentHostName = host.Name;

//                                    CurrentHostHasUpdates = false;
                                    //// Update the song updates dictionary, and keep a data point for the ui
                                    //var dict = Settings.HostSongUpdates;
                                    //if (host.LastSongsUpdate.HasValue)
                                    //{
                                    //    if (dict.ContainsKey(host.Id))
                                    //    {
                                    //        if (host.LastSongsUpdate > dict[host.Id])
                                    //            CurrentHostHasUpdates = true;
                                    //        dict[host.Id] = host.LastSongsUpdate.Value;
                                    //    }
                                    //    else
                                    //    {
                                    //        CurrentHostHasUpdates = true;

                                    //        dict.Add(host.Id, host.LastSongsUpdate.Value);
                                    //    }
                                    //}
                                    //Settings.HostSongUpdates = dict;
                                }
                                catch (JsonReaderException jre)
                                {
                                    // It was bad. Let the user know.
                                    Error("Failed to contact host server. Please check your internet connection and try again later.", jre);
                                }
                            }
                            Debug.WriteLine("App.MakeMainPage preloadTask ended");
                        });
                    }
                }
                else
                {
                    MainPage = new SplashPage();
                }
            }
            catch (Exception ee)
            {
                Analytics?.ReportException(ee, AnalyticsSeverity.Error);
                throw;
            }
            Debug.WriteLine("App.MakeMainPage ended");
        }

        public async Task<int> ProcessBackgrounding()
        {
            Analytics?.Track("ProcessBackgrounding");
            return await SongRequestStorage.Update();
            //            if (numberNewSongRequests > 0)
            //            {
            //                return BackgroundResult.NewData;
            //            }
            //            else if (numberNewSongRequests < 0)
            //            {
            //                return BackgroundResult.Failed;
            //            }
            //            return BackgroundResult.NoNewData;
        }

        public static Task BeginInvokeOnMainThreadAsync(Action action)
        {
            Debug.WriteLine($"App.BeginInvokeOnMainThreadAsync started");
            var tcs = new TaskCompletionSource<object>();
            Device.BeginInvokeOnMainThread(() =>
            {
                try
                {
                    action();
                    Debug.WriteLine($"App.BeginInvokeOnMainThreadAsync action completed without error");
                    tcs.SetResult(null);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine($"App.BeginInvokeOnMainThreadAsync action completed with exception {ex}");
                    tcs.SetException(ex);
                }
            });
            return tcs.Task;
        }

        public void LogoutAction()
        {
            Analytics?.Track("LogoutAction");
            Settings.JWT = null;
            Comms.LoginToken = null;
            MakeMainPage(false);
            DependencyService.Get<ILoginOutService>()?.LoggedOut();
        }

        public void LoggedOut()
        {
            Analytics?.Track("LoggedOut");
            LogoutAction();
        }

        public void TokenLoggedIn(string token)
        {
            Analytics?.Track("TokenLoggedIn");
            Task.Factory.StartNew(async () => await SuccessfulLoginAction(token));
        }

        public void RestoreOriginalHostIdIfNeeded()
        {
            if (string.IsNullOrEmpty(_originalHostId) == false)
                CurrentHostId = _originalHostId;
        }

        public void ApplyControlHostId()
        {
            if (!string.IsNullOrEmpty(_controlHostId))
                CurrentHostId = _controlHostId;
        }

        private int _requestCount = 0;
        private readonly Random _random = new Random();

        private async Task DisplayAlertReceived(DisplayAlertMessage msg)
        {
            using (var scope = Container.BeginLifetimeScope())
            {
                var dlgProvider = Container.Resolve<IDialogProvider>();
                await dlgProvider.DisplayAlert(msg.Header, msg.Text, "OK");
            }

            if (msg.ShowAd)
            {
                _requestCount++;
                if (_requestCount == 1 || _random.Next(100) > 75)
                {
                    var adService = DependencyService.Get<IAdService>();
                    adService.ShowInterstitialAd();
                }
            }
        }

        private async Task ErrorMessageReceived(ErrorMessage obj)
        {
            await UserDialogs.Instance.AlertAsync(obj.ErrorText, obj.ErrorTitle ?? "App Error");
            //using (Container.BeginLifetimeScope())
            //{
            //    var dlgProvider = Container.Resolve<IDialogProvider>();
            //    await dlgProvider.DisplayAlert("App Error", obj.ErrorText, "OK");
            //}
        }

        private void SetNonHostMainpage()
        {
            Debug.WriteLine("App.SetNonHostMainPage started");
            Analytics?.Track("App.SetNonHostMainPage");
            Device.BeginInvokeOnMainThread(() =>
            {
                if (string.IsNullOrEmpty(CustomAppData.Host.HostNumber))
                {
                    // This is the generic app, so we present the choose host page.
                    // Since the user can go to a different host, we include a navigation
                    //   to allow them to come back to the choose host page later if they
                    //   wish.
                    MainPage = new NavigationPage(new ChooseHostPage());
                }
                else
                {
                    // This is a specific hosts app. We don't allow switching hosts
                    //   with this, so no navigation and just straight into the host page.
                    MainPage = new HostPage();
                }
            });
            Debug.WriteLine($"App.SetNonHostMainPage ended");
        }

        public class AmIAHostResult
        {
            public string Id { get; set; }
            public string notif { get; set; }
        }

        public async Task SuccessfulLoginAction(string token)
        {
            Debug.WriteLine($"App.SuccessfulLoginAction started");
            Analytics?.Track("App.SuccessfulLoginAction", "token", token);
            var splash = MainPage as BaseSplashPage;
            var dataContext = splash?.BindingContext as BaseSplashPageViewModel;
            if (dataContext != null) dataContext.IsWorking = true;

            // Blurgh!!!! This is to stop some vast weirdness in xamarin forms.
            //  this workaround is brought to you by the gurus at Xamarin, who told me to do it :)
            await Task.Delay(500);
            Debug.WriteLine($"App.SuccessfulLoginAction waiting for preloadTask");
            _preloadTask?.Wait();
            Debug.WriteLine($"App.SuccessfulLoginAction waiting for preloadTask done");

            Comms.LoginToken = token;
            Settings.JWT = token;
            Debug.WriteLine($"App.SuccessfulLoginAction token = {token}");
            if (!string.IsNullOrEmpty(token))
            {
                var result = await Comms.AmIaHost();
                Debug.WriteLine($"App.SuccessfulLoginAction called AmIaHost and got {result}");
                Analytics?.Track("App.SuccessfulLoginAction", "AmIAHost.result", $"{result}");
                if (result.IsSuccess && result.StatusCode == HttpStatusCode.OK)
                {
                    do
                    {
                        AmIAHostResult realResult = null;
                        try
                        {
                            realResult = result.Deserialise<AmIAHostResult>();
                        }
                        catch (JsonReaderException jre)
                        {
                            if (int.TryParse(result.RawResponse, out var x))
                            {
                                realResult = new AmIAHostResult { Id = x.ToString() };
                            }
                            else
                            {
                                // Bad things. Most probably a connection issue such as a wifi requiring registration.
                                Error(
                                    "Failed to contact host server. Please check your internet connection and try again later.",
                                    jre);
                                break;
                            }
                        }
                        _originalHostId = CurrentHostId;
                        _controlHostId = CurrentHostId = realResult.Id;
                        if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.LocationWhenInUse))
                        {
                            await UserDialogs.Instance.AlertAsync("We need location permission to improve your song request information");
                        }
                        await CrossPermissions.Current.RequestPermissionsAsync(Permission.LocationWhenInUse);
                        try
                        {
                            await SongRequestStorage.Init();
                        }
                        catch (Exception ee)
                        {

                        }

                        try
                        {
                            Analytics?.Track("App.SuccessfulLoginAction.Push");
                            Debug.WriteLine("Starting Push Notifications");
                            var pushNotif = DependencyService.Get<IPushNotifications>();
                            pushNotif?.Begin();
                        }
                        catch (Exception ee)
                        {
                            Error($"Failed to start push notifications: {ee}", ee);
                        }
                        // The host can navigate up to a 'normal' session
                        Debug.WriteLine("Switching to HostControlPage");
                        Device.BeginInvokeOnMainThread(() => MainPage = _navigationPage = new NavigationPage(new HostControlPage()));
                    } while (false);
                }
                else
                {
                    SetNonHostMainpage();
                }
            }
            else
            {
                if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.LocationWhenInUse))
                {
                    await UserDialogs.Instance.AlertAsync("We need your location to show you nearby karaoke events, and to improve song requesting.");
                }
                await CrossPermissions.Current.RequestPermissionsAsync(Permission.LocationWhenInUse);

                SetNonHostMainpage();
            }
            if (dataContext != null) dataContext.IsWorking = false;
            DependencyService.Get<ILoginOutService>()?.LoggedIn(token);
            Debug.WriteLine($"App.SuccessfulLoginAction ended");
        }

        public void Error(string text, Exception exception = null)
        {
            if (exception != null)
                Analytics?.ReportException(exception, AnalyticsSeverity.Error);
            Debug.WriteLine($"App.Error {text}");
            MessagingCenter.Send(new ErrorMessage { ErrorText = text }, "");
        }

        public void StartBackgrounding()
        {
            Analytics?.Track("StartBackgrounding", "_backgrounding", $"{_backgrounding}");
            if (_backgrounding)
                return;
            _backgrounding = true;
            OnStartBackgrounding?.Invoke(this, new EventArgs());
        }

        public void StopBackgrounding()
        {
            Analytics?.Track("StopBackgrounding", "_backgrounding", $"{_backgrounding}");
            if (!_backgrounding)
                return;
            _backgrounding = false;
            OnStopBackgrounding?.Invoke(this, new EventArgs());
        }
    }

    public enum BackgroundResult
    {
        NewData,
        NoNewData,
        Failed
    }
}
