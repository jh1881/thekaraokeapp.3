﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DJWayniesApp
{
    public static class ArrayExtensions
    {
        public static IEnumerable<T> TakeLast<T>(this T[] enumerable, int number)
        {
            return enumerable.Skip(Math.Max(0, enumerable.Length - number));
        }
    }
}
