﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DJWayniesApp.Interfaces;
using Microsoft.AppCenter.Analytics;

namespace DJWayniesApp.Services
{
    class AppCenterAnalytics : IAnalytics
    {
        public void Init(string appId)
        {
        }

        public void Identify(string uid, IDictionary<string, string> table = null)
        {
        }

        public void Identify(string uid, string key, string value)
        {
        }

        public void Track(string trackIdentifier, string key, string value)
        {
            Analytics.TrackEvent(trackIdentifier, new Dictionary<string, string>{{key, value}});
        }

        public void Track(string trackIdentifier, IDictionary<string, string> table = null)
        {
            Analytics.TrackEvent(trackIdentifier, table);
        }

        public void ReportException(Exception e, AnalyticsSeverity severity)
        {
        }

        public void ReportException(Exception e, IDictionary data = null)
        {
        }

        public void ReportException(Exception e, string key, string value)
        {
        }
    }
}
