using System;
using Plugin.Settings.Abstractions;
using Plugin.Settings;
using DJWayniesApp.DataModel;
using System.Collections.Generic;
using System.Diagnostics;
using Newtonsoft.Json;

namespace DJWayniesApp.Helpers
{
    public static class Settings
    {
        private static ISettings AppSettings => CrossSettings.Current;

        public static Guid Id
        {
            get
            {
                var id = GetAndLog("GuidId", default(Guid));
                if (id == default(Guid))
                {
                    SetAndLog("GuidId", Guid.NewGuid());
                    id = GetAndLog("GuidId", default(Guid));
                }
                return id;
            }
        }

        public static bool NonPersonalisedAds
        {
            get => GetAndLog("NotPersonalisedAds", false);
            set => SetAndLog("NotPersonalisedAds", value);
        }

        public static bool HasSeenAdsMessage
        {
            get => GetAndLog("HasSeenAdsMessage", false);
            set => SetAndLog("HasSeenAdsMessage", value);
        }

        public static Dictionary<string, DateTime> HostSongUpdates
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<Dictionary<string, DateTime>>(GetAndLog("HostSongUpdates", ""));
                }
                catch
                {
                    return new Dictionary<string, DateTime>();
                }
            }
            set { SetAndLog("HostSongUpdates", JsonConvert.SerializeObject(value)); }
        }

        public static bool ExactMatch
        {
            get { return GetAndLog("ExactMatch", false); }
            set { SetAndLog("ExactMatch", value); }
        }

        public static bool TitleFirst
        {
            get { return GetAndLog("TitleFirst", false); }
            set { SetAndLog("TitleFirst", value); }
        }

        public static string JWT
        {
            get => GetAndLog("JWT", "");
            set => SetAndLog("JWT", value);
        }

        public static List<SongRequest> CurrentSongRequests
        {
            get { return JsonConvert.DeserializeObject<List<SongRequest>>(GetAndLog("SongRequests", "")); }
            set { SetAndLog("SongRequests", JsonConvert.SerializeObject(value)); }
        }

        public static List<RequestedSong> LastSongsRequested
        {
            get { return JsonConvert.DeserializeObject<List<RequestedSong>>(GetAndLog("LastSongsRequested", "")); }
            set { SetAndLog("LastSongsRequested", JsonConvert.SerializeObject(value)); }
        }

        public static string LastRequestName
        {
            get { return GetAndLog("LastRequestName", ""); }
            set { SetAndLog("LastRequestName", value); }
        }

        private static void SetAndLog<T>(string name, T val)
        {
            Debug.WriteLine($"Settings.SetAndLog name={name}, val={val}");
            AppSettings.AddOrUpdateValue(name, val);
        }

        private static T GetAndLog<T>(string name, T defaultValue = default(T))
        {
            Debug.WriteLine($"Settings.GetAndLog name={name}");
            var val = AppSettings.GetValueOrDefault(name, defaultValue);
            Debug.WriteLine($"Settings.GetAndLog name={name}, val={val}");
            return val;
        }
    }
}
