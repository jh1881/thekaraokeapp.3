﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace DJWayniesApp
{
    public class KTimer : IDisposable
    {
        private Stopwatch _stopwatch;
        private readonly Task _task;
        private readonly ManualResetEvent _manualResetEvent = new ManualResetEvent(false);
        private bool _endTask;
        private bool _taskStopped = true;

        public int Delay { get; set; }

        public EventHandler OnTimerFired { get; set; }

        public void ResetTimer()
        {
            _stopwatch?.Stop();
            _stopwatch = Stopwatch.StartNew();
            _manualResetEvent.Set();
            _taskStopped = false;
        }

        public void Stop()
        {
            _taskStopped = true;
        }

        public KTimer()
        {
            _task = new Task(async () => await TaskProcess());
            _task.Start();
        }

        private async Task TaskProcess()
        {
            _taskStopped = false;
            while (!_endTask)
            {
                _manualResetEvent.WaitOne();
                _stopwatch?.Stop();
                _stopwatch = Stopwatch.StartNew();

                while (!_endTask && !_taskStopped)
                {
                    await Task.Delay(100);
                    if (_stopwatch.ElapsedMilliseconds >= Delay)
                    {
                        _manualResetEvent.Reset();
                        _stopwatch.Stop();
                        _stopwatch = null;
                        OnTimerFired?.Invoke(this, new EventArgs());
                        break;
                    }
                }
            }
        }


        public void Dispose()
        {
            _endTask = true;
            _manualResetEvent.Set();
            _task.Wait();

            _manualResetEvent.Dispose();
        }
    }
}
