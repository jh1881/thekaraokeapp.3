﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Newtonsoft.Json;

namespace DJWayniesApp.DataModel
{
    public class SongRequest : INotifyPropertyChanged
    {
        private bool _marked;
        private bool _cancelled;

        public int Id { get; set; }
        public string Title { get; set; }
        public string Artist { get; set; }
        public string KeyChange { get; set; }
        public string TempoChange { get; set; }
        public string Requestor { get; set; }
        public string DuetSecond { get; set; }
        public DateTime RequestedTime { get; set; }
        [JsonConverter(typeof(JsonBoolConverter))]
        public bool Notified { get; set; }
        public string GroupNames { get; set; }
        public int GpsResult { get; set; }

        public bool HasGoodGps => GpsResult == 1;

        public bool Cancelled
        {
            get => _cancelled;
            set { _cancelled = value; OnPropertyChanged(); }
        }

        public bool Marked
        {
            get => _marked;
            set
            {
                _marked = value; OnPropertyChanged();
                SongRequestStorage.SaveChanges();
            }
        }

        public bool DuetVisible => !string.IsNullOrEmpty(DuetSecond);
        public bool GroupVisible => !string.IsNullOrEmpty(GroupNames);
        public bool KeyChangeVisible => !string.IsNullOrEmpty(KeyChange);
        public bool TempoChangeVisible => !string.IsNullOrEmpty(TempoChange);
        public string ShortTime => RequestedTime.ToString("HH:mm");

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
