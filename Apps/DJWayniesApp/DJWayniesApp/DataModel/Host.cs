﻿using System;

namespace DJWayniesApp.DataModel
{
    public class Host
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public DateTime? LastSongsUpdate { get; set; }
    }
}