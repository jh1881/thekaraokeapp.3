﻿namespace DJWayniesApp.DataModel
{
    public class HostLoginResponse
    {
        public string jwt { get; set; }
    }
}
