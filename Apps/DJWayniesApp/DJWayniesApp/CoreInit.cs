﻿using Autofac;
using DJWayniesApp.Alerts;

namespace DJWayniesApp
{
    public static class CoreInit
    {
        public static void Init(ContainerBuilder builder)
        {
            builder.RegisterType<DialogService>().As<IDialogProvider>().SingleInstance();
            builder.RegisterType<PageProxy>().As<IPage>().SingleInstance();
//            builder.RegisterType<InsightsAnalytics>().As<IAnalytics>().SingleInstance();
        }
    }
}
