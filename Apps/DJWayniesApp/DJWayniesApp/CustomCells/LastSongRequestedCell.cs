﻿using DJWayniesApp.DataModel;
using Xamarin.Forms;

namespace DJWayniesApp.CustomCells
{
    public class LastSongRequestedCell : ViewCell
    {
        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();
            if (!(BindingContext is RequestedSong model)) return;

            var grid = new Grid
            {
                ColumnDefinitions = new ColumnDefinitionCollection
                {
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                },
                Padding = new Thickness(10, 10, 10, 10),
            };

            var titleView = new Label
            {
                FontSize = 12,
                Text = model.Song.Title,
                LineBreakMode = LineBreakMode.WordWrap
            };
            grid.Children.Add(titleView, 0, 0);

            var artistView = new Label
            {
                FontSize = 12,
                Text = model.Song.Artist,
                LineBreakMode = LineBreakMode.WordWrap
            };
            grid.Children.Add(artistView, 1, 0);

            View = grid;
            Height = View.Height;
        }
    }
}
