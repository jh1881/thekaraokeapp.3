﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DJWayniesApp.Controls
{
    public class CustomEntry : Entry
    {
        public const string ReturnKeyPropertyName = "ReturnKeyType";

        public CustomEntry() { }

        public static readonly BindableProperty ReturnKeyTypeProperty = BindableProperty.Create(
            propertyName: ReturnKeyPropertyName,
            returnType: typeof(ReturnKeyTypes),
            declaringType: typeof(CustomEntry),
            defaultValue: ReturnKeyTypes.Done);

        public ReturnKeyTypes ReturnKeyType
        {
            get => (ReturnKeyTypes)GetValue(ReturnKeyTypeProperty);
            set => SetValue(ReturnKeyTypeProperty, value);
        }
    }

    public enum ReturnKeyTypes : int
    {
        Default,
        Go,
        Google,
        Join,
        Next,
        Route,
        Search,
        Send,
        Yahoo,
        Done,
        EmergencyCall,
        Continue
    }
}
