﻿using Xamarin.Forms;

namespace DJWayniesApp.Controls
{
    public class AdMobBuddyControl : View
    {
        public static readonly BindableProperty AdUnitIdProperty = 
            BindableProperty.Create("AdUnitId", typeof(string), typeof(AdMobBuddyControl), string.Empty);

        public string AdUnitId
        {
            get => (string)GetValue(AdUnitIdProperty);
            set => SetValue(AdUnitIdProperty, value);
        }
    }
}