﻿using Xamarin.Forms;
using System.Windows.Input;

namespace DJWayniesApp.Controls
{
    public class HyperLinkLabel : Label
    {
        public static readonly BindableProperty SubjectProperty = BindableProperty.Create("Subject", typeof (string),
            typeof (HyperLinkLabel), string.Empty);
        public static readonly BindableProperty NavigateUriProperty = BindableProperty.Create("NavigateUri",
            typeof (string), typeof (HyperLinkLabel), string.Empty);
        public static readonly BindableProperty NavigateCommandProperty = BindableProperty.Create("NavigateCommand",
            typeof(ICommand), typeof(HyperLinkLabel));

        public string Subject
        {
            get => (string)GetValue(SubjectProperty);
            set => SetValue(SubjectProperty, value);
        }

        public string NavigateUri
        {
            get => (string)GetValue(NavigateUriProperty);
            set => SetValue(NavigateUriProperty, value);
        }

        public ICommand NavigateCommand 
        {
            get => (ICommand)GetValue(NavigateCommandProperty);
            set => SetValue(NavigateCommandProperty, value);
        }
    }
}
