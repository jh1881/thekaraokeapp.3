﻿using System;
using Xamarin.Forms;

namespace DJWayniesApp.Controls
{
    public class CustomRadioButton : View
    {
        public static readonly BindableProperty CheckedProperty = BindableProperty.Create("Checked", typeof(bool), typeof(CustomRadioButton), false);
        public static readonly BindableProperty TextProperty = BindableProperty.Create("Text", typeof(string), typeof(CustomRadioButton), string.Empty);
        public static readonly BindableProperty TextColorProperty = BindableProperty.Create("TextColor", typeof(Color), typeof(CustomRadioButton), Color.FromRgb(0, 122, 255));
        
        public EventHandler<EventArgs<bool>> CheckedChanged;

        public bool Checked
        {
            get => (bool)GetValue(CheckedProperty);
            set
            {
                SetValue(CheckedProperty, value);
                var eventHandler = CheckedChanged;
                eventHandler?.Invoke(this, value);
            }
        }

        public string Text
        {
            get => (string)GetValue(TextProperty);
            set => SetValue(TextProperty, value);
        }

        public Color TextColor
        {
            get => (Color)GetValue(TextColorProperty);
            set => SetValue(TextColorProperty, value);
        }

        public int RadioId { get; set; }
    }
}
