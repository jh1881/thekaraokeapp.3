﻿using DJWayniesApp.ViewModels;

namespace DJWayniesApp.Messages
{
    public class HostChangePageMessage
    {
        public PageType Page { get; set; }
    }
}
