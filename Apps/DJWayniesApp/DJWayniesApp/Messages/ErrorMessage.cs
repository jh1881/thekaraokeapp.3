﻿namespace DJWayniesApp.Messages
{
    public class ErrorMessage
    {
        public string ErrorText { get; set; }
        public string ErrorTitle { get; set; }
    }
}
