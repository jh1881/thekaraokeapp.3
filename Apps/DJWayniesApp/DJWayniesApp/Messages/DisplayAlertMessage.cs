﻿namespace DJWayniesApp.Messages
{
    public class DisplayAlertMessage
    {
        public string Text { get; set; }
        public string Header { get; set; }
        public bool ShowAd { get; set; }
    }
}