﻿namespace TheKaraokeApp.Core.Interfaces
{
    public interface IErrorApp
    {
        void Error(string text);
    }
}