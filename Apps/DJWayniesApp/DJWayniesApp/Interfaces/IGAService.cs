﻿using System;

namespace DJWayniesApp
{
    public interface IGAService
    {
        void TrackPage(string pageName);
        void TrackEvent(string eventCategory, string eventName);
        void TrackException(string exceptionMessage, bool isFatal);
    }
}
