﻿namespace TheKaraokeApp.Core.Interfaces
{
    public interface ISelectable
    {
        bool IsSelected { get; set; }
    }
}