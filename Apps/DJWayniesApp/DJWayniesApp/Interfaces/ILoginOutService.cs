﻿namespace TheKaraokeApp.Core.Interfaces
{
    public interface ILoginOutService
    {
        void LoggedOut();
        void LoggedIn(string token);
    }
}