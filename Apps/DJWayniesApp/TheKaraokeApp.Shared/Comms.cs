﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Plugin.Connectivity;
using Plugin.Geolocator;
using Plugin.Settings;
using Xamarin.Forms;

namespace TheKaraokeApp.Shared
{
    public class Comms
    {
        public class CommsResult
        {
            public HttpStatusCode StatusCode { get; set; } = HttpStatusCode.OK;
            public string RawResponse { get; set; }
            public bool IsSuccess { get; set; }
            public bool NoConnection { get; set; }
            public Exception Exception { get; set; }

            public T Deserialise<T>()
            {
                return JsonConvert.DeserializeObject<T>(RawResponse);
            }

            public object Deserialise(Type targetType)
            {
                return JsonConvert.DeserializeObject(RawResponse, targetType);
            }

            public override string ToString()
            {
                return (Exception == null ? $"{StatusCode} {RawResponse}" : $"Exception: {Exception}");
            }
        }

        private const string ProcessAmiahost = "AM";
        private const string ProcessChooseHost = "HO";
        private const string ProcessSongList = "SN";
        private const string ProcessRecentSongList = "RS";
        private const string ProcessRequestSong = "RQ";
        private const string ProcessRequestPush = "PU";
        private const string ProcessStartShow = "ST";
        private const string ProcessStopShow = "SP";
        private const string ProcessSongRequests = "UR";
        private const string ProcessCheckShowStatus = "CS";
        private const string ProcessHostLogin = "LO";
        private const string ProcessVerifyLogin = "VE";

        private const string CommsVersion = "3";
        private const string BaseUrl = "http://thekaraokeapp.co.uk/v" + CommsVersion;

        private static string _loginToken;
        public static IAnalytics Analytics { get; set; }

        public static string LoginToken
        {
            get => _loginToken;
            set
            {
                _loginToken = value;
                OnLoginTokenChanged?.Invoke(null, EventArgs.Empty);
            }
        }

        public static event EventHandler OnLoginTokenChanged;

        public static bool IsConnected()
        {
            var isConnected = CrossConnectivity.Current.IsConnected;
            Debug.WriteLine($"Comms.IsConnected said {isConnected}");
            return isConnected;
        }

        private static async Task<CommsResult> CallApi(string url)
        {
            Debug.WriteLine($"Comms.CallApi asked {url}");
            Analytics?.Track("Comms.CallApi", "url", url);
            if (!IsConnected())
            {
                Debug.WriteLine($"Comms.CallApi({url}) - no connection found");
                return new CommsResult {IsSuccess = false, NoConnection = true};
            }

            using (var client = new HttpClient())
            {
                var result = new CommsResult();
                HttpResponseMessage response = null;
                try
                {
                    var msg = new HttpRequestMessage(HttpMethod.Get, url);
                    if (!string.IsNullOrEmpty(LoginToken))
                        msg.Headers.Add("Authorization", $"Bearer {LoginToken}");
                    response = await client.SendAsync(msg);
                    result.RawResponse = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode) result.IsSuccess = true;
                    else result.StatusCode = response.StatusCode;
                }
                catch (Exception e)
                {
                    Analytics?.ReportException(e);
                    result.IsSuccess = false;
                    result.StatusCode = response?.StatusCode ?? HttpStatusCode.Unused;
                    result.RawResponse = $"Exception: {e.Message}";
                    result.Exception = e;
                }

                Debug.WriteLine($"Comms.CallApi({url}) got {result}");
                return result;
            }
        }

        private static string GetProcessUrl(string operation, string parameters = null)
        {
            var url = $"{BaseUrl}/process.php?op={operation}&uid={Uid}";
            if (!string.IsNullOrEmpty(parameters)) url += $"&{parameters}";
            return url;
        }

        private static Guid Uid
        {
            get
            {
                var id = CrossSettings.Current.GetValueOrDefault("GuidId", default(Guid));
                if (id == default(Guid))
                {
                    CrossSettings.Current.AddOrUpdateValue("GuidId", Guid.NewGuid());
                    id = CrossSettings.Current.GetValueOrDefault("GuidId", default(Guid));
                }
                return id;
            }
        }

        public static async Task<CommsResult> ChooseHost(string number)
        {
            Debug.WriteLine("Comms.ChooseHost started");
            var url = GetProcessUrl(ProcessChooseHost, $"number={number}");
            try
            {
                var result = await CallApi(url);
                Debug.WriteLine($"Comms.ChooseHost ended with result {result}");
                return result;
            }
            catch (Exception e)
            {
                Debug.WriteLine($"Comms.ChooseHost exception {e}");
                Analytics?.ReportException(e);
                return new CommsResult { IsSuccess = false, Exception = e };
            }
        }

        public static async Task<CommsResult> AmIaHost(string uid, string fbid = null)
        {
            var url = (fbid == null ? GetProcessUrl(ProcessAmiahost) : GetProcessUrl(ProcessAmiahost, $"facebookid={fbid}"));
            var value = await CallApi(url);
            return value;
        }

        public static async Task<CommsResult> SongListCount(string hostId)
        {
            var url = GetProcessUrl(ProcessSongList, $"hostid={hostId}&count=");
            var returnValue = await CallApi(url);
            Debug.WriteLine($"Comms.SongListCount got {returnValue}");
            return returnValue;
        }

        public static async Task<CommsResult> SongList(string hostId, bool exactMatch = false, string search = null, int firstRow = 0, bool orderByTitle = false, bool getCount = false)
        {
            var url = GetProcessUrl(ProcessSongList, $"hostid={hostId}&limit={firstRow}");
            if (string.IsNullOrEmpty(search) == false)
            {
                url += $"&search={WebUtility.UrlEncode(search)}";
            }
            if (orderByTitle) url += "&orderbytitle=";
            if (exactMatch) url += "&exactmatch=";
            if (getCount) url += "&count=";
            if (getCount) url += "&new=";
            var returnValue = await CallApi(url);
            Debug.WriteLine($"Comms.SongList asked {url} and got {returnValue}");
            return returnValue;
        }

        public static async Task<CommsResult> RecentSongList(string hostId, bool orderByTitle = false)
        {
            var url = GetProcessUrl(ProcessRecentSongList, $"hostid={hostId}");
            if (orderByTitle) url += "&orderbytitle=";
            var returnValue = await CallApi(url);
            Debug.WriteLine($"Comms.RecentSongList asked {url} and got {returnValue}");
            return returnValue;
        }

        public enum RequestSongResult
        {
            Success,
            ServerError,
            ShowNotInProgress,
            HostNotFound,
            BadRequest,
            NoConnection,
            TooManyRequests,
            UnknownError
        }

        private static async Task AddGeoData(StringBuilder sb, bool includeInitialAmpersand = true)
        {
            if (CrossGeolocator.IsSupported && CrossGeolocator.Current.IsGeolocationAvailable && CrossGeolocator.Current.IsGeolocationEnabled)
            {
                try
                {
                    var position = await CrossGeolocator.Current.GetPositionAsync(TimeSpan.FromSeconds(5));
                    if (position != null)
                    {
                        sb.AppendFormat("{2}lat={0}&lon={1}", position.Latitude, position.Longitude, includeInitialAmpersand ? "&" : "");
                    }
                }
                catch
                {
                    //
                }
            }
        }

        public static async Task<RequestSongResult> RequestSong(string title, string artist, string hostId, string requestor, string keyChange = null, string tempoChange = null, string duetSecond = null, string groupNames = null)
        {
            Debug.WriteLine($"Comms.RequestSong({title}, {artist}, {hostId}, {requestor}, {keyChange ?? "null"}, {tempoChange ?? "null"}, {duetSecond ?? "null"}, {groupNames ?? "null"})");
            var sb = new StringBuilder($"title={WebUtility.UrlEncode(title)}&artist={WebUtility.UrlEncode(artist)}&requestor={WebUtility.UrlEncode(requestor)}&hostid={hostId}");
            if (string.IsNullOrEmpty(keyChange) == false) sb.AppendFormat("&keychange={0}", WebUtility.UrlEncode(keyChange));
            if (string.IsNullOrEmpty(tempoChange) == false) sb.AppendFormat("&tempochange={0}", WebUtility.UrlEncode(tempoChange));
            if (string.IsNullOrEmpty(duetSecond) == false) sb.AppendFormat("&duetsecond={0}", WebUtility.UrlEncode(duetSecond));
            if (string.IsNullOrEmpty(groupNames) == false) sb.AppendFormat("&groupnames={0}", WebUtility.UrlEncode(groupNames));
            switch (Device.RuntimePlatform)
            {
                case Device.iOS: sb.AppendFormat("&deviceType=1"); break;
                case Device.Android: sb.AppendFormat("&deviceType=2"); break;
            }

            // Get a location
            await AddGeoData(sb);

            var url = GetProcessUrl(ProcessRequestSong, sb.ToString());
            Debug.WriteLine($"Comms.RequestSong url = {url}");

            var result = await CallApi(url);
            var songResult = RequestSongResult.Success;
            if (result.IsSuccess == false)
            {
                if (result.NoConnection) return RequestSongResult.NoConnection;
                if ((int)result.StatusCode == 429)
                {
                    songResult = RequestSongResult.TooManyRequests;
                }
                else
                {
                    switch (result.StatusCode)
                    {
                        case HttpStatusCode.InternalServerError:
                            songResult = RequestSongResult.ServerError;
                            break;
                        case HttpStatusCode.BadRequest:
                            songResult = RequestSongResult.BadRequest;
                            break;
                        case HttpStatusCode.NotFound:
                            songResult = RequestSongResult.HostNotFound;
                            break;
                        case HttpStatusCode.Forbidden:
                            songResult = RequestSongResult.ShowNotInProgress;
                            break;
                        default:
                            songResult = RequestSongResult.UnknownError;
                            break;
                    }
                }
            }
            Debug.WriteLine($"Comms.RequestSong ended with {songResult}");
            return songResult;
        }

        public enum DeviceType
        {
            Ios = 1,
            Android = 2,
            Windows = 3
        }

        public static async Task<bool> SendPushNotificationToken(string deviceId, DeviceType deviceType)
        {
            var parameters = $"deviceid={deviceId}&devicetype=";
            switch (deviceType)
            {
                case DeviceType.Ios: parameters += "1"; break;
                case DeviceType.Android: parameters += "2"; break;
                case DeviceType.Windows: parameters += "3"; break;
            }
            var url = GetProcessUrl(ProcessRequestPush, parameters);
            var result = await CallApi(url);
            return result.IsSuccess;
        }

        public static async Task<CommsResult> StartShow()
        {
            var sb = new StringBuilder("");
            await AddGeoData(sb, false);

            var url = GetProcessUrl(ProcessStartShow, sb.ToString());

            return await CallApi(url);
        }

        public static async Task<CommsResult> StopShow()
        {
            var url = GetProcessUrl(ProcessStopShow);
            return await CallApi(url);
        }

        public static async Task<CommsResult> UnreadSongRequests(string hostId = null)
        {
            var url = GetProcessUrl(ProcessSongRequests, (hostId != null ? $"hostid={hostId}" : null));
            var response = await CallApi(url);
            return response;
        }

        public static async Task<CommsResult> GetShowStatus()
        {
            var url = GetProcessUrl(ProcessCheckShowStatus);
            var response = await CallApi(url);
            return response;
        }

        public static async Task<CommsResult> LoginHost(string username, string password)
        {
            var url = GetProcessUrl(ProcessHostLogin, $"u={username}&p={password}");
            var response = await CallApi(url);
            return response;
        }

        public static async Task<CommsResult> VerifyHost()
        {
            var url = GetProcessUrl(ProcessVerifyLogin);
            var response = await CallApi(url);
            return response;
        }
    }
}
