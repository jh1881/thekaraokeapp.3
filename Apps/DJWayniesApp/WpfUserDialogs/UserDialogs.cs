﻿using System;
using System.Threading.Tasks;

namespace WpfUserDialogs
{
    public static class UserDialogs
    {
        public static void Init(Func<Action, Task> customDispatcher = null)
        {
            Acr.UserDialogs.UserDialogs.Instance = new UserDialogsImpl();
        }
    }
}
