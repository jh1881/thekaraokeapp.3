﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Acr.UserDialogs;

namespace WpfUserDialogs
{
    internal class UserDialogsImpl : AbstractUserDialogs
    {
        private readonly Func<Action, Task> _dispatcher;

        public UserDialogsImpl(Func<Action, Task> dispatcher = null)
        {
//            _dispatcher = dispatcher ?? new Func<Action, Task>(x => Application.Current.MainWindow.Dispatcher);
        }

        public override IDisposable Alert(AlertConfig config)
        {
            return null;
        }

        public override IDisposable ActionSheet(ActionSheetConfig config)
        {
            throw new NotImplementedException();
        }

        public override IDisposable Confirm(ConfirmConfig config)
        {
            throw new NotImplementedException();
        }

        public override IDisposable DatePrompt(DatePromptConfig config)
        {
            throw new NotImplementedException();
        }

        public override IDisposable TimePrompt(TimePromptConfig config)
        {
            throw new NotImplementedException();
        }

        public override IDisposable Login(LoginConfig config)
        {
            throw new NotImplementedException();
        }

        public override IDisposable Prompt(PromptConfig config)
        {
            throw new NotImplementedException();
        }

        public override IDisposable Toast(ToastConfig config)
        {
            throw new NotImplementedException();
        }

        protected override IProgressDialog CreateDialogInstance(ProgressDialogConfig config)
        {
            throw new NotImplementedException();
        }
    }
}
