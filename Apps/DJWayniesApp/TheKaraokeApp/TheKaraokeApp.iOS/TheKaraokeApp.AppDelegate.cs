﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Acr.UserDialogs;
using Foundation;
using UIKit;
using Xamarin.Forms;

namespace TheKaraokeApp.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication uiApp, NSDictionary options)
        {
            Xamarin.Forms.Forms.Init();
            var app = new App();
            LoadApplication(app);

            return base.FinishedLaunching(uiApp, options);
        }

        public override void FailedToRegisterForRemoteNotifications(UIApplication application, NSError error)
        {
            // Notify the user!
//            App.Analytics?.Track("FailedToRegisterForRemoteNotifications");
            Device.BeginInvokeOnMainThread(async () => await UserDialogs.Instance.AlertAsync("Error registering push notifications"));
        }

        public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {
            var token = deviceToken.Description;
            if (!string.IsNullOrWhiteSpace(token))
            {
                token = token.Trim('<').Trim('>').Replace(" ", "");
            }
            Debug.WriteLine($"Device Token {token}");
            //App.Analytics?.Track("RegisteredForRemoteNotifications", "token", token);
            //System.Threading.Tasks.Task.Factory.StartNew(async () => await Comms.SendPushNotificationToken(token, Comms.DeviceType.Ios));
        }

        public override void DidReceiveRemoteNotification(UIApplication application, NSDictionary userInfo, Action<UIBackgroundFetchResult> completionHandler)
        {
//            App.Analytics?.Track("DidReceiveRemoteNotification");

            // Push notification received.
//            System.Threading.Tasks.Task.Factory.StartNew(async () => await _app.ProcessBackgrounding());
            completionHandler(UIBackgroundFetchResult.NewData);
        }
    }
}
