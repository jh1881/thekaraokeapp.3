﻿using System.ComponentModel;
using Android.Content;
using Android.Views.InputMethods;
using DJWayniesApp.Controls;
using DJWayniesApp.Droid.CustomRenderers;
using DJWayniesApp.Droid.Extensions;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
namespace DJWayniesApp.Droid.CustomRenderers
{
    public class CustomEntryRenderer : EntryRenderer
    {
        public CustomEntryRenderer(Context context) : base(context) { }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if ((Control != null) && (e.NewElement != null))
            {
                var entryExt = (e.NewElement as CustomEntry);
                Control.ImeOptions = entryExt?.ReturnKeyType.GetValueFromDescription() ?? ImeAction.Done;
                // This is hackie ;-) / A Android-only bindable property should be added to the EntryExt class 
                Control.SetImeActionLabel(entryExt?.ReturnKeyType.ToString(), Control.ImeOptions);
            }
        }
        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (e.PropertyName == CustomEntry.ReturnKeyPropertyName)
            {
                var entryExt = (sender as CustomEntry);
                Control.ImeOptions = entryExt?.ReturnKeyType.GetValueFromDescription() ?? ImeAction.Done;
                // This is hackie ;-) / A Android-only bindable property should be added to the EntryExt class 
                Control.SetImeActionLabel(entryExt?.ReturnKeyType.ToString(), Control.ImeOptions);
            }
        }
    }
}
