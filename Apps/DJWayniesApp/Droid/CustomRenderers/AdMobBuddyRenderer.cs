using System.ComponentModel;
using Android.Content;
using Android.Gms.Ads;
using DJWayniesApp.Controls;
using DJWayniesApp.Droid.CustomRenderers;
using DJWayniesApp.Droid.Services;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(AdMobBuddyControl), typeof(AdMobBuddyRenderer))]

namespace DJWayniesApp.Droid.CustomRenderers
{
    public class AdMobBuddyRenderer : ViewRenderer
    {
        public AdMobBuddyRenderer(Context context) : base(context) { }

        protected override void OnElementChanged(ElementChangedEventArgs<View> e)
        {
            base.OnElementChanged(e);

            if (Element is AdMobBuddyControl && e.OldElement != null)
            {
                var ad = new AdView(Context)
                {
                    AdSize = AdSize.Banner,
                    AdUnitId = DroidAdService.BannerAdUnitId
                };
                var requestBuilder = new AdRequest.Builder();
                ad.LoadAd(requestBuilder.Build());
                SetNativeControl(ad);
            }
        }
    }
}
