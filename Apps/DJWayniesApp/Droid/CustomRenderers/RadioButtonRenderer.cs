using System.ComponentModel;
using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.Widget;
using DJWayniesApp.Controls;
using DJWayniesApp.Droid.CustomRenderers;
using Xamarin.Forms.Platform.Android;

[assembly:MetaData("com.facebook.sdk.ApplicationId", Value="@string/app_id")]
[assembly: Xamarin.Forms.ExportRenderer(typeof(CustomRadioButton), typeof(RadioButtonRenderer))]
namespace DJWayniesApp.Droid.CustomRenderers
{
    public class RadioButtonRenderer : ViewRenderer<CustomRadioButton, RadioButton>
    {
        private ColorStateList _defaultTextColor;

        public RadioButtonRenderer(Context context) : base(context) { }

        protected override void OnElementChanged(ElementChangedEventArgs<CustomRadioButton> e)
        {
            base.OnElementChanged(e);

            if (Control == null)
            {
                var radButton = new RadioButton(Context);
                _defaultTextColor = radButton.TextColors;
                radButton.CheckedChange += (sender, args) => Element.Checked = args.IsChecked;
                SetNativeControl(radButton);
            }

            Control.Text = e.NewElement.Text;
            Control.Checked = e.NewElement.Checked;

            UpdateTextColour();
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            switch (e.PropertyName)
            {
                case "Checked": Control.Checked = Element.Checked; break;
                case "Text": Control.Text = Element.Text; break;
                case "TextColor": UpdateTextColour(); break;
            }
        }

        private void UpdateTextColour()
        {
            if (Control == null || Element == null) return;
            if(Element.TextColor == Xamarin.Forms.Color.Default)
                Control.SetTextColor(_defaultTextColor);
            else
                Control.SetTextColor(Element.TextColor.ToAndroid());
        }
    }
}