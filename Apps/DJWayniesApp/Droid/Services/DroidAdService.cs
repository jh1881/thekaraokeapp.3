﻿using Android.Gms.Ads;
using Android.OS;
using DJWayniesApp.Droid.Services;
using DJWayniesApp.Helpers;
using DJWayniesApp.Interfaces;
using Google.Ads.Mediation.Admob;
using Java.Lang;
using Xamarin.Forms;

[assembly: Dependency(typeof(DroidAdService))]
namespace DJWayniesApp.Droid.Services
{
    public class DroidAdService : IAdService
    {
        internal const string BannerAdUnitId = "ca-app-pub-8064175537251762/5376594503";
        internal const string InterstitialAdUnitId = "ca-app-pub-8064175537251762/9736383902";
        private InterstitialAd _interstitialAd;

        public void ShowInterstitialAd()
        {
            CreateAd();
        }

        void CreateAd()
        {
            _interstitialAd = new InterstitialAd(Android.App.Application.Context) { AdUnitId = InterstitialAdUnitId };

            LoadAd();
        }

        private void LoadAd()
        {
            _interstitialAd.LoadAd(GetAdRequest());
        }

        void ShowAd()
        {
            if (_interstitialAd == null)
                CreateAd();
            if (_interstitialAd.IsLoaded)
                _interstitialAd.Show();
            LoadAd();
        }

        internal AdRequest GetAdRequest()
        {
            var requestBuilder = new AdRequest.Builder();
            if (Settings.NonPersonalisedAds)
            {
                var extras = new Bundle();
                extras.PutString("npa", "1");
                requestBuilder.AddNetworkExtrasBundle(Class.FromType(typeof(AdMobAdapter)), extras);
            }

            return requestBuilder.Build();
        }
    }
}
