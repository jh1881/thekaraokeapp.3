using System;
using DJWayniesApp.Droid.Services;
using DJWayniesApp.Interfaces;

[assembly: Xamarin.Forms.Dependency(typeof(DroidPushNotifications))]

namespace DJWayniesApp.Droid.Services
{
    class DroidPushNotifications : IPushNotifications
    {
        public void Begin()
        {
        }

        public void End()
        {
        }

        public void ClearBadge()
        {
        }
    }
}
