using DJWayniesApp.Droid.Services;
using TheKaraokeApp.Core.Interfaces;

[assembly: Xamarin.Forms.Dependency(typeof(DroidLoginOutService))]

namespace DJWayniesApp.Droid.Services
{
    class DroidLoginOutService : ILoginOutService
    {
        public void LoggedOut()
        {
        }

        public void LoggedIn(string token)
        {
        }
    }
}
