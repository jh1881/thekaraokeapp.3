﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TKADesktop.DataModel;
using TKADesktop.Helpers;

namespace TKADesktop
{
    static class WpfComms
    {
        public class CommsResult
        {
            public HttpStatusCode StatusCode { get; set; } = HttpStatusCode.OK;
            public string RawResponse { get; set; }
            public bool IsSuccess { get; set; }
            public bool NoConnection { get; set; }
            public Exception Exception { get; set; }

            public T Deserialise<T>()
            {
                return JsonConvert.DeserializeObject<T>(RawResponse);
            }

            public object Deserialise(Type targetType)
            {
                return JsonConvert.DeserializeObject(RawResponse, targetType);
            }

            public override string ToString()
            {
                return (Exception == null ? $"{StatusCode} {RawResponse}" : $"Exception: {Exception}");
            }
        }

        private const string ProcessAmiahost = "AM";
        private const string ProcessChooseHost = "HO";
        private const string ProcessSongList = "SN";
        private const string ProcessRecentSongList = "RS";
        private const string ProcessRequestSong = "RQ";
        private const string ProcessRequestPush = "PU";
        private const string ProcessStartShow = "ST";
        private const string ProcessStopShow = "SP";
        private const string ProcessSongRequests = "UR";
        private const string ProcessCheckShowStatus = "CS";
        private const string ProcessHostLogin = "LO";
        private const string ProcessVerifyLogin = "VE";

        private const string CommsVersion = "3";
        private const string BaseUrl = "http://thekaraokeapp.co.uk/v" + CommsVersion;

        private static string LoginToken => Database.Token;

        private static async Task<CommsResult> CallApi(string url)
        {
            Debug.WriteLine($"Comms.CallApi asked {url}");
//            App.Analytics?.Track("Comms.CallApi", "url", url);
            //if (!IsConnected())
            //{
            //    Debug.WriteLine($"Comms.CallApi({url}) - no connection found");
            //    return new CommsResult { IsSuccess = false, NoConnection = true };
            //}

            using (var client = new HttpClient())
            {
                var result = new CommsResult();
                HttpResponseMessage response = null;
                try
                {
                    var msg = new HttpRequestMessage(HttpMethod.Get, url);
                    if (!string.IsNullOrEmpty(LoginToken))
                        msg.Headers.Add("Authorization", $"Bearer {LoginToken}");
                    //                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", LoginToken);

                    //                    response = await client.GetAsync(url);
                    response = await client.SendAsync(msg);
                    result.RawResponse = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode) result.IsSuccess = true;
                    else result.StatusCode = response.StatusCode;
                }
                catch (Exception e)
                {
//                    App.Analytics?.ReportException(e);
                    result.IsSuccess = false;
                    result.StatusCode = response?.StatusCode ?? HttpStatusCode.Unused;
                    result.RawResponse = $"Exception: {e.Message}";
                    result.Exception = e;
                }
                Debug.WriteLine($"Comms.CallApi({url}) got {result}");
                return result;
            }
        }

        private static string GetProcessUrl(string operation, string parameters = null)
        {
            var url = $"{BaseUrl}/process.php?op={operation}&uid={Database.Id}";
            if (!string.IsNullOrEmpty(parameters)) url += $"&{parameters}";
            return url;
        }

        public static async Task<CommsResult> ChooseHost(string number)
        {
            Debug.WriteLine("Comms.ChooseHost started");
            var url = GetProcessUrl(ProcessChooseHost, $"number={number}");
            try
            {
                var result = await CallApi(url);
                Debug.WriteLine($"Comms.ChooseHost ended with result {result}");
                return result;
            }
            catch (Exception e)
            {
                Debug.WriteLine($"Comms.ChooseHost exception {e}");
                return new CommsResult { IsSuccess = false, Exception = e };
            }
        }

        public static async Task<CommsResult> AmIaHost()
        {
            var url = GetProcessUrl(ProcessAmiahost);
            var value = await CallApi(url);
            return value;
        }

        public static async Task<CommsResult> StartShow()
        {
            var url = GetProcessUrl(ProcessStartShow);
            return await CallApi(url);
        }

        public static async Task<CommsResult> StopShow()
        {
            var url = GetProcessUrl(ProcessStopShow);
            return await CallApi(url);
        }

        public static async Task<CommsResult> UnreadSongRequests(string hostId = null)
        {
            var url = GetProcessUrl(ProcessSongRequests, (hostId != null ? $"hostid={hostId}" : null));
            var response = await CallApi(url);
            return response;
        }

        public static async Task<CommsResult> GetShowStatus()
        {
            var url = GetProcessUrl(ProcessCheckShowStatus);
            var response = await CallApi(url);
            return response;
        }

        public static async Task<CommsResult> LoginHost(string username, string password)
        {
            var url = GetProcessUrl(ProcessHostLogin, $"u={username}&p={password}");
            var response = await CallApi(url);
            return response;
        }

        public static async Task<CommsResult> VerifyHost()
        {
            var url = GetProcessUrl(ProcessVerifyLogin);
            var response = await CallApi(url);
            return response;
        }
    }
}
