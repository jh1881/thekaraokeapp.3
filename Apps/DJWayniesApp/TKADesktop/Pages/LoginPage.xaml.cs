﻿using System.Windows.Controls;
using TKADesktop.ViewModels;

namespace TKADesktop.Pages
{
    public partial class LoginPage : Page
    {
        public LoginPage()
        {
            InitializeComponent();

            DataContext = new LoginPageViewModel(this);
        }
    }
}
