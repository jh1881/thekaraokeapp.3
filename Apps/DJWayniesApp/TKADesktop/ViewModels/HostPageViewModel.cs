﻿using System.Collections.ObjectModel;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using TKADesktop.DataModel;
using TKADesktop.Helpers;
using TKADesktop.Messages;

namespace TKADesktop.ViewModels
{
    public class HostPageViewModel : BaseViewModel
    {
        private ICommand _clearCommand, _startShowCommand, _stopShowCommand, _logoutCommand;
        private bool _isRefreshing;
        public ObservableCollection<RequestedSong> Songs { get; set; }

        public ICommand ClearDoneCommand => _clearCommand ?? (_clearCommand = new RelayCommand(ClearDone));
        public ICommand StartShowCommand => _startShowCommand ?? (_startShowCommand = new AsyncCommand(StartShow));
        public ICommand StopShowCommand => _stopShowCommand ?? (_stopShowCommand = new AsyncCommand(StopShow));
        public ICommand LogoutCommand => _logoutCommand ?? (_logoutCommand = new RelayCommand(Logout));

        public bool IsRefreshing
        {
            get => _isRefreshing;
            set
            {
                if (value == _isRefreshing) return;
                _isRefreshing = value;
                OnPropertyChanged();
            }
        }

        public HostPageViewModel()
        {
            Songs = Database.Songs;

            MessagingCenter.Subscribe<RefreshingMessage>(this, "", msg => IsRefreshing = msg.IsRefreshing);
        }

        private static void ClearDone() => Database.ClearDone();

        private async Task StartShow()
        {
            var result = await WpfComms.StartShow();
            if (result.IsSuccess)
            {
                MessageBox.Show("Show started");
            }
            else
            {
                if (result.StatusCode == HttpStatusCode.Conflict)
                    MessageBox.Show("Show was already started");
                else MessageBox.Show("Could not start show");
            }
        }

        private async Task StopShow()
        {
            await WpfComms.StopShow();
            MessageBox.Show("Done");
        }

        private void Logout()
        {
            MessagingCenter.Send(new LogoutMessage(), "");
        }
    }
}
