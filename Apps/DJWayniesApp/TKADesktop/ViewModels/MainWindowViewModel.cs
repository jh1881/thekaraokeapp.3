﻿using System.Windows;
using System.Windows.Input;
using TKADesktop.DataModel;
using TKADesktop.Helpers;
using TKADesktop.Messages;
using TKADesktop.Pages;
using TKADesktop.Views;
using Mouse = TKADesktop.Helpers.Mouse;
// ReSharper disable BitwiseOperatorOnEnumWithoutFlags

namespace TKADesktop.ViewModels
{
    public partial class MainWindowViewModel : BaseViewModel, INavigator
    {
        private readonly MainWindow _window;
        private int _outerMargin = 10;
        private int _windowRadius = 12;
        private int _resizeBorder = 4;
        private ApplicationPage _currentPage;


        public ApplicationPage CurrentPage
        {
            get => _currentPage;
            set
            {
                if (value == _currentPage) return;
                _currentPage = value;
                OnPropertyChanged();
            }
        }

        private int ResizeBorder
        {
            get => _resizeBorder;
            set
            {
                _resizeBorder = value;
                OnPropertyChanged(nameof(ResizeBorderThickness));
            }
        }

        private int WindowRadius
        {
            get => _window.WindowState == WindowState.Maximized ? 0 : _windowRadius;
            set
            {
                _windowRadius = value;
                OnPropertyChanged(nameof(WindowCornerRadius));
            }
        }

        private int OuterMarginSize
        {
            get => _window.WindowState == WindowState.Maximized ? 0 : _outerMargin;
            set
            {
                _outerMargin = value;
                OnPropertyChanged(nameof(OuterMarginThickness));
            }
        }

        public CornerRadius WindowCornerRadius => new CornerRadius(WindowRadius);
        public Thickness OuterMarginThickness => new Thickness(OuterMarginSize);
        public Thickness ResizeBorderThickness => new Thickness(ResizeBorder + OuterMarginSize);
        public Thickness InnerContentPadding => new Thickness(4);

        public int TitleHeight => 30;
        public GridLength TitleHeightGridLength => new GridLength(TitleHeight + ResizeBorder);

        public ICommand MinimizeCommand { get; set; }
        public ICommand MaximizeCommand { get; set; }
        public ICommand CloseCommand { get; set; }
        public ICommand MenuCommand { get; set; }

//        private bool _isLoggedIn;

        public MainWindowViewModel(MainWindow window)
        {
            _window = window;

            _window.StateChanged += (sender, args) => WindowResized();

            MinimizeCommand = new RelayCommand(o => _window.WindowState = WindowState.Minimized);
            MaximizeCommand = new RelayCommand(o => _window.WindowState ^= WindowState.Maximized);
            CloseCommand = new RelayCommand(o => _window.Close());
            MenuCommand = new RelayCommand(o => SystemCommands.ShowSystemMenu(_window, Mouse.GetMousePosition(_window)));

            var resizer = new WindowResizer(_window);
            resizer.WindowDockChanged += dock => WindowResized();

            InitMessaging();

            CurrentPage = ApplicationPage.Login;
            // Are we still logged in?
            //if (!string.IsNullOrWhiteSpace(Database.Token))
            //{
            //    CurrentPage = ApplicationPage.Host;
            //}
            //else
            //{
            //}
        }

        private void InitMessaging()
        {
            MessagingCenter.Subscribe<LoginMessage>(this, "", message =>
            {
                NavigateTo(ApplicationPage.Host);
            });
            MessagingCenter.Subscribe<LogoutMessage>(this, "", message =>
            {
                NavigateTo(ApplicationPage.Login);
            });
        }

        private void WindowResized()
        {
            OnPropertyChanged(nameof(WindowCornerRadius));
            OnPropertyChanged(nameof(OuterMarginThickness));
            OnPropertyChanged(nameof(ResizeBorderThickness));
        }

        public void NavigateTo(ApplicationPage page)
        {
            CurrentPage = page;
        }
    }
}
