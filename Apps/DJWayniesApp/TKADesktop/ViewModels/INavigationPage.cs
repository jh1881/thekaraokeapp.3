﻿namespace TKADesktop.ViewModels
{
    public interface INavigationPage
    {
        void OnNavigatedTo();
        void OnNavigatedFrom();
    }
}
