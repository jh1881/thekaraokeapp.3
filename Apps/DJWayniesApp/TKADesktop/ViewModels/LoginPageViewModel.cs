﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using TKADesktop.Controls;
using TKADesktop.DataModel;
using TKADesktop.Helpers;
using TKADesktop.Messages;
using TKADesktop.Pages;

namespace TKADesktop.ViewModels
{
    public class LoginPageViewModel : BaseViewModel, INavigationPage
    {
        private readonly LoginPage _loginPage;
        private bool _isLoggingIn;
        private ICommand _loginCommand;

        public LoginPageViewModel(LoginPage loginPage)
        {
            _loginPage = loginPage;

            if (!string.IsNullOrEmpty(Database.Token))
            {
                Task.Run(async () =>
                {
                    IsLoggingIn = true;
                    try
                    {
                        await Task.Delay(5000);
                        var result = await WpfComms.VerifyHost();
                        if (result.IsSuccess)
                        {
                            MessagingCenter.Send(new LoginMessage(), "");
                        }
                        else
                        {
                            Database.Token = null;
                            Database.Save();
                        }
                    }
                    catch  { /**/ }
                    finally
                    {
                        IsLoggingIn = false;
                    }
                });
            }
        }

        public bool IsLoggingIn
        {
            get => _isLoggingIn;
            set
            {
                if (value == _isLoggingIn) return;
                _isLoggingIn = value;
                OnPropertyChanged();
            }
        }

        public ICommand LoginRequestedCommand => _loginCommand ?? (_loginCommand = new AsyncCommand<LoginControl.OnLoginEventArgs>(async args =>
        {
            IsLoggingIn = true;
            try
            {
                var password = _loginPage.LoginControl.Password;
                var username = _loginPage.LoginControl.Username;

                // Attempt the login
                var result = await WpfComms.LoginHost(username, password);
                if(result.IsSuccess)
                {
                    var data = result.Deserialise<HostLoginResponse>();
                    Database.Token = data.jwt;
                    MessagingCenter.Send(new LoginMessage(), "");
                }
                else
                {
                    MessageBox.Show("Failed to login.");
                }
            }
            catch (Exception ee)
            {
            }
            finally
            {
                IsLoggingIn = false;
            }
        }));

        public void OnNavigatedTo()
        {
        }

        public void OnNavigatedFrom()
        {
        }
    }
}
