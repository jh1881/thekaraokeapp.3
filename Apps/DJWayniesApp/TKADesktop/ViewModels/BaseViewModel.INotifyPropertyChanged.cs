﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using TKADesktop.Annotations;

namespace TKADesktop.ViewModels
{
    partial class BaseViewModel
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
