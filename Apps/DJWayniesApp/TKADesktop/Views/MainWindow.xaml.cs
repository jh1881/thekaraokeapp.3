﻿using System.Windows;
using TKADesktop.ViewModels;

namespace TKADesktop.Views
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            DataContext = new MainWindowViewModel(this);
        }
    }
}
