﻿namespace TKADesktop.Messages
{
    class RefreshingMessage
    {
        public bool IsRefreshing { get; private set; }

        public RefreshingMessage(bool isRefreshing)
        {
            IsRefreshing = isRefreshing;
        }
    }
}
