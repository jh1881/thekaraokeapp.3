﻿using System.Windows;
using TKADesktop.DataModel;
using TKADesktop.Helpers;
using TKADesktop.Messages;

namespace TKADesktop
{
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            MessagingCenter.Subscribe<LoginMessage>(this, "", msg =>
            {
                // Start the background thread
                BackgroundThread.Start();
            });
            MessagingCenter.Subscribe<LogoutMessage>(this, "", async msg =>
            {
                // STop the background thread
                await BackgroundThread.Stop();
            });

            Database.Init();

            Toaster.Toast("Starting The Karaoke App...");
        }
    }
}
