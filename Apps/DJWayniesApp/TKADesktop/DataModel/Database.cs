﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TKADesktop.Helpers;
using TKADesktop.Messages;
using Application = System.Windows.Application;

namespace TKADesktop.DataModel
{
    // In-memory, and also persistence.
    public static class Database
    {
        private const string SaveFile = "tru.dat";
        private static MessageReceiver _receiver;
        public static ObservableCollection<RequestedSong> Songs { get; private set; } = new ObservableCollection<RequestedSong>
        {
#if DEBUG
            //new RequestedSong
            //{
            //    Artist = "Dolly Parton",
            //    Title = "Jolene",
            //    Requestor = "Adam G",
            //    RequestedTime = DateTime.Now.AddHours(-0.5),
            //},
            //new RequestedSong
            //{
            //    Artist = "Dolly Parton",
            //    Title = "9 to 5",
            //    Requestor = "Adam G",
            //    RequestedTime = DateTime.Now.AddHours(-1),
            //},
            //new RequestedSong
            //{
            //    Artist = "Todrick Hall",
            //    Title = "Nails, Hair, Hips, Heels",
            //    Requestor = "Adam G",
            //    RequestedTime = DateTime.Now.AddHours(-1),
            //},
#endif
        };

        public static string Token { get; set; } = "";
        public static string Id { get; set; }
        public static string HostId { get; set; }

        public class SaveData
        {
            public List<RequestedSong> Songs { get; set; }
            public string Token { get; set; }   // Encrypt this!!!
            public string Id { get; set; }
        }

        class MessageReceiver
        {
            public MessageReceiver()
            {
                MessagingCenter.Subscribe<LoginMessage>(this, "", async msg =>
                {
                    // If we got a successful login, check which host we are
                    var result = await WpfComms.AmIaHost();
                    if (result.IsSuccess)
                    {
                        AmIAHostResult realResult = null;
                        try
                        {
                            realResult = result.Deserialise<AmIAHostResult>();
                        }
                        catch (JsonReaderException jre)
                        {
                            if (int.TryParse(result.RawResponse, out var x))
                            {
                                realResult = new AmIAHostResult { Id = x.ToString() };
                            }
                            else
                            {
                                return;
                            }
                        }

                        HostId = realResult.Id;
                        GetSongRequests(true);
                    }
                });
            }
        }

        private static int _isUpdating = 0;

        public static async Task GetSongRequests(bool first = false)
        {
            if (Interlocked.Exchange(ref _isUpdating, 1) == 1) return;// It was already running.
            try
            {
                MessagingCenter.Send(new RefreshingMessage(true), "");
                await Task.Delay(5000);
                var result = await WpfComms.UnreadSongRequests(HostId);
                if (result.IsSuccess)
                {
                    List<RequestedSong> requests;
                    try
                    {
                        requests = result.Deserialise<List<RequestedSong>>();
                    }
                    catch (Exception)
                    {
                        return;
                    }

                    // Check for new.
                    var currentIds = Songs.Select(s => s.Id).ToList();
                    var newSongs = requests.Where(s => !currentIds.Contains(s.Id)).ToList();
                    // Do this in the proper thread
                    await Application.Current.Dispatcher.BeginInvoke(new Action(() => Songs.AddRange(newSongs)));
                    if (newSongs.Any())
                    {
                        Toaster.Toast($"You have received {newSongs.Count} new song requests!");
                    }
                    Save();
                }
            }
            finally
            {
                MessagingCenter.Send(new RefreshingMessage(false), "");
                Interlocked.Exchange(ref _isUpdating, 0);
            }
        }

        public static bool Init()
        {
            _receiver = new MessageReceiver();
            // Check if save file exists.
            if (!File.Exists(SaveFile))
            {
                // Check we can create it.
                try
                {
                    File.Create(SaveFile).Close();
                    Id = Guid.NewGuid().ToString();
                    Save();
                    return true;
                }
                catch
                {
                    // 
                    return false;
                }
            }

            // Read in data.
            var text = File.ReadAllText(SaveFile);
            if (text.Length > 0)
            {
                try
                {
                    var saveData = JsonConvert.DeserializeObject<SaveData>(text);
                    Songs = new ObservableCollection<RequestedSong>(saveData.Songs);
                    Token = DecryptToken(saveData.Token);
                }
                catch { /* Not worried about this, tbh. */ }
            }

            return true;
        }

        private static string DecryptToken(string token)
        {
            // TODO: actually do decryption
            return token;
        }

        public static bool Save()
        {
            var saveData = new SaveData {Songs = Songs.ToList(), Token = EncryptToken(Token), Id = Id};
            try
            {
                var text = JsonConvert.SerializeObject(saveData, Formatting.None);
                File.WriteAllText(SaveFile, text);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private static string EncryptToken(string token)
        {
            // TODO: actually do encryption
            return token;
        }

        public static bool ClearDone()
        {
            var doneSongs = Songs.Where(s => s.Marked).ToList();
            foreach (var d in doneSongs)
                Songs.Remove(d);

            return Save();
        }
    }
}
