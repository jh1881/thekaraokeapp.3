﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Newtonsoft.Json;
using TKADesktop.Annotations;
using TKADesktop.Helpers;

namespace TKADesktop.DataModel
{
    public class RequestedSong : INotifyPropertyChanged
    {
        private bool _cancelled;
        private bool _marked;
        public int Id { get; set; }
        public string Title { get; set; }
        public string Artist { get; set; }
        public string KeyChange { get; set; }
        public string TempoChange { get; set; }
        public string Requestor { get; set; }
        public string DuetSecond { get; set; }
        public DateTime RequestedTime { get; set; }
        [JsonConverter(typeof(JsonBoolConverter))]
        public bool Notified { get; set; }
        public string GroupNames { get; set; }
        public int GpsResult { get; set; }

        public bool HasGoodGps => GpsResult == 1;

        public bool Cancelled
        {
            get => _cancelled;
            set { _cancelled = value; OnPropertyChanged(); }
        }

        public bool Marked
        {
            get => _marked;
            set
            {
                _marked = value; OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
