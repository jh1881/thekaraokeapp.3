﻿using System;
using System.Collections;
using System.Globalization;
using System.Windows.Data;
using Windows.UI.Xaml;

namespace TKADesktop.Converters
{
    public class CollectionToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (parameter != null)
            {
                if (value is ICollection c)
                {
                    if (c.Count == 0) return Visibility.Visible;
                    return Visibility.Collapsed;
                }
            }
            else
            {
                if (value is ICollection c)
                {
                    if (c.Count == 0) return Visibility.Collapsed;
                    return Visibility.Visible;
                }
            }

            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
