﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using TKADesktop.Pages;
using TKADesktop.ViewModels;

namespace TKADesktop.Converters
{
    public class ApplicationPageValueConverter : BaseValueConverter<ApplicationPageValueConverter>
    {
        private static readonly Dictionary<ApplicationPage, Type> PageTypes = new Dictionary<ApplicationPage, Type>
        {
            {ApplicationPage.Login, typeof(LoginPage)},
            {ApplicationPage.Host, typeof(HostPage)},
        };

        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;
            if (PageTypes.TryGetValue((ApplicationPage)value, out var pageType))
            {
                var newPage = Activator.CreateInstance(pageType);
                if (newPage is FrameworkElement e && e.DataContext is INavigationPage navPage)
                {
                    navPage.OnNavigatedTo();
                }

                return newPage;
            }
            return null;
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
