﻿using System;
using System.Threading;
using System.Threading.Tasks;
using TKADesktop.DataModel;

namespace TKADesktop.Helpers
{
    public static class BackgroundThread
    {
        private static Task _backgroundTask;
        private static CancellationTokenSource _tokenSource;
        public static void Start()
        {
            if (_backgroundTask != null) return;
            _tokenSource = new CancellationTokenSource();
            var token = _tokenSource.Token;
            _backgroundTask = Task.Run(async () => await RunTask(token), token);
        }

        public static async Task Stop()
        {
            if (_backgroundTask == null) return;
            _tokenSource.Cancel();
            await _backgroundTask;
            _tokenSource.Dispose();
            _tokenSource = null;
        }

        private static async Task RunTask(CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                // Every minute, download any new song requests.
                await Task.Delay(TimeSpan.FromMinutes(1), token);
                if (token.IsCancellationRequested) break;

                await Database.GetSongRequests();
            }
        }
    }
}
