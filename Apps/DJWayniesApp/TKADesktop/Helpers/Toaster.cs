﻿using System.Windows;
using Windows.UI.Notifications;

namespace TKADesktop.Helpers
{
    public static class Toaster
    {
        public const string APP_ID = "TKA_APP";
        public static void Toast(string text)
        {
            var toastXml = ToastNotificationManager.GetTemplateContent(ToastTemplateType.ToastText01);
            var element = toastXml.GetElementsByTagName("text");
            element[0].AppendChild(toastXml.CreateTextNode(text));
            var toast = new ToastNotification(toastXml);
            toast.Activated += (sender, args) =>
            {
                Application.Current.Dispatcher.Invoke(() => Application.Current.MainWindow?.Activate());
            };
            ToastNotificationManager.CreateToastNotifier(APP_ID).Show(toast);
        }
    }
}
