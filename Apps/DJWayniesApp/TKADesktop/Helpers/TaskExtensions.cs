﻿using System;
using System.Threading.Tasks;

namespace TKADesktop.Helpers
{
    public interface IExceptionHandler
    {
        void HandleException(Exception ee);
    }

    public static class TaskExtensions
    {
        public static async void FireAndForgetSafeAsync(this Task task, IExceptionHandler exh = null)
        {
            try
            {
                await task;
            }
            catch (Exception ee)
            {
                exh?.HandleException(ee);
            }
        }
    }
}
