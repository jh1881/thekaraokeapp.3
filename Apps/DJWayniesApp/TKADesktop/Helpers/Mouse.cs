﻿using System.Windows;

namespace TKADesktop.Helpers
{
    public static class Mouse
    {
        public static Point GetMousePosition(Window parentWindow)
        {
            var pos = System.Windows.Input.Mouse.GetPosition(parentWindow);
            return new Point(pos.X + parentWindow.Left, pos.Y + parentWindow.Top);
        }
    }
}
