﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace TKADesktop.Helpers
{
    public class EventToCommand : TriggerAction<DependencyObject>
    {
        private static void HandleEventUpdate(DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            if (s is EventToCommand sender && sender.AssociatedObject != null)
                sender.EnableDisableElement();
        }

        public static readonly DependencyProperty CommandParameterProperty = DependencyProperty.Register(
            "CommandParameter", typeof(object), typeof(EventToCommand), new PropertyMetadata(null, HandleEventUpdate));

        public static readonly DependencyProperty CommandProperty = DependencyProperty.Register(
            "Command", typeof(ICommand), typeof(EventToCommand), new PropertyMetadata(null, (s, e) => OnCommandChanged(s as EventToCommand, e)));

        public static readonly DependencyProperty MustToggleIsEnabledProperty = DependencyProperty.Register(
            "MustToggleIsEnabled", typeof(bool), typeof(EventToCommand), new PropertyMetadata(false, HandleEventUpdate));

        public static readonly DependencyProperty AlwaysInvokeCommandProperty = DependencyProperty.Register(
            "AlwaysInvokeCommand", typeof(bool), typeof(EventToCommand), new PropertyMetadata(default(bool)));

        public bool AlwaysInvokeCommand
        {
            get => (bool) GetValue(AlwaysInvokeCommandProperty);
            set => SetValue(AlwaysInvokeCommandProperty, value);
        }

        public bool MustToggleIsEnabled
        {
            get => (bool) GetValue(MustToggleIsEnabledProperty);
            set => SetValue(MustToggleIsEnabledProperty, value);
        }

        public ICommand Command
        {
            get => (ICommand) GetValue(CommandProperty);
            set => SetValue(CommandProperty, value);
        }

        public object CommandParameter
        {
            get => (object) GetValue(CommandParameterProperty);
            set => SetValue(CommandParameterProperty, value);
        }

        private object _commandParameterValue;

        private bool? _mustToggleValue;

        public object CommandParameterValue
        {
            get => _commandParameterValue ?? CommandParameter;
            set
            {
                _commandParameterValue = value;
                EnableDisableElement();
            }
        }

        public bool MustToggleIsEnabledValue
        {
            get => _mustToggleValue ?? MustToggleIsEnabled;
            set
            {
                _mustToggleValue = value;
                EnableDisableElement();
            }
        }

        public bool PassEventArgsToCommand { get; set; }
//        public IEventArgsConverter EventArgsConverter { get; set; }


        protected override void OnAttached()
        {
            base.OnAttached();
            EnableDisableElement();
        }

        public void Invoke() => Invoke(null);

        protected override void Invoke(object parameter)
        {
            if (AssociatedElementIsDisabled() && !AlwaysInvokeCommand) return;
            var command = Command;
            var commandParameter = CommandParameterValue;
            if (commandParameter == null && PassEventArgsToCommand)
                commandParameter = parameter;

            if (command != null && command.CanExecute(commandParameter))
                command.Execute(commandParameter);
        }

        private void OnCommandCanExecuteChanged(object sender, EventArgs e) => EnableDisableElement();

        private static void OnCommandChanged(EventToCommand element, DependencyPropertyChangedEventArgs e)
        {
            if (element == null) return;

            if (e.OldValue != null)
                ((ICommand) e.OldValue).CanExecuteChanged -= element.OnCommandCanExecuteChanged;

            if (e.NewValue is ICommand command)
                command.CanExecuteChanged += element.OnCommandCanExecuteChanged;

            element.EnableDisableElement();
        }

        private FrameworkElement GetAssociatedObject() => AssociatedObject as FrameworkElement;

        private bool AssociatedElementIsDisabled()
        {
            if (AssociatedObject is FrameworkElement element) return !element.IsEnabled;
            return true;
        }

        private void EnableDisableElement()
        {
            var element = GetAssociatedObject();
            if (element == null)
                return;

            if (MustToggleIsEnabledValue && Command != null)
                element.IsEnabled = Command.CanExecute(CommandParameterValue);
        }
    }
}
