﻿using System;
using System.Windows.Input;

namespace TKADesktop.Helpers
{
    class RelayCommand : ICommand
    {
        private readonly Action<object> _execute;
        private readonly Func<object, bool> _canExecute;
        private readonly Action _noParamExecute;
        private readonly Func<bool> _noParamCanExecute;

        public event EventHandler CanExecuteChanged
        {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        }

        public RelayCommand(Action<object> execute, Func<object, bool> canExecute = null)
        {
            _execute = execute;
            _canExecute = canExecute;
        }

        public RelayCommand(Action execute, Func<bool> canExecute = null)
        {
            _noParamExecute = execute;
            _noParamCanExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            if (_noParamCanExecute != null) return _noParamCanExecute();
            return _canExecute == null || _canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            if (_noParamExecute != null) _noParamExecute();
            else _execute(parameter);
        }
    }
}
