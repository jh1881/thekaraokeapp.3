﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace TKADesktop.Controls
{
    public partial class LoginControl : UserControl
    {
        public static readonly DependencyProperty UsernameProperty = DependencyProperty.Register(
            "Username", typeof(string), typeof(LoginControl), new PropertyMetadata(default(string)));

        public static readonly DependencyProperty IsLoggingInProperty = DependencyProperty.Register(
            "IsLoggingIn", typeof(bool), typeof(LoginControl), new PropertyMetadata(default(bool), (o, args) =>
            {
                if (o is LoginControl l) l.UpdateIsLoggingIn();
            }));

        public bool IsLoggingIn
        {
            get
            {
                return (bool) GetValue(IsLoggingInProperty);
            }
            set => SetValue(IsLoggingInProperty, value);
        }

        public string Username
        {
            get => (string) GetValue(UsernameProperty);
            set => SetValue(UsernameProperty, value);
        }

        public LoginControl()
        {
            InitializeComponent();
        }

        public string Password => PasswordBox.Password;

        public class OnLoginEventArgs : EventArgs
        {
            public string Username { get; }
            public string Password { get; }

            public OnLoginEventArgs(string username, string password)
            {
                Username = username;
                Password = password;
            }
        }

        public event EventHandler<OnLoginEventArgs> OnLoginEvent;

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(Username) || string.IsNullOrEmpty(Password))
            {
                // No good!
                return;
            }

//            IsLoggingIn = true;
            OnLoginEvent?.Invoke(this, new OnLoginEventArgs(Username, Password));
        }

        private void UpdateIsLoggingIn()
        {
            LoginLabel.Content = IsLoggingIn ? "Please wait..." : "Login";
            IsEnabled = !IsLoggingIn;
        }
    }
}
