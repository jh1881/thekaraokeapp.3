﻿using System.Windows;
using System.Windows.Controls;

namespace TKADesktop.Controls
{
    //[TemplateVisualState(Name = InactiveState, GroupName = "ActiveStates")]
    //[TemplateVisualState(Name = ActiveState, GroupName = "ActiveStates")]
    //public class ProgressRing : Control
    //{
    //    public const string ActiveState = "Active";
    //    public const string InactiveState = "Inactive";

    //    #region DPs
    //    public static readonly DependencyProperty IsActiveProperty = DependencyProperty.Register(
    //        "IsActive", typeof(bool), typeof(ProgressRing), new PropertyMetadata(false, IsActiveChanged));

    //    public static readonly DependencyProperty MaxSideLengthProperty = DependencyProperty.Register(
    //        "MaxSideLength", typeof(double), typeof(ProgressRing), new PropertyMetadata(default(double)));

    //    public static readonly DependencyProperty EllipseDiameterProperty = DependencyProperty.Register(
    //        "EllipseDiameter", typeof(double), typeof(ProgressRing), new PropertyMetadata(default(double)));

    //    public static readonly DependencyProperty EllipseOffsetProperty = DependencyProperty.Register(
    //        "EllipseOffset", typeof(Thickness), typeof(ProgressRing), new PropertyMetadata(new Thickness(0, 7, 0, 0)));

    //    public Thickness EllipseOffset
    //    {
    //        get => (Thickness) GetValue(EllipseOffsetProperty);
    //        set => SetValue(EllipseOffsetProperty, value);
    //    }

    //    public double EllipseDiameter
    //    {
    //        get => (double) GetValue(EllipseDiameterProperty);
    //        set => SetValue(EllipseDiameterProperty, value);
    //    }

    //    public double MaxSideLength
    //    {
    //        get => (double) GetValue(MaxSideLengthProperty);
    //        set => SetValue(MaxSideLengthProperty, value);
    //    }

    //    public bool IsActive
    //    {
    //        get => (bool) GetValue(IsActiveProperty);
    //        set => SetValue(IsActiveProperty, value);
    //    }

    //    private static void IsActiveChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    //    {
    //        if (d is ProgressRing ring)
    //            ring.UpdateActiveState();
    //    }
    //    #endregion

    //    public ProgressRing()
    //    {
    //        SizeChanged += OnSizeChanged;
    //    }

    //    private void OnSizeChanged(object sender, SizeChangedEventArgs e)
    //    {
    //        UpdateMaxSideLength();
    //    }

    //    private void UpdateMaxSideLength()
    //    {
    //        if (ActualWidth <= 30) MaxSideLength = 25;
    //        else MaxSideLength = ActualWidth - 5;
    //    }

    //    private void UpdateActiveState()
    //    {
    //        VisualStateManager.GoToState(this, IsActive ? ActiveState : InactiveState, true);
    //    }

    //    private void UpdateEllipseDiameter()
    //    {
    //        if (ActualWidth <= 25) EllipseDiameter = 3;
    //        else EllipseDiameter = (ActualWidth / 10) + 0.5;
    //    }

    //    private void UpdateEllipseOffset()
    //    {
    //        if (ActualWidth <= 25)
    //        {
    //            EllipseOffset = new Thickness(0, 7, 0, 0);
    //        }
    //        else if (ActualWidth <= 30)
    //        {
    //            var top = ActualWidth * 0.45 - 4.5;
    //            EllipseOffset = new Thickness(0, top, 0, 0);
    //        }
    //        else
    //        {
    //            var top = ActualWidth * 0.4 - 2;
    //            EllipseOffset = new Thickness(0, top, 0, 0);
    //        }
    //    }
    //}
}
