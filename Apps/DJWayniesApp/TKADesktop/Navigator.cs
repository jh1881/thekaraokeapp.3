﻿using System.Windows.Controls;
using TKADesktop.Pages;

namespace TKADesktop
{
    public interface INavigator
    {
        void NavigateTo(ApplicationPage page);
    }
}
