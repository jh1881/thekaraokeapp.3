﻿using TKAShared.DataModel;
using Xamarin.Forms;

namespace TKAShared.CustomCells
{
    public class LastSongRequestedCell : ViewCell
    {
        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();
            var model = BindingContext as RequestedSong;
            if (model == null) return;


            var grid = new Grid
            {
                ColumnDefinitions = new ColumnDefinitionCollection
                {
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Auto) },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Auto) },
                },
                Padding = new Thickness(10, 10, 10, 10)
            };

            var titleView = new Label
            {
                FontSize = 12,
                Text = model.Song.Title
            };
            grid.Children.Add(titleView, 0, 0);

            var artistView = new Label
            {
                FontSize = 12,
                Text = model.Song.Artist,
                LineBreakMode = LineBreakMode.WordWrap
            };
            grid.Children.Add(artistView, 1, 0);

            View = grid;
            Height = View.Height;
        }
    }
}
