﻿using Xamarin.Forms;

namespace DJWayniesApp.CustomCells
{
    public class SongRequestCell : ViewCell
    {
        public SongRequestCell()
        {
            var grid = new Grid();
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto });
            grid.ColumnDefinitions.Add(new ColumnDefinition());

            var markedCheck = GetCheckboxLayout("Marked", Color.White, "Marked");
            grid.Children.Add(markedCheck, 0, 0);

            var rect = new BoxView { Color = new Color(0.5, 0.5, 0.5, 0.5)};
            grid.Children.Add(rect, 1, 0);
            rect.SetBinding(VisualElement.IsVisibleProperty, "Cancelled", BindingMode.OneWay);

            var scroll = new ScrollView { Orientation = ScrollOrientation.Horizontal };

            var stackLayout = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                Children =
                {
                    GetStackLayout("Time", "ShortTime", Color.White, Color.White),
                    GetStackLayout("Name", "Requestor", Color.White, Color.White),
                    GetStackLayout("Duet Name", "DuetSecond", Color.White, Color.White, "DuetVisible"),
                    GetStackLayout("Group Names", "GroupNames", Color.White, Color.White, "GroupVisible"),
                    GetStackLayout("Title", "Title", Color.White, Color.White),
                    GetStackLayout("Artist", "Artist", Color.White, Color.White),
                    GetStackLayout("Key Change", "KeyChange", Color.Red, Color.Red, "KeyChangeVisible"),
                    GetStackLayout("Tempo Change", "TempoChange", Color.Red, Color.Red, "TempoChangeVisible"),
                    GetCheckboxLayout("Cancelled", Color.White, "Cancelled", false)
                }
            };
            scroll.Content = stackLayout;
            grid.Children.Add(scroll, 1, 0);
            //var rect = new BoxView { Color = new Color(0.5, 0.5, 0.5, 0.5)};
            //grid.Children.Add(rect, 1, 0);
            //grid.SetBinding(VisualElement.IsVisibleProperty, "Marked", BindingMode.OneWay);

            this.View = grid;
        }

        private static StackLayout GetCheckboxLayout(string title, Color titleColor, string binding = null, bool isEnabled = true, string isVisibleBinding = null)
        {
            var stackLayout = new StackLayout
            {
                Orientation = StackOrientation.Vertical,
                Padding = new Thickness(10),
                Children = { new Label { Text = title, FontAttributes = FontAttributes.Bold, TextColor = titleColor } }
            };
            var checkbox = new Switch { IsEnabled = isEnabled };
            if (binding != null)
                checkbox.SetBinding(Switch.IsToggledProperty, binding);
            stackLayout.Children.Add(checkbox);

            if (!string.IsNullOrEmpty(isVisibleBinding))
                stackLayout.SetBinding(VisualElement.IsVisibleProperty, isVisibleBinding);

            return stackLayout;
        }

        private static StackLayout GetStackLayout(string title, string binding, Color textColor, Color titleColor, string isVisibleBinding = null, string bindingFormat = null)
        {
            var stackLayout = new StackLayout
                {
                    Orientation = StackOrientation.Vertical,
                    Padding = new Thickness(10),
                    Children = { new Label { Text = title, FontAttributes = FontAttributes.Bold, TextColor = titleColor } }
                };
            var label = new Label { TextColor = textColor };
            label.SetBinding(Label.TextProperty, binding, BindingMode.Default, null, bindingFormat);
            stackLayout.Children.Add(label);

            if (!string.IsNullOrEmpty(isVisibleBinding))
                stackLayout.SetBinding(VisualElement.IsVisibleProperty, isVisibleBinding);

            return stackLayout;
        }
    }
}
