﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Plugin.Connectivity;
using TKAShared.Interfaces;

namespace TKAShared
{
    public interface ICommsDataProvider
    {
        string HostId { get; }
        Guid Uid { get; }
    }

    public class Comms : IComms
    {
        private readonly IAnalytics _analytics;
        private readonly ICommsDataProvider _dataProvider;

        public class CommsResult
        {
            public HttpStatusCode StatusCode { get; set; } = HttpStatusCode.OK;
            public string RawResponse { get; set; }
            public bool IsSuccess { get; set; }
            public bool NoConnection { get; set; }
            public Exception Exception { get; set; }

            public T Deserialise<T>()
            {
                return JsonConvert.DeserializeObject<T>(RawResponse);
            }

            public object Deserialise(Type targetType)
            {
                return JsonConvert.DeserializeObject(RawResponse, targetType);
            }

            public override string ToString()
            {
                return (Exception == null ? $"{StatusCode} {RawResponse}" : $"Exception: {Exception}");
            }
        }

        private const string ProcessAmiahost = "AM";
        private const string ProcessChooseHost = "HO";
        private const string ProcessSongList = "SN";
        private const string ProcessRequestSong = "RQ";
        private const string ProcessRequestPush = "PU";
        private const string ProcessStartShow = "ST";
        private const string ProcessStopShow = "SP";
        private const string ProcessSongRequests = "UR";
        private const string ProcessCheckShowStatus = "CS";

        private const string BaseUrl = "http://thekaraokeapp.co.uk";

        private string _facebookToken;

        public string FacebookToken
        {
            get => _facebookToken;
            set { _facebookToken = value; OnFacebookTokenChanged?.Invoke(null); }
        }

        public event EventHandler OnFacebookTokenChanged;

        public Comms(IAnalytics analytics, ICommsDataProvider dataProvider)
        {
            _analytics = analytics;
            _dataProvider = dataProvider;
        }

        public bool IsConnected()
        {
            var isConnected = CrossConnectivity.Current.IsConnected;
            Debug.WriteLine($"Comms.IsConnected said {isConnected}");
            return isConnected;
        }

        public async Task<CommsResult> CallApi(string url)
        {
            Debug.WriteLine($"Comms.CallApi asked {url}");
            _analytics?.Track("Comms.CallApi", "url", url);
            if (!IsConnected())
            {
                Debug.WriteLine($"Comms.CallApi({url}) - no connection found");
                return new CommsResult { IsSuccess = false, NoConnection = true };
            }
            using (var client = new HttpClient())
            {
                var result = new CommsResult();
                HttpResponseMessage response = null;
                try
                {
                    response = await client.GetAsync(url);
                    result.RawResponse = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode) result.IsSuccess = true;
                    else result.StatusCode = response.StatusCode;
                }
                catch (Exception e)
                {
                    _analytics?.ReportException(e);
                    result.IsSuccess = false;
                    result.StatusCode = response?.StatusCode ?? HttpStatusCode.Unused;
                    result.RawResponse = $"Exception: {e.Message}";
                    result.Exception = e;
                    return result;
                }
                Debug.WriteLine($"Comms.CallApi({url}) got {result}");
                return result;
            }
        }

        public string GetProcessUrl(string operation, string parameters = null)
        {
            var url = $"{BaseUrl}/process.php?op={operation}&uid={_dataProvider.Uid}";
            if (!string.IsNullOrEmpty(FacebookToken))
                url += $"&fbtoken={FacebookToken}";
            if (parameters != null) url += $"&{parameters}";
            return url;
        }

        public async Task<CommsResult> ChooseHost(string number)
        {
            Debug.WriteLine("Comms.ChooseHost started");
            var url = GetProcessUrl(ProcessChooseHost, $"number={number}");
            try
            {
                var result = await CallApi(url);
                Debug.WriteLine($"Comms.ChooseHost ended with result {result}");
                return result;
            }
            catch (Exception e)
            {
                Debug.WriteLine($"Comms.ChooseHost exception {e}");
                _analytics?.ReportException(e);
                return new CommsResult { IsSuccess = false, Exception = e };
            }
        }

        public async Task<CommsResult> AmIaHost(string fbid = null)
        {
            var url = (fbid == null ? GetProcessUrl(ProcessAmiahost) : GetProcessUrl(ProcessAmiahost, $"facebookid={fbid}"));
            var value = await CallApi(url);
            return value;
        }

        public async Task<CommsResult> SongListCount(string hostId = null)
        {
            if (hostId == null)
                hostId = _dataProvider.HostId;
            var url = GetProcessUrl(ProcessSongList, $"hostid={hostId}&count=");
            var returnValue = await CallApi(url);
            Debug.WriteLine($"Comms.SongListCount got {returnValue}");
            return returnValue;
        }

        public async Task<CommsResult> SongList(string hostId = null, bool exactMatch = false, string search = null, int firstRow = 0, bool orderByTitle = false, bool getCount = false)
        {
            if (hostId == null)
                hostId = _dataProvider.HostId;
            var url = GetProcessUrl(ProcessSongList, $"hostid={hostId}&limit={firstRow}");
            if (string.IsNullOrEmpty(search) == false)
            {
                url += $"&search={WebUtility.UrlEncode(search)}";
            }
            if (orderByTitle) url += "&orderbytitle=";
            if (exactMatch) url += "&exactmatch=";
            if (getCount) url += "&count=";
            var returnValue = await CallApi(url);
            Debug.WriteLine($"Comms.SongList asked {url} and got {returnValue}");
            return returnValue;
        }

        public async Task<RequestSongResult> RequestSong(string title, string artist, string hostId, string requestor, string keyChange = null, string tempoChange = null, string duetSecond = null, string groupNames = null)
        {
            Debug.WriteLine($"Comms.RequestSong({title}, {artist}, {hostId}, {requestor}, {keyChange ?? "null"}, {tempoChange ?? "null"}, {duetSecond ?? "null"}, {groupNames ?? "null"})");
            var sb = new StringBuilder($"title={WebUtility.UrlEncode(title)}&artist={WebUtility.UrlEncode(artist)}&requestor={WebUtility.UrlEncode(requestor)}&hostid={hostId}");
            if (string.IsNullOrEmpty(keyChange) == false) sb.AppendFormat("&keychange={0}", WebUtility.UrlEncode(keyChange));
            if (string.IsNullOrEmpty(tempoChange) == false) sb.AppendFormat("&tempochange={0}", WebUtility.UrlEncode(tempoChange));
            if (string.IsNullOrEmpty(duetSecond) == false) sb.AppendFormat("&duetsecond={0}", WebUtility.UrlEncode(duetSecond));
            if (string.IsNullOrEmpty(groupNames) == false) sb.AppendFormat("&groupnames={0}", WebUtility.UrlEncode(groupNames));

            var url = GetProcessUrl(ProcessRequestSong, sb.ToString());
            Debug.WriteLine($"Comms.RequestSong url = {url}");

            var result = await CallApi(url);
            var songResult = RequestSongResult.Success;
            if (result.IsSuccess == false)
            {
                if (result.NoConnection) return RequestSongResult.NoConnection;
                switch (result.StatusCode)
                {
                    case HttpStatusCode.InternalServerError: songResult = RequestSongResult.ServerError; break;
                    case HttpStatusCode.BadRequest: songResult = RequestSongResult.BadRequest; break;
                    case HttpStatusCode.NotFound: songResult = RequestSongResult.HostNotFound; break;
                    case HttpStatusCode.Forbidden: songResult = RequestSongResult.ShowNotInProgress; break;
                    default: songResult = RequestSongResult.UnknownError; break;
                }
            }
            Debug.WriteLine($"Comms.RequestSong ended with {songResult}");
            return songResult;
        }

        public async Task<bool> SendPushNotificationToken(string deviceId, DeviceType deviceType)
        {
            var parameters = $"deviceid={deviceId}&devicetype=";
            switch (deviceType)
            {
                case DeviceType.Ios: parameters += "1"; break;
                case DeviceType.Android: parameters += "2"; break;
                case DeviceType.Windows: parameters += "3"; break;
            }
            var url = GetProcessUrl(ProcessRequestPush, parameters);
            var result = await CallApi(url);
            return result.IsSuccess;
        }

        public async Task<CommsResult> StartShow()
        {
            var url = GetProcessUrl(ProcessStartShow);
            return await CallApi(url);
        }

        public async Task<CommsResult> StopShow()
        {
            var url = GetProcessUrl(ProcessStopShow);
            return await CallApi(url);
        }

        public async Task<CommsResult> UnreadSongRequests(string hostId = null)
        {
            var url = GetProcessUrl(ProcessSongRequests, (hostId != null ? $"hostid={hostId}" : null));
            var response = await CallApi(url);
            return response;
        }

        public async Task<CommsResult> GetShowStatus()
        {
            var url = GetProcessUrl(ProcessCheckShowStatus);
            var response = await CallApi(url);
            return response;
        }
    }
}
