﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace TKAShared
{
    public static class Utils
    {
        public static Task BeginInvokeOnMainThreadAsync(Action action)
        {
            Debug.WriteLine($"App.BeginInvokeOnMainThreadAsync started");
            var tcs = new TaskCompletionSource<object>();
            Device.BeginInvokeOnMainThread(() =>
            {
                try
                {
                    action();
                    Debug.WriteLine($"App.BeginInvokeOnMainThreadAsync action completed without error");
                    tcs.SetResult(null);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine($"App.BeginInvokeOnMainThreadAsync action completed with exception {ex}");
                    tcs.SetException(ex);
                }
            });
            return tcs.Task;
        }
    }
}
