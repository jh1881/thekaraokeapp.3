﻿namespace TKAShared
{
    public enum PageType
    {
        Other,
        SongList,
        Settings,
        RequestSong,
    }

    public enum RequestSongResult
    {
        Success,
        ServerError,
        ShowNotInProgress,
        HostNotFound,
        BadRequest,
        NoConnection,
        UnknownError
    }

    public enum DeviceType
    {
        Ios = 1,
        Android = 2,
        Windows = 3
    }

    public enum AnalyticsSeverity
    {
        Warning,
        Error,
        Critical
    }
}
