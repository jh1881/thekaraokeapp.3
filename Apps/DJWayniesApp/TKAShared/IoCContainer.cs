﻿using Autofac;

namespace TKAShared
{
    public static class IoCContainer
    {
        public static IContainer Container { get; set; }

        public static ILifetimeScope BeginLifetimeScope()
        {
            return Container.BeginLifetimeScope();
        }

        public static T Resolve<T>()
        {
            return Container.Resolve<T>();
        }
    }
}
