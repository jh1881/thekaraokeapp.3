﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Acr.UserDialogs;
using DJWayniesApp.Interfaces;
using TKAShared.DataModel;
using TKAShared.Interfaces;
using TKAShared.Messages;
using Xamarin.Forms;

namespace TKAShared
{
    public class SongRequestStorage : ISongRequestStorage
    {
        private readonly IComms _comms;
        private readonly IUserDialogs _userDialogs;

        public class SongRequestsReceivedArgs : EventArgs
        {
            public int NewSongRequestsReceived { get; }

            public SongRequestsReceivedArgs(int newSongRequestsReceived)
            {
                NewSongRequestsReceived = newSongRequestsReceived;
            }
        }

        public ObservableCollection<SongRequest> SongRequests { get; private set; } = new ObservableCollection<SongRequest>();

        public event EventHandler<SongRequestsReceivedArgs> OnNewSongRequestsReceived;

        private Func<List<SongRequest>> _requestsGetter;
        private Action<List<SongRequest>> _requestsSetter;

        public SongRequestStorage(IComms comms, IUserDialogs userDialogs)
        {
            _comms = comms;
            _userDialogs = userDialogs;
        }

        // On Init, we get the current song requests.
        // We then allow other tasks (such as the background task) to add new songs or update
        //   existing songs.
        public async Task Init(Func<List<SongRequest>> requestsGetter, Action<List<SongRequest>> requestsSetter)
        {
            Debug.WriteLine("SongRequestStorage.Init started");
            try
            {
                _requestsGetter = requestsGetter;
                _requestsSetter = requestsSetter;

                // Load the current set from file.
                var requests = (requestsGetter.Invoke() ?? new List<SongRequest>())
                    .Where(s => s.RequestedTime.Date < DateTime.Today);
                SongRequests = new ObservableCollection<SongRequest>(requests);
                requestsSetter(SongRequests.ToList());
                Debug.WriteLine($"SongRequestStorage.Init has {SongRequests.Count} current song requests");

                // Now update the requests.
                await Update();
            }
            catch (Exception e)
            {
                Debug.WriteLine($"SongRequestStorage.Init got exception: {e}");
                MessagingCenter.Send(new ErrorMessage { ErrorText = $"Exception: {e.Message}" }, "");
            }
            Debug.WriteLine("SongRequestStorage.Init ended");
        }

        public void Clear()
        {
            SongRequests.Clear();
            _requestsSetter(new List<SongRequest>());
        }

        private bool _isUpdating;

        public void SaveChanges()
        {
            _requestsSetter(SongRequests.ToList());
        }

        // Returns the number of new songs.
        public async Task<int> Update()
        {
            Debug.WriteLine("SongRequestStorage.Update started");
            if (_isUpdating) return 0;
            var newSongCount = 0;
            _isUpdating = true;
            try
            {
                var result = await _comms.UnreadSongRequests();
                Debug.WriteLine($"SongRequestStorage.Update UnreadNotifications got {result}");
                if (result.IsSuccess)
                {
                    List<SongRequest> requests;
                    try
                    {
                        requests = result.Deserialise<List<SongRequest>>();
                    }
                    catch (Exception)
                    {
                        MessagingCenter.Send(new ErrorMessage { ErrorText = "Failed to contact host server. Please check your internet connection and try again later." }, "");
                        newSongCount = -1;
                        return newSongCount;
                    }

                    DependencyService.Get<IPushNotifications>()?.ClearBadge();

                    await Utils.BeginInvokeOnMainThreadAsync(() =>
                    {
                        foreach (var existingSong in requests)
                        {
                            var song = SongRequests.FirstOrDefault(s => s.Id == existingSong.Id);
                            if (song != null)
                            {
                                song.Cancelled = existingSong.Cancelled;
                            }
                            else
                            {
                                SongRequests.Add(existingSong);
                                newSongCount++;
                            }
                        }
                    });
                    if (newSongCount > 0)
                    {
                        UserDialogs.Instance.Toast("New song requests received");
//                        DependencyService.Get<IToast>()?.ShowToast("New song requests received", 250);
                        OnNewSongRequestsReceived?.Invoke(null, new SongRequestsReceivedArgs(newSongCount));
                    }
                    _requestsSetter(SongRequests.ToList());
                    //Helpers.Settings.CurrentSongRequests = SongRequests.ToList();
                }
                else
                {
                    HandleNonSuccess(result);
                    newSongCount = -1;
                }
            }
            finally
            {
                _isUpdating = false;
            }
            Debug.WriteLine($"SongRequestStorage.Update ended with newSongCount = {newSongCount}");
            return newSongCount;
        }

        public void HandleNonSuccess(Comms.CommsResult result)
        {
            string errorText;
            if (result.NoConnection)
                errorText = "No connection available.";
            else if (result.Exception != null)
                errorText = $"Exception: {result.Exception.Message}";
            else
                errorText = $"Error: {result.StatusCode} {result.RawResponse}";
            MessagingCenter.Send(new ErrorMessage { ErrorText = errorText }, "");
        }
    }
}
