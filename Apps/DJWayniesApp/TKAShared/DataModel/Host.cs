﻿namespace TKAShared.DataModel
{
    public class Host
    {
        public string Name { get; set; }
        public string Id { get; set; }
    }
}