﻿using System;

namespace TKAShared.DataModel
{
    public class RequestedSong
    {
        public Song Song { get; set; }
        public DateTime TimeRequested { get; set; }
        public string KeyChange { get; set; }
        public string TempoChange { get; set; }
    }
}
