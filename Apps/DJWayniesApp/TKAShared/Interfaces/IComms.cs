﻿using System;
using System.Threading.Tasks;

namespace TKAShared.Interfaces
{
    public interface IComms
    {
        string FacebookToken { get; set; }
        event EventHandler OnFacebookTokenChanged;
        bool IsConnected();
        Task<Comms.CommsResult> CallApi(string url);
        string GetProcessUrl(string operation, string parameters = null);
        Task<Comms.CommsResult> ChooseHost(string number);
        Task<Comms.CommsResult> AmIaHost(string fbid = null);
        Task<Comms.CommsResult> SongListCount(string hostId = null);
        Task<Comms.CommsResult> SongList(string hostId = null, bool exactMatch = false, string search = null, int firstRow = 0, bool orderByTitle = false, bool getCount = false);
        Task<RequestSongResult> RequestSong(string title, string artist, string hostId, string requestor, string keyChange = null, string tempoChange = null, string duetSecond = null, string groupNames = null);
        Task<bool> SendPushNotificationToken(string deviceId, DeviceType deviceType);
        Task<Comms.CommsResult> StartShow();
        Task<Comms.CommsResult> StopShow();
        Task<Comms.CommsResult> UnreadSongRequests(string hostId = null);
        Task<Comms.CommsResult> GetShowStatus();
    }
}
