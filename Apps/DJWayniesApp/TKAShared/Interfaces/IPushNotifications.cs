﻿namespace DJWayniesApp.Interfaces
{
    public interface IPushNotifications
    {
        void Begin();
        void End();

        void ClearBadge();
    }
}
