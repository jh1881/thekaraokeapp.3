using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using TKAShared.DataModel;

namespace TKAShared.Interfaces
{
    public interface ISongRequestStorage
    {
        ObservableCollection<SongRequest> SongRequests { get; }
        event EventHandler<SongRequestStorage.SongRequestsReceivedArgs> OnNewSongRequestsReceived;
        Task Init(Func<List<SongRequest>> requestsGetter, Action<List<SongRequest>> requestsSetter);
        void Clear();
        void SaveChanges();
        Task<int> Update();
        void HandleNonSuccess(Comms.CommsResult result);
    }
}
