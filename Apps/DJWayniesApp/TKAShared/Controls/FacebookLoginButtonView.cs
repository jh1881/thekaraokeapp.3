﻿using TKAShared.Messages;
using Xamarin.Forms;

namespace TKAShared.Controls
{
    public class FacebookLoginButtonView : View
    {
        public void LoggedOut()
        {
            MessagingCenter.Send(new FacebookLoggedOutMessage(), "");
        }

        public void LoggedIn(string token)
        {
            MessagingCenter.Send(new FacebookLoggedInMessage(token), "");
        }

        public void Error(string text)
        {
            MessagingCenter.Send(new ErrorMessage { ErrorText = text }, "");
        }
    }
}
