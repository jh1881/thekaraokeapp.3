﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace TKAShared.Controls
{
    public class BindableRadioGroup : Grid
    {
        public static BindableProperty ItemsSourceProperty = BindableProperty.Create("ItemsSource", typeof(IEnumerable), typeof(BindableRadioGroup),
                default(IEnumerable), propertyChanged: OnItemsSourceChanged);

        public static BindableProperty SelectedIndexProperty = BindableProperty.Create("SelectedIndex", typeof(int), typeof(BindableRadioGroup),
                default(int), BindingMode.TwoWay, propertyChanged: OnSelectedIndexChanged);

        public static BindableProperty OrientationProperty = BindableProperty.Create("Orientation", typeof(StackOrientation), typeof(BindableRadioGroup),
                default(StackOrientation), BindingMode.TwoWay, propertyChanged: OnOrientationChanged);

        public IEnumerable ItemsSource
        {
            get => (IEnumerable)GetValue(ItemsSourceProperty);
            set => SetValue(ItemsSourceProperty, value);
        }

        public int SelectedIndex
        {
            get => (int)GetValue(SelectedIndexProperty);
            set => SetValue(SelectedIndexProperty, value);
        }

        public StackOrientation Orientation
        {
            get => (StackOrientation)GetValue(OrientationProperty);
            set => SetValue(OrientationProperty, value);
        }

        private readonly List<CustomRadioButton> _rads = new List<CustomRadioButton>();
        public event EventHandler<int> CheckedChanged;

        private static void OnItemsSourceChanged(BindableObject bindable, object oldvalue, object newvalue)
        {
            var radButtons = bindable as BindableRadioGroup;
            radButtons?.SetupRadioButtons(newvalue as IEnumerable);
        }

        private void OnCheckedChanged(object sender, EventArgs<bool> e)
        {
            if (e.Value == false) return;

            var selectedRad = sender as CustomRadioButton;
            if (selectedRad == null) return;

            foreach (var rad in _rads)
            {
                if (!selectedRad.RadioId.Equals(rad.RadioId)) rad.Checked = false;
                else
                {
                    CheckedChanged?.Invoke(sender, rad.RadioId);
                    SelectedIndex = rad.RadioId;    // Allows Binding
                }
            }
        }

        private static void OnSelectedIndexChanged(BindableObject bindable, object oldValue, object newValue)
        {
            int iNewValue = (int)newValue;
            if (iNewValue == -1) return;

            var bindableRadioGroup = bindable as BindableRadioGroup;
            if (bindableRadioGroup == null) return;

            foreach (var rad in bindableRadioGroup._rads.Where(rad => rad.RadioId == bindableRadioGroup.SelectedIndex))
            {
                rad.Checked = true;
            }
        }

        private static void OnOrientationChanged(BindableObject bindable, object oldvalue, object newvalue)
        {
            var radButtons = bindable as BindableRadioGroup;
            if (radButtons?.ItemsSource != null)
                radButtons.SetupRadioButtons(radButtons.ItemsSource);
        }

        private void SetupRadioButtons(IEnumerable items)
        {
            _rads.Clear();
            Children.Clear();
            var columns = Orientation == StackOrientation.Horizontal;
            if (items != null)
            {
                var radIndex = 0;
                var selectedIndex = SelectedIndex;
                foreach (var item in items)
                {
                    var rad = new CustomRadioButton
                        {
                            Text = item.ToString(),
                            RadioId = radIndex,
                            Checked = radIndex == selectedIndex,
                        };
                    radIndex++;
                    if (columns)
                    {
                        // Make a column
                        var col = new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) };
                        ColumnDefinitions.Add(col);
                    }
                    else
                    {
                        // Make a row
                        var row = new RowDefinition { Height = GridLength.Auto };
                        RowDefinitions.Add(row);
                    }

                    rad.CheckedChanged += OnCheckedChanged;
                    _rads.Add(rad);
                }
                foreach (var rad in _rads)
                {
                    var number = rad.RadioId;
                    if (columns) Children.Add(rad, number, 0);
                    else Children.Add(rad, 0, number);
                }
            }
        }
    }
}
