﻿namespace TKAShared.Messages
{
    public class HostChangePageMessage
    {
        public PageType Page { get; set; }
    }
}
