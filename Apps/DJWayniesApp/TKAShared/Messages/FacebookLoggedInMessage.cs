﻿namespace TKAShared.Messages
{
    public class FacebookLoggedInMessage
    {
        public string Token { get; }

        public FacebookLoggedInMessage(string token)
        {
            Token = token;
        }
    }
}
