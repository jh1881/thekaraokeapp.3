﻿using Xamarin.Forms;
using DJWayniesApp.iOS.Services;
using DJWayniesApp.Interfaces;
using UIKit;

[assembly: Dependency(typeof(IosPushNotifications))]

namespace DJWayniesApp.iOS.Services
{
    public class IosPushNotifications : IPushNotifications
    {
        public void Begin()
        {
            Device.BeginInvokeOnMainThread(() => UIApplication.SharedApplication.RegisterForRemoteNotifications());
        }

        public void End()
        {
            Device.BeginInvokeOnMainThread(() => UIApplication.SharedApplication.UnregisterForRemoteNotifications());
        }

        public void ClearBadge()
        {
            Device.BeginInvokeOnMainThread(() => UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0);
        }
    }
}
