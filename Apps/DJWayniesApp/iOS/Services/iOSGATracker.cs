﻿using System;
using Xamarin.Forms;
using DJWayniesApp.iOS;
using Google.Analytics;
using Foundation;

[assembly:Dependency(typeof(iOSGATracker))]
namespace DJWayniesApp.iOS
{
    public class iOSGATracker : IGAService
    {
        private ITracker _tracker;
        const string TrackingId = "";
        const string AllowTrackingKey = "AllowTracking";

        public void InitGAS()
        {
            var optionsDict = NSDictionary.FromObjectAndKey(new NSString("YES"), new NSString(AllowTrackingKey));
            NSUserDefaults.StandardUserDefaults.RegisterDefaults(optionsDict);

            Gai.SharedInstance.OptOut = !NSUserDefaults.StandardUserDefaults.BoolForKey(AllowTrackingKey);

            Gai.SharedInstance.DispatchInterval = 10;
            Gai.SharedInstance.TrackUncaughtExceptions = true;

#if DEBUG
            Gai.SharedInstance.DryRun = true;
#endif

            Tracker = Gai.SharedInstance.GetTracker("Test App", TrackingId);
        }

        #region IGAService implementation
        public void TrackPage(string pageName)
        {
            Gai.SharedInstance.DefaultTracker.Set(GaiConstants.ScreenName, pageName);
            Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateScreenView().Build());        
        }

        public void TrackEvent(string eventCategory, string eventName)
        {
            Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateEvent(eventCategory, eventName, "AppEvent", null).Build());
            Gai.SharedInstance.Dispatch();     
        }

        public void TrackException(string exceptionMessage, bool isFatal)
        {
            Gai.SharedInstance.DefaultTracker.Send(DictionaryBuilder.CreateException(exceptionMessage, isFatal).Build());
        }
        #endregion
    }
}
