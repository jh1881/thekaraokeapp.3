﻿using System;
using DJWayniesApp.Helpers;
using DJWayniesApp.iOS.Services;
using DJWayniesApp.Interfaces;
using Foundation;
using Google.MobileAds;
using UIKit;
using Xamarin.Forms;

[assembly:Dependency(typeof(IosAdService))]
namespace DJWayniesApp.iOS.Services
{
    class IosAdService : IAdService
    {
        internal const string BannerAdUnitId = "ca-app-pub-8064175537251762/7121828858";
        internal const string InterstitialAdUnitId = "ca-app-pub-8064175537251762/5613306064";

        private Interstitial _interstitialAd;

        public IosAdService()
        {
            LoadAd();
        }

        public void ShowInterstitialAd()
        {
            if (_interstitialAd.IsReady)
            {
                _interstitialAd.ScreenDismissed += (sender, args) => LoadAd();
                var vc = UIApplication.SharedApplication.KeyWindow.RootViewController;
                _interstitialAd.PresentFromRootViewController(vc);
            }
        }

        private void LoadAd()
        {
            _interstitialAd = new Interstitial(InterstitialAdUnitId);
            _interstitialAd.LoadRequest(GetAdRequest());
        }

        internal static Request GetAdRequest()
        {
            var r = Request.GetDefaultRequest();
            if (Settings.NonPersonalisedAds)
            {
                try
                {
                    var extras = new Extras
                    {
                        AdditionalParameters = NSDictionary.FromObjectAndKey(new NSString("1"), new NSString("npa"))
                    };
                    r.RegisterAdNetworkExtras(extras);
                }
                catch (Exception ee)
                {

                }
            }

            return r;
        }
    }
}
