using DJWayniesApp.iOS.Services;
using TheKaraokeApp.Core.Interfaces;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(OsLoginOutService))]

namespace DJWayniesApp.iOS.Services
{
    public class OsLoginOutService : ILoginOutService
    {
        public void LoggedOut()
        {
            Device.BeginInvokeOnMainThread(() => UIApplication.SharedApplication.SetStatusBarStyle(UIStatusBarStyle.Default, true));
        }

        public void LoggedIn(string token)
        {
            Device.BeginInvokeOnMainThread(() => UIApplication.SharedApplication.SetStatusBarStyle(UIStatusBarStyle.LightContent, true));
        }
    }
}
