﻿using System;
using System.Diagnostics;
using Acr.UserDialogs;
using Foundation;
using UIKit;
using UserNotifications;
using Xamarin.Forms;
using Xamarin.Forms.Themes;
using Xamarin.Forms.Themes.iOS;

namespace DJWayniesApp.iOS
{
    [Register("AppDelegate")]
    public class AppDelegate : Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        private App _app;
        
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            Google.MobileAds.MobileAds.Configure("ca-app-pub-8064175537251762~1714138242");
            Forms.Init();
            if (options != null && options.ContainsKey(UIApplication.LaunchOptionsRemoteNotificationKey))
            {
                // We opened by tapping the notification.
            }

            _app = new App();

            _app.OnStartBackgrounding += (sender, args) =>
            {
                UIApplication.SharedApplication.BeginInvokeOnMainThread(() =>
                {
                    if (UIDevice.CurrentDevice.CheckSystemVersion(10, 0))
                    {
                        // Request Permissions
                        UNUserNotificationCenter.Current.RequestAuthorization(
                            UNAuthorizationOptions.Alert | UNAuthorizationOptions.Badge | UNAuthorizationOptions.Sound, 
                            (granted, error) =>
                            {
                                // Do something if needed
                            });
                    }
                    else if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
                    {
                        var notificationSettings = UIUserNotificationSettings.GetSettingsForTypes(
                        UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound, null);

                        app.RegisterUserNotificationSettings(notificationSettings);
                    }
                });
            };

            var y = new DarkThemeResources();
            if (y.ContainsKey("a")) { }
            var z = new UnderlineEffect();
            if (z.Control == null) { }

            LoadApplication(_app);

            UIApplication.SharedApplication.SetStatusBarStyle(UIStatusBarStyle.Default, false);
            return base.FinishedLaunching(app, options);
        }

        public override void FailedToRegisterForRemoteNotifications(UIApplication application, NSError error)
        {
            // Notify the user!
            App.Analytics?.Track("FailedToRegisterForRemoteNotifications");
            Device.BeginInvokeOnMainThread(
                async () => await UserDialogs.Instance.AlertAsync("Error registering push notifications"));
        }

        public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {
            var token = deviceToken.Description;
            if (!string.IsNullOrWhiteSpace(token)) {
                token = token.Trim('<').Trim('>').Replace(" ", "");
            }
            Debug.WriteLine($"Device Token {token}");
            App.Analytics?.Track("RegisteredForRemoteNotifications", "token", token);
            System.Threading.Tasks.Task.Factory.StartNew(async () => await Comms.SendPushNotificationToken(token, Comms.DeviceType.Ios));
        }

        public override void DidReceiveRemoteNotification(UIApplication application, NSDictionary userInfo, Action<UIBackgroundFetchResult> completionHandler)
        {
            App.Analytics?.Track("DidReceiveRemoteNotification");

            // Push notification received.
            System.Threading.Tasks.Task.Factory.StartNew(async () => await _app.ProcessBackgrounding());
            completionHandler(UIBackgroundFetchResult.NewData);
        }
    }
}
