﻿using System;
using Foundation;
using System.Threading.Tasks;
using UIKit;
using System.Threading;
using System.Collections.Concurrent;

namespace DJWayniesApp.iOS
{
    public class CustomSessionDownloadDelegate : NSUrlSessionDownloadDelegate
    {
        private readonly Downloader _downloader;

        public CustomSessionDownloadDelegate(Downloader downloader)
        {
            _downloader = downloader;
        }

        public override void DidFinishDownloading(NSUrlSession session, NSUrlSessionDownloadTask downloadTask, NSUrl location)
        {
            var fileManager = NSFileManager.DefaultManager;
            var contents = fileManager.Contents(location.Path);
            var contentLength = contents.Length;
            var byteData = new byte[contentLength];
            System.Runtime.InteropServices.Marshal.Copy(contents.Bytes, byteData, 0, Convert.ToInt32(byteData));

            _downloader.DownloadCompleted(downloadTask.OriginalRequest.Url.AbsoluteString, byteData);
        }

        public override void DidCompleteWithError(NSUrlSession session, NSUrlSessionTask task, NSError error)
        {
            if (error == null)
                return;

            var errorMessage = $"Download task completed with error. Task: {task.TaskIdentifier}, Error: {error}";
            _downloader.DownloadErrored(task.OriginalRequest.Url.AbsoluteUrl.ToString(), errorMessage);

            task.Cancel();
        }

        public override void DidFinishEventsForBackgroundSession(NSUrlSession session)
        {
            
        }
    }

    public class Downloader
    {
        public class DownloadResult : IDisposable
        {
            public string Url { get; set; }
            public bool Succeeded { get; set; }
            public byte[] Data { get; set; }
            public string Error { get; set; }
            public AutoResetEvent ResetEvent { get; set; }

            #region IDisposable implementation

            public void Dispose()
            {
                if (ResetEvent != null)
                {
                    ResetEvent.Set();
                    ResetEvent.Dispose();
                    ResetEvent = null;
                }
            }

            #endregion
        }

        private const string SessionId = "uk.co.thekaraokeapp.downloader";
        private readonly NSUrlSession _session;
        private readonly ConcurrentDictionary<string, DownloadResult> _downloadResults = new ConcurrentDictionary<string, DownloadResult>();

        public Downloader()
        {
            using (var sessionConfig = UIDevice.CurrentDevice.CheckSystemVersion(8, 0) ?
                NSUrlSessionConfiguration.CreateBackgroundSessionConfiguration(SessionId) :
                NSUrlSessionConfiguration.BackgroundSessionConfiguration(SessionId))
            {
                sessionConfig.NetworkServiceType = NSUrlRequestNetworkServiceType.Default;
                sessionConfig.HttpMaximumConnectionsPerHost = 4;

                var sessionDelegate = new CustomSessionDownloadDelegate(this);
                _session = NSUrlSession.FromConfiguration(sessionConfig, (INSUrlSessionDelegate)sessionDelegate, null);
            }
        }

        public void DownloadErrored(string url, string error)
        {
            var result = _downloadResults[url];
            result.Error = error;
            result.Succeeded = false;
            result.ResetEvent.Set();
        }

        public void DownloadCompleted(string url, byte[] data)
        {
            var result = _downloadResults[url];
            result.Error = null;
            result.Succeeded = true;
            result.Data = data;
            result.ResetEvent.Set();
        }

        public async Task<DownloadResult> DownloadFileAsync(string url)
        {
            var result = new DownloadResult { Url = url, ResetEvent = new AutoResetEvent(false) };
            if(!_downloadResults.TryAdd(url, result) == false)
            {
                result.Dispose();
                return new DownloadResult { Error = "That resource is already being downloaded", Succeeded = false, Data = null };
            }
            _session.CreateDownloadTask(NSUrl.FromString(url)).Resume();
            await Task.Factory.StartNew(() => result.ResetEvent.WaitOne());
            _downloadResults.TryRemove(url, out result);
            return result;
        }

        public DownloadResult DownloadFile(string url)
        {
            var result = new DownloadResult { Url = url, ResetEvent = new AutoResetEvent(false) };
            if(!_downloadResults.TryAdd(url, result) == false)
            {
                result.Dispose();
                return new DownloadResult { Error = "That resource is already being downloaded", Succeeded = false, Data = null };
            }
            _session.CreateDownloadTask(NSUrl.FromString(url)).Resume();
            result.ResetEvent.WaitOne();
            _downloadResults.TryRemove(url, out result);
            return result;
        }

        public void DownloadFileCallback(string url, Action<DownloadResult> callback)
        {
            var result = new DownloadResult { Url = url, ResetEvent = new AutoResetEvent(false) };
            if(!_downloadResults.TryAdd(url, result) == false)
            {
                result.Dispose();
                callback(new DownloadResult { Error = "That resource is already being downloaded", Succeeded = false, Data = null });
            }
            _session.CreateDownloadTask(NSUrl.FromString(url)).Resume();
            Task.Factory.StartNew(() => result.ResetEvent.WaitOne()).ContinueWith(t =>
            {
                _downloadResults.TryRemove(url, out result);
                callback(result);
            });
        }
    }
}
