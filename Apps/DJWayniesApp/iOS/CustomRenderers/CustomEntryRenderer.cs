﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using DJWayniesApp.Controls;
using DJWayniesApp.iOS.CustomRenderers;
using DJWayniesApp.iOS.Extensions;
using Foundation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
namespace DJWayniesApp.iOS.CustomRenderers
{
    public class CustomEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                Control.SpellCheckingType = UITextSpellCheckingType.No;             // No Spellchecking
                Control.AutocorrectionType = UITextAutocorrectionType.No;           // No Autocorrection
                Control.AutocapitalizationType = UITextAutocapitalizationType.None; // No Autocapitalization
                Control.ReturnKeyType = (e.NewElement as CustomEntry)?.ReturnKeyType.GetValueFromDescription() ?? UIReturnKeyType.Default;
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (e.PropertyName == CustomEntry.ReturnKeyPropertyName)
            {
                Control.ReturnKeyType = (sender as CustomEntry)?.ReturnKeyType.GetValueFromDescription() ?? UIReturnKeyType.Default;
            }
        }
    }
}
