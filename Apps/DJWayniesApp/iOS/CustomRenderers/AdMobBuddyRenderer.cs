﻿using DJWayniesApp.Controls;
using DJWayniesApp.Helpers;
using DJWayniesApp.iOS.CustomRenderers;
using DJWayniesApp.iOS.Services;
using Foundation;
using Google.MobileAds;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(AdMobBuddyControl), typeof(AdMobBuddyRenderer))]
namespace DJWayniesApp.iOS.CustomRenderers
{
    public class AdMobBuddyRenderer : ViewRenderer
    {
        private BannerView _adView;
        private bool _viewOnScreen;

        protected override void OnElementChanged(ElementChangedEventArgs<View> e)
        {
            base.OnElementChanged(e);

            if (Element is AdMobBuddyControl)
            {
                bool npa = Settings.NonPersonalisedAds;
                var vc = UIApplication.SharedApplication.Windows[0].RootViewController;
                foreach (var v in UIApplication.SharedApplication.Windows)
                {
                    if (v.RootViewController != null)
                    {
                        vc = v.RootViewController;
                    }
                }

                _adView = new BannerView(AdSizeCons.Banner)
                {
                    AdUnitID = IosAdService.BannerAdUnitId,
                    RootViewController = vc
                };

                _adView.AdReceived += (sender, args) =>
                {
                    if (!_viewOnScreen)
                    {
                        AddSubview(_adView);
                        _viewOnScreen = true;
                    }
                };
                _adView.ReceiveAdFailed += (sender, args) =>
                {
                    //_adView.LoadRequest(Request.GetDefaultRequest());
                };
                _adView.LoadRequest(IosAdService.GetAdRequest());
                SetNativeControl(_adView);
            }
        }
    }
}
