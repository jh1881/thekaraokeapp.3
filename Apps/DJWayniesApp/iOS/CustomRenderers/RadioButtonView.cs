﻿using CoreGraphics;
using Foundation;
using UIKit;

namespace DJWayniesApp.iOS.CustomRenderers
{
    [Register("RadioButtonView")]
    public class RadioButtonView : UIButton
    {
        public RadioButtonView()
        {
            Initialize();
        }

        public RadioButtonView(CGRect bounds) : base(bounds)
        {
            Initialize();
        }

        public bool Checked
        {
            set => Selected = value;
            get => Selected;
        }

        public string Text { set => SetTitle(value, UIControlState.Normal); }

        void Initialize()
        {
            AdjustEdgeInsets();
            ApplyStyle();

            TouchUpInside += (sender, args) => Selected = !Selected;
        }

        void AdjustEdgeInsets()
        {
            const float inset = 8f;

            HorizontalAlignment = UIControlContentHorizontalAlignment.Left;
            ImageEdgeInsets = new UIEdgeInsets(0f, inset, 0f, 0f);
            TitleEdgeInsets = new UIEdgeInsets(0f, inset * 2, 0f, 0f);
        }

        void ApplyStyle()
        {
            var selectedImg = UIImage.FromBundle("checked.png");
            SetImage(selectedImg, UIControlState.Selected);
            var unselectedImg = UIImage.FromBundle("unchecked.png");
            SetImage(unselectedImg, UIControlState.Normal);
        }
    }
}