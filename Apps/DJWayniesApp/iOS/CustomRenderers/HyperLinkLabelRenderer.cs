﻿using DJWayniesApp.Controls;
using DJWayniesApp.iOS.CustomRenderers;
using Foundation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(HyperLinkLabel), typeof(HyperLinkLabelRenderer))]
namespace DJWayniesApp.iOS.CustomRenderers
{
    public class HyperLinkLabelRenderer : LabelRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement == null)
            {
                Control.TextColor = UIColor.FromRGB(0, 122, 255);
                Control.BackgroundColor = UIColor.Clear;
                Control.UserInteractionEnabled = true;
                var tapXamarin = new UITapGestureRecognizer();

                tapXamarin.AddTarget(() =>
                    {
                        var hyperLinkLabel = Element as HyperLinkLabel;
                        if(hyperLinkLabel == null) return;
                        if(!string.IsNullOrEmpty(hyperLinkLabel.NavigateUri))
                        {
                            UIApplication.SharedApplication.OpenUrl(new NSUrl(GetNavigationUri(hyperLinkLabel.NavigateUri)));
                        }
                        else
                        {
                            hyperLinkLabel.NavigateCommand?.Execute(null);
                        }
                    });

                tapXamarin.NumberOfTapsRequired = 1;
                tapXamarin.DelaysTouchesBegan = true;
                Control.AddGestureRecognizer(tapXamarin);
            }
        }

        private string GetNavigationUri(string uri)
        {
            if (string.IsNullOrEmpty(uri)) return uri;
            if (uri.Contains("@") && !uri.StartsWith("mailto:"))
            {
                return $"mailto:{uri}";
            }
            if (uri.StartsWith("www."))
            {
                return $"http://{uri}";
            }
            return uri;
        }
    }
}
