﻿using System;
using System.ComponentModel;
using CoreGraphics;
using DJWayniesApp.Controls;
using DJWayniesApp.iOS.CustomRenderers;
using DJWayniesApp.iOS.Extensions;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomRadioButton), typeof(RadioButtonRenderer))]
namespace DJWayniesApp.iOS.CustomRenderers
{
    public class RadioButtonRenderer : ViewRenderer<CustomRadioButton, RadioButtonView>
    {
        protected override void OnElementChanged(ElementChangedEventArgs<CustomRadioButton> e)
        {
            base.OnElementChanged(e);

            try
            {
                BackgroundColor = Element.BackgroundColor.ToUIColor();
                if (Control == null)
                {
                    var checkBox = new RadioButtonView(Bounds);
                    checkBox.TouchUpInside += (s, args) => Element.Checked = Control.Checked;

                    SetNativeControl(checkBox);
                }

                if (Control != null)
                {
                    Control.LineBreakMode = UILineBreakMode.CharacterWrap;
                    Control.VerticalAlignment = UIControlContentVerticalAlignment.Top;
                    Control.Text = e.NewElement?.Text;
                    if (e.NewElement != null)
                    {
                        var textColour = e.NewElement.TextColor.ToUIColor();
                        Control.Checked = e.NewElement.Checked;
                        Control.SetTitleColor(textColour, UIControlState.Normal);
                        Control.SetTitleColor(textColour, UIControlState.Selected);
                    }
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }

        private void ResizeText()
        {
            var text = Element.Text;
            var bounds = Control.Bounds;

            var width = (float)Control.TitleLabel.Bounds.Width;
            var height = text.StringHeight(Control.Font, width);
            var minHeight = string.Empty.StringHeight(Control.Font, width);

            var requiredLines = (int)Math.Round(height / minHeight, MidpointRounding.AwayFromZero);
            var supportedLines = (int)Math.Round(bounds.Height / minHeight, MidpointRounding.ToEven);

            if (supportedLines != requiredLines)
            {
                bounds.Height += (float)(minHeight * (requiredLines - supportedLines));
                Control.Bounds = bounds;
                Element.HeightRequest = bounds.Height;
            }
        }

        public override void Draw(CGRect rect)
        {
            base.Draw(rect);
            ResizeText();
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            switch (e.PropertyName)
            {
                case "Checked":
                    Control.Checked = Element.Checked;
                    break;
                case "Text":
                    Control.Text = Element.Text;
                    break;
                case "TextColor":
                    var colour = Element.TextColor.ToUIColor();
                    Control.SetTitleColor(colour, UIControlState.Normal);
                    Control.SetTitleColor(colour, UIControlState.Selected);
                    break;
                case "Element":
                    break;
                default:
                    return;
            }
        }
    }
}
