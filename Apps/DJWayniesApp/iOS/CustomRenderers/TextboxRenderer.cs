﻿using DJWayniesApp.iOS.CustomRenderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(Entry), typeof(TextboxRenderer))]
namespace DJWayniesApp.iOS.CustomRenderers
{
    // Renderer for textbox that ensures we get the Clear "x" button
    public class TextboxRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (Control == null) return;
            Control.ClearButtonMode = UITextFieldViewMode.WhileEditing;
            Control.AutocorrectionType = UITextAutocorrectionType.No;
        }
    }
}